(function () {
    String.prototype.replaceAll = function (target, replacement) {
        return this.split(target).join(replacement);
    };

    moment.locale('pl');

    angular.module('registreoApp', [
            'ngMaterial',
            'ui.router',
            'ui.router.stateHelper',
            'restangular',
            'notifications',
            'registreoApp.about',
            'registreoApp.pricing',
            'registreoApp.contact',
            'registreoApp.home',
            'registreoApp.eventsList',
            'ui.bootstrap',
            'mailerService',
            'angularSpinner',
            'registreoApp.eventService',
            'registreoApp.eventEditController',
            'registreoApp.eventCustomPageController',
            'registreoApp.eventCustomFormController',
            'registreoApp.eventsListController',
            'customPageService',
            'ui.sortable',
            'registreoApp.userService',
            'ngCookies',
            'angularFileUpload',
            'mdPickers',
            'ngMessages',
            'ui.tinymce',
            'vcRecaptcha',
            'nemLogging',
            'uiGmapgoogle-maps',
            'dndLists'
        ])
        .config(function(uiGmapGoogleMapApiProvider) {
            uiGmapGoogleMapApiProvider.configure({
              key: 'AIzaSyCcyYBnlEFOswqxn5xZ795-nZoheIw-QuQ'
            });
        })
        .config(function ($urlRouterProvider, stateHelperProvider, RestangularProvider, api) {
            $urlRouterProvider.otherwise("/");
            RestangularProvider.setBaseUrl(api.url);
        })
        .run(function ($window, $mdDialog, Restangular) {
            Restangular.setErrorInterceptor(function (response) {
                if (response.status == 401){
                    $window.location.href = '/signin';
                } else {

                    var content = '';
                    if (typeof response.data === 'string' ) {
                        content = response.data;
                    } else {
                        $.each(response.data, function (index, item){
                            if (item.message !== undefined) {
                                content += item.message + "\n";
                            }
                        });
                        if (content.length < 1) {
                            content = response.data;
                        }                    
                    }

                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title('Wystąpił błąd')
                            .content(content)
                            .parent(angular.element(document.querySelector('body')))
                            .ok('Ok')
                    );
                }
                return true;
            });
        })
        .constant('api', {
            url: '/api'
        })
        .filter('rgPrice', function() {
            return function(input) {
                var value = Math.round(input * 100);
                var afterComa = value % 100;
                
                if (input >= 1000) {
                    value = Math.floor(input);
                    var hungreset = String("00" + (value % 1000)).slice(-3);
                    return Math.floor(input/1000) + ' ' + hungreset + ',' + String("00" + afterComa).slice(-2);
                } else if (input >= 0) {
                    return Math.floor(input) + ',' + String("00" + afterComa).slice(-2);
                } else if (input > -1) {
                    return '-' + Math.ceil(input) + ',' + String("00" + afterComa).slice(-2);
                } else if (input > -1000) {
                    return Math.ceil(input) + ',' + String("00" + afterComa).slice(-2);
                } else {
                    value = Math.ceil(input);
                    var hungreset = String("00" + (value % 1000)).slice(-3);
                    return Math.ceil(input/1000) + ' ' + hungreset + ',' + String("00" + afterComa).slice(-2);
                }
            };
        })
        .filter('debug', function() {
            return function(input) {
                if (input === '') return 'empty string';
                return input ? input : ('' + input);
            };
        });

})();