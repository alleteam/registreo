angular.module('registreoApp.contactController', [])
    .controller('ContactController',['$rootScope', '$scope', 'Breadcrumb', 'NotificationsService', 'MailerService', 'vcRecaptchaService',
        function ($rootScope, $scope, $Breadcrumb, NotificationsService, MailerService, recaptcha) {
            $rootScope.$broadcast('activeMenu', 'contact');

            var self = this;
            $scope.contact = {};
            $scope.sendMessageLoader = false;
            $scope.recaptchaPublicKey = '6LeQgxQUAAAAABDHmMR3iAKdI8Grh3uEx9_jcLGn';
            $scope.widgetId = '';

            $rootScope.ngview.show = true;
            window.scrollTo(0, 0);
            $Breadcrumb.set('Kontakt', [{name: 'Kontakt', uri: '/contact'}]);


            $scope.reset = function(form) {
                $scope.contact = {};
                form.$setPristine();
                form.$setUntouched();
                recaptcha.reload($scope.widgetId);
            };

            $scope.setWidgetId = function (widgetId) {
                $scope.widgetId = widgetId;
            };

            $scope.sendContactMessage = function (form) {
                form.$submitted = true;
                if(form.$valid && $scope.sendMessageLoader == false) {
                    $scope.sendMessageLoader = true;
                    MailerService.sendMessage($scope.contact).then(function (response) {
                        if (response.status == 204) {
                            NotificationsService.addNotification('Twoje zapytanie zostało wysłane. Dziękujemy za kontakt.');
                            $scope.reset(form);
                        } else {
                            NotificationsService.addNotification('Wystąpił problem techniczny. Spróbuj wysłać wiadomość ponownie', NotificationsService.ERROR_NOTIFICATION);
                        }
                        $scope.sendMessageLoader = false;
                    });
                }
            };
        }
    ]);


