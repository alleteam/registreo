<?php
namespace Registreo\CustomPageBundle\Element;

use JMS\Serializer\Annotation as JMS;

class Image extends ElementAbstract implements ElementTemplate
{
    /**
     * @JMS\Exclude
     */
    public static $TEMPLATE = '<img ng-src="{{templateData.section_list[#index#].element.url}}" class="img-responsive"></img>';


    /**
     * @JMS\Type("string")
     * @var string
     */
    private $url;

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    public function getTemplate()
    {
        return self::$TEMPLATE;
    }
}