<?php

namespace Registreo\EventBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Registreo\Entity\Event;
use Registreo\Entity\User as UserModel;

class EventRepository extends EntityRepository
{
    /**
     * @param Event $event
     */
    public function save(Event $event)
    {
        $this->getEntityManager()->persist($event);
        $this->getEntityManager()->flush();
    }

    public function update(Event $event)
    {
        $this->getEntityManager()->merge($event);
        $this->getEntityManager()->flush();
    }

    public function getAll(UserModel $user)
    {
        $qb = $this->createQueryBuilder('event');
        $query = $qb->select('event')
            ->where('event.user = :user')
            ->orderBy('event.id', 'desc')
            ->setParameters([
                'user' => $user,
            ])
            ->getQuery();
        
        return $query->getResult();
    }

    //TODO get all list with active events, with cover photo
    /**
     * @return Event[]
     */
    public function getAllList()
    {
        $qb = $this->createQueryBuilder('event');
        $query = $qb->select('e')
            ->from('Registreo:Event', 'e')
            ->innerJoin('e.seoLink','s')
            ->where('s.name in (:links)')
            ->orderBy('event.id', 'desc')
            ->setParameters([
                'links' => ['kayak-trip-example', 'cross-country-example', 'conference-example', 'fatima-example'],
            ])
            ->getQuery();

        return $query->getResult();
    }
}