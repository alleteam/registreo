(function () {
    angular.module('registreoApp.about', ['registreoApp.aboutController'])
        .config(config);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('about', {
                url: '/about',
                templateUrl: 'app/about/about.html',
                controller: 'AboutController',
                controllerAs: 'aboutCtrl'
            });
    }
})();
