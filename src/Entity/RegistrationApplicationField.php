<?php
namespace Registreo\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as ORMAssert;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\Exclude;

/**
 * Class Event
 *
 * @ORM\Entity(repositoryClass="Registreo\CustomPageBundle\Repository\RegistrationApplicationFieldRepository")
 * @ORM\Table(name="application_fields")
 */
class RegistrationApplicationField
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=50)
     * @Assert\NotBlank
     */
    protected $value;

    /**
     * @var CustomFormField
     *
     * @ORM\ManyToOne(targetEntity="CustomFormField")
     * @@ORM\JoinColumn(name="form_field_id", referencedColumnName="id")
     */
    protected $formField;

    /**
     * @ORM\ManyToOne(targetEntity="Participant", inversedBy="fields")
     * @ORM\JoinColumn(name="participant_id", referencedColumnName="id")
     * @Exclude
     */
    protected $participant;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return CustomFormField
     */
    public function getFormField()
    {
        return $this->formField;
    }

    /**
     * @param CustomFormField $formField
     */
    public function setFormField($formField)
    {
        $this->formField = $formField;
    }

    /**
     * @return mixed
     */
    public function getParticipant()
    {
        return $this->participant;
    }

    /**
     * @param mixed $participant
     */
    public function setParticipant($participant)
    {
        $this->participant = $participant;
    }
}
