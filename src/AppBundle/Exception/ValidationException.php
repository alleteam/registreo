<?php

namespace Registreo\AppBundle\Exception;

use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationException extends \Exception
{
    /**
     * @var ConstraintViolationListInterface
     */
    protected $validationErrors;

    public function __construct(ConstraintViolationListInterface $validationErrors, $message = '')
    {
        parent::__construct($message);
        $this->validationErrors = $validationErrors;
    }

    /**
     * @return ConstraintViolationListInterface
     */
    public function getValidationErrors()
    {
        return $this->validationErrors;
    }
}
