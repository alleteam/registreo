angular.module('registreoApp')
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('participants', {
                url: "/event/{eventId:int}/participants",
                controller: 'ParticipantsCtrl',
                controllerAs: 'participantsCtrl',
                templateUrl: "app/event/views/participants.html",
                resolve:{
                    checkAuth: ['UserService', function(UserService){
                        return UserService.checkAuth();
                    }]
                }
            });    
        }
    ])
    .controller('ParticipantsCtrl',['$scope', '$rootScope', 'Breadcrumb', 'EventService', 
                'NotificationsService', '$mdDialog', '$stateParams', 'PaginService', '$location',
        function ($scope, $rootScope, $Breadcrumb, EventService, NotificationsService, 
                    $mdDialog, $stateParams, $PaginService, $location) {
            $rootScope.$broadcast('activeMenu', 'myaccount');
            $rootScope.ngview.show = true;
            window.scrollTo(0, 0);
            $Breadcrumb.set('Lista uczestników', [
                {name: 'Moje konto', uri: '/myaccount'}, {name: 'Moje wydarzenia', uri: '/myevents'},
                {name: 'Lista uczestników', uri: '/event/'+ $stateParams.eventId +'/participants'}
            ]);

            $scope.newParticipant = {};
            $scope.participantSaveing  = false;

            var self = this;
            self.loadParticipants = true;
            self.participants = [];
            $scope.limit = 50;
            $scope.pagin = {
                visible: false,
                current: 1
            };
            $scope.eventId = $stateParams.eventId;
            $scope.event = {closed: false} 

            $scope.myaccount = function() {
                $location.path('/myevents');
            };

            self.refreshList = function() {
                self.loadParticipants = true;
                EventService.getParticipants($stateParams.eventId, $scope.pagin.current, $scope.limit).then(function (data) {
                    $scope.totalResult = data[0].pagin.total;
                    $scope.page = data[0].pagin.current;
                    $scope.pagin = $PaginService.craetePagin(data[0].pagin);
                    $scope.pagin.current = parseInt($scope.pagin.current);
                    var list = data[0].list;
                    for (var i in list) {
                        list[i].statusName = self.getPaymentStatusName(list[i].payment_status);
                    }
                    self.participants = list;
                    self.loadParticipants = false;
                    window.scrollTo(0, 0);
                });
            };
            
            self.checkPaid = function(event, participant) {
                $mdDialog.show({
                    clickOutsideToClose: true,
                    parent: angular.element(document.querySelector('#wrapper')),
                    targetEvent: event,
                    templateUrl: 'app/event/views/changePaymentStatus.html',
                    controller: function ($scope, $mdDialog) {
                        $rootScope.ngview.show = true;
                        $scope.participant = participant;
                        $scope.changePaymentStatusLoading = false;
                        $scope.closeModal = function () {
                            $mdDialog.hide();
                        };
                        $scope.changePaymentStatus = function () {
                            $scope.changePaymentStatusLoading = true;
                            EventService.changePaymentStatus($stateParams.eventId, participant.id).then(function () {
                                participant.payment_status = 3;
                                participant.statusName = self.getPaymentStatusName(3);
                                $mdDialog.hide();
                                $scope.changePaymentStatusLoading = false;
                            });
                        };
                    }
                });
            };

            self.showDetails = function(event, participantId) {
                $mdDialog.show({
                    clickOutsideToClose: true,
                    parent: angular.element(document.querySelector('#wrapper')),
                    targetEvent: event,
                    templateUrl: 'app/event/views/participantView.html',
                    controller: function ($scope, $mdDialog) {
                        $rootScope.ngview.show = true;
                        $scope.loadingParticipantDetails = true;
                        EventService.getParticipantDetails($stateParams.eventId, participantId).then(function (data) {
                            $scope.participantFields = data;
                            $scope.loadingParticipantDetails = false;
                        });

                        $scope.closeModal = function () {
                            $mdDialog.hide();
                        };
                    }
                });
            };
            
            $scope.setPage = function(current) {
                $scope.pagin.current = current;
                UserService.getCurrentUser().then(function (user) {
                    $scope.user = user;
                    self.refreshList();
                });
            };
            
            
            self.getPaymentStatusName = function(status) {
                switch (status) {
                    case 0:
                        return "brak";
                    case 1:
                        return "nowy";
                    case 2:
                        return "w trakcie przetwarzania";
                    case 3:
                        return "opłacony";
                    case 4:
                        return "odrzucony";
                    case 5:
                        return "oczekujący na przetwarzanie";
                    case 6:
                        return "realizowana";
                    case 10:
                        return "zweryfikowany";
                    case 11:
                        return "zaproszony";

                    return "brak";
                }
            };
            
            self.refreshList();
            
            EventService.getEvent($scope.eventId).then(function (event) {
                $scope.event = event;
            });
            
            
            $scope.saveParticipant = function(valid) {
                if (valid) {
                    EventService.inviteParticipant($scope.eventId, $scope.newParticipant).then(function (data) {
                        self.refreshList();
                        NotificationsService.addNotification('Twoje zaproszenie zostało wysłane.');
                    });
                }
            };
        }
    ]);

