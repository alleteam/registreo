angular.module('registreoApp')
    .service('TransactionService', ['Restangular', '$q', function(restangular, q) {
        
        this.getPayoutsList = function(eventId) {
            var defer = q.defer();
            restangular.all('event/' + eventId + '/payouts').getList().then(
                function (data) {
                    defer.resolve(restangular.stripRestangular(data));
                }
            );
            return defer.promise;
        };            
        
        this.getList = function(eventId) {
            var defer = q.defer();
            restangular.all('event/' + eventId + '/transactions').getList().then(
                function (data) {
                    defer.resolve(restangular.stripRestangular(data));
                }
            );
            return defer.promise;
        };            
        
        this.getShortList = function(eventId) {
            var defer = q.defer();
            restangular.all('event/' + eventId + '/transactions/short').getList().then(
                function (data) {
                    defer.resolve(restangular.stripRestangular(data));
                }
            );
            return defer.promise;
        };            
        
        this.getSummary = function(eventId) {
            var defer = q.defer();
            restangular.one('event/' + eventId + '/transactions/summary').get().then(
                function (data) {
                    defer.resolve(restangular.stripRestangular(data));
                }
            );
            return defer.promise;
        };            
        
        this.createPayout = function(eventId, amount) {
            var payout = {amount: amount};
            var defer = q.defer();
            restangular.all('event/' + eventId + '/payout').post(payout).then(
                function (data) {
                    defer.resolve(restangular.stripRestangular(data));
                }
            );
            return defer.promise;
        };            
}]);

