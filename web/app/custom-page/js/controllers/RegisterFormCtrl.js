angular.module('registreoAppCustom')
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('register', {
                url: "/{eventName}/register",
                controller: 'RegistrationFormCtrl',
                controllerAs: 'RegistrationFormCtrl',
                templateUrl: '/app/custom-page/views/register.html'
            });    
        }
    ])
    .controller('RegistrationFormCtrl',['$rootScope', '$scope', '$state', 'CustomPageService', 
        function ($rootScope, $scope, $state, CustomPageService) {
            $rootScope.ngview.show = true;
            window.scrollTo(0, 0);
            $scope.form = $rootScope.eventForm;
            $scope.participantData = $rootScope.participant;
            $scope.endRegistration = $rootScope.endRegistration;
            $scope.registrationFinished = $rootScope.registrationFinished;

            if ($scope.participantData) {
                if ($scope.participantData.payment_status != 11) {
                    $state.go('payment', {eventName: $state.params.eventName, participant: $scope.participantData.id});
                }
                
                $scope.form.fields = $scope.form.fields.filter(function(el) { return el.editable; }); 
            }

            $scope.saveApplicationForm = function(isValid) {
                if (isValid) {
                    $scope.customFormSaveing = true;
                    CustomPageService.saveApplicationForm($rootScope.eId, $scope.form.fields, $scope.form.regulation).then(function (participant) {
                        $scope.participantData = participant;
                        $scope.customFormSaveing = false;
                        //if($rootScope.freeAccount == 1) {
                        //    $state.go('registration-status', {eventName: $state.params.eventName});
                        //} else {
                            $state.go('payment', {eventName: $state.params.eventName, participant: participant.id});
                        //}
                    });
                }
            };
        }
    ]);

