angular.module('registreoApp')
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('myevents', {
                url: "/myevents",
                controller: 'EventListCtrl',
                controllerAs: 'eventListCtrl',
                templateUrl: "app/event/views/list.html",
                resolve:{
                    checkAuth: ['UserService', function(UserService){
                        return UserService.checkAuth();
                    }]
                }
            });    
        }
    ])
    .controller('EventListCtrl',['$scope', '$rootScope', 'Breadcrumb', 'EventService', 'PaginService', '$location', '$mdDialog',
        function ($scope, $rootScope, $Breadcrumb, $EventService, $PaginService, $location, $mdDialog) {
            $rootScope.$broadcast('activeMenu', 'myaccount');
            
            $rootScope.ngview.show = true;
            $Breadcrumb.set('Moje wydarzenia', [
                {name: 'Moje konto', uri: '/myaccount'}, {name: 'Moje wydarzenia', uri: '/myevents'}]);
            window.scrollTo(0, 0);
            
            $scope.pharse = "";
            $scope.search_name = "";
            $scope.pagin = {
                visible: false,
                current: 1
            };
            $scope.events = [];
            
            $scope.refreshEvents = function() {
                $scope.loadingEvents = true;
                $EventService.getEvents($scope.pagin.current).then(
                    function (data) {
                        $scope.events = data.events;
                        $scope.pagin = $PaginService.craetePagin(data.pagin);
                        $scope.loadingEvents = false;
                    }
                );
            };

            $scope.setPage = function(current) {
                $scope.pagin.current = current;
                $scope.refreshEvents();
            };




            $scope.showManagePanel = function(event) {
                if (event.status) {
                    $location.path('/event/' + event.id + '/edit');
                }
            };
            
            $scope.showParticipants = function(eventId) {
                $location.path('/event/' + eventId + '/participants');
            };

            $scope.showEventPage = function(event) {
                if (event.status) {
                    var url = $location.protocol() + '://' + $location.host() + '/' + event.seo_link.name;
                    window.open(url, '_blank');
                }                
            };

            $scope.showRegisterForm = function(event) {
                if (event.status && !event.closed) {
                    var url = $location.protocol() + '://' + $location.host() + '/' + event.seo_link.name + '/register';
                    window.open(url, '_blank');
                }
            };

            $scope.showTransactions = function(event) {
                if (!event.free) {
                    $location.path('event/' + event.id  + '/transactions');
                }
            };

            $scope.disableEvent = function (event, eventElement) {
                if (eventElement.status) {
                    window.scrollTo(0, 0);
                    $mdDialog.show({
                        clickOutsideToClose: true,
                        parent: angular.element(document.querySelector('#wrapper')),
                        targetEvent: event,
                        templateUrl: 'app/event/views/changeEventStatus.html',
                        controller: function ($scope, $mdDialog) {
                            $rootScope.ngview.show = true;
                            $scope.event = eventElement;
                            $scope.changeEventStatusLoading = false;
                            $scope.closeModal = function () {
                                $mdDialog.hide();
                            };
                            $scope.changePaymentStatus = function () {
                                $scope.changeEventStatusLoading = true;
                                $EventService.changeEventStatus(eventElement.id, 0).then(function () {
                                    eventElement.status = 0;
                                    $mdDialog.hide();
                                    $scope.changeEventStatusLoading = false;
                                });
                            };
                        }
                    });
                }
            };


            $scope.addEvent = function() {
                $location.path('/event/add');
            };

            $scope.searchBy = function () {
                $scope.search_name = $scope.pharse;
            };
            
            $scope.refreshEvents();

            $scope.addEvent = function() {
                $location.path('/event/add');
            };
        }
    ]);

