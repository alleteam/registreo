<?php

namespace Registreo\UserBundle\Service;

use Symfony\Component\Templating\EngineInterface;

class MailerService
{
    const NOTIFICATION_DIRECTORY = 'UserBundle:Notifications:';
    const HTML_TYPE = 'text/html';

    /**
     * @var \Swift_Mailer
     */
    public $mailer;

    /**
     * @var EngineInterface
     */
    public $templateEngine;

    /**
     * @var string
     */
    public $senderEmail;

    /**
     * @var string
     */
    public $senderName;

    /**
     * MailerService constructor.
     * @param \Swift_Mailer $mailer
     * @param EngineInterface $template
     * @param string $senderName
     * @param string $senderEmail
     */
    public function __construct($mailer, $template, $senderName, $senderEmail)
    {
        $this->mailer = $mailer;
        $this->templateEngine = $template;
        $this->senderEmail = $senderEmail;
        $this->senderName = $senderName;
    }

    /**
     * Email send on password recovery request
     *
     * @param string $recoveryLink
     * @param string $recipient
     * @return int
     */
    public function sendPasswordRecovery($recoveryLink, $recipient)
    {
        $data = [
            'shortcutUrl' => $recoveryLink
        ];
        
        $body = $this->templateEngine->render(
            self::NOTIFICATION_DIRECTORY . 'passwordRecovery.html.twig',
            $data
        );
        
        $message = \Swift_Message::newInstance()
            ->setSubject('Przywracanie hasła')
            ->setFrom([ $this->senderEmail => $this->senderName ])
            ->setTo($recipient)
            ->setBody($body, self::HTML_TYPE);

        return $this->mailer->send($message);
    }

}
