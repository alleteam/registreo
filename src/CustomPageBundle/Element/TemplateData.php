<?php
namespace Registreo\CustomPageBundle\Element;

use JMS\Serializer\Annotation as JMS;


class TemplateData
{
    /**
     * @JMS\Type("string")
     * @var string
     */
    private $style = "default.css";


    /**
     * @JMS\Type("Registreo\CustomPageBundle\Element\Banner")
     * @var Banner
     */
    private $banner;

    /**
     * @JMS\Type("Registreo\CustomPageBundle\Element\MainParagraph")
     * @var MainParagraph
     */
    private $mainParagraph;

    /**
     * @JMS\Type("array<Registreo\CustomPageBundle\Element\Section>")
     * @var Registreo\CustomPageBundle\Element\Section[]
     */
    private $sectionList = [];


    public function __construct()
    {
        $this->banner = new Banner();
        $this->mainParagraph = new MainParagraph();
    }

    public function getStyle()
    {
        return $this->style;
    }

    public function getBanner()
    {
        return $this->banner;
    }

    public function getMainParagraph()
    {
        return $this->mainParagraph;
    }

    public function setMainParagraph(MainParagraph $mainParagraph)
    {
        $this->mainParagraph = $mainParagraph;
        return $this;
    }


    public function setStyle($style)
    {
        $this->style = $style;
        return $this;
    }

    public function setBanner(Banner $banner)
    {
        $this->banner = $banner;
        return $this;
    }

    public function setMainSection(Section $mainSection)
    {
        $this->mainSection = $mainSection;
        return $this;
    }

    public function setSectionList(array $sectionList)
    {
        $this->sectionList = $sectionList;
        return $this;
    }


    public function getTemplate()
    {
        $template =  $this->banner->getTemplate();
        $template .= '<div class="container" style="padding-bottom: 10px">';
        $template .= $this->mainParagraph->getTemplate();
        $template .= '<div class="row">';
        $part = 0;
        foreach ($this->sectionList as $key=>$section) {
            $part += $section->getPartOfPage();
            if ($part > 12) {
                $template .= '</div><div class="row">';
                $part = $section->getPartOfPage();
            }
            $template .=  str_replace('#index#', $key, $section->getTemplate());
        }
        return $template . '</div></div>';
    }



}