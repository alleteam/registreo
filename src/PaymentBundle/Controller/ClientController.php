<?php

namespace Registreo\PaymentBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as WEB;
use Registreo\AppBundle\Exception\ResourceNotFoundException;
use Registreo\PaymentBundle\Service\PaymentService;
use Registreo\Entity\Participant;

class ClientController extends FOSRestController
{

    /**
     * @var PaymentService
     */
    private $paymentService;

    /**
     * @var string
     */
    private $dotpayAddress;

    /**
     *
     * @param ContainerInterface $container
     * @param DotPay $dotpayService
     * @param PaymentService $paymentService
     * @param type $dotpayAddress
     */
    public function __construct(ContainerInterface $container, PaymentService $paymentService, $dotpayAddress)
    {
        $this->container = $container;
        $this->paymentService = $paymentService;
        $this->dotpayAddress = $dotpayAddress;
    }


    /**
     * @Rest\Route("/event/payment-data/{participant}")
     * @Web\Method({"GET"})
     * @WEB\ParamConverter("participant", class="Registreo:Participant")
     *
     * @param Participant $participant
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function getPaymentDataFormAction(Participant $participant)
    {
        //if (empty($event)) {
        //    throw new ResourceNotFoundException('Event not found');
        //}
        $view = $this->view([
            'participant' => $participant,
            'address' => $this->dotpayAddress,
            'params' => $this->paymentService->getDataForm($participant)]);
        return $this->handleView($view);
    }
}