<?php

namespace Registreo\EventBundle\Service;

use FOS\UserBundle\Model\UserInterface;
use Registreo\AppBundle\Exception\ValidationException;
use Registreo\Entity\CustomForm;
use Registreo\Entity\CustomFormField;
use Registreo\Entity\CustomPage;
use Registreo\Entity\CustomPageBanner;
use Registreo\Entity\CustomPageContent;
use Registreo\Entity\Event;
use Registreo\Entity\SeoLink;
use Registreo\Entity\File;
use Registreo\EventBundle\Repository\CustomFormFieldRepository;
use Registreo\EventBundle\Repository\CustomFormRepository;
use Registreo\EventBundle\Repository\CustomPageContentRepository;
use Registreo\EventBundle\Repository\CustomPageRepository;
use Registreo\EventBundle\Repository\EventRepository;
use Registreo\FileBundle\Repository\FileRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class EventService
{
    const CONTENT_TYPE_SIMPLE = 1;
    const CONTENT_TYPE_CONTACT = 2;
    const CONTENT_TYPE_MAP = 3;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @var EventRepository
     */
    protected $eventRepository;

    /**
     * @var CustomPageRepository
     */
    protected $customPageRepository;

    /**
     * @var CustomPageContentRepository
     */
    protected $customPageContentRepository;

    /**
     * @var FileRepository
     */
    protected $fileRepository;

    /**
     * @var CustomFormRepository
     */
    protected $customFormRepository;

    /**
     * @var CustomFormFieldRepository
     */
    protected $customFormFieldRepository;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorization;

    /**
     * @var SeoService
     */
    protected $seoService;

    /**
     * @param EventRepository $eventRepository
     */
    public function setEventRepository(EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    /**
     * @return EventRepository
     */
    public function getEventRepository()
    {
        return $this->eventRepository;
    }

    /**
     * @param CustomFormFieldRepository $customFormFieldRepository
     */
    public function setCustomFormFieldRepository(CustomFormFieldRepository $customFormFieldRepository)
    {
        $this->customFormFieldRepository = $customFormFieldRepository;
    }

    /**
     * @param CustomFormRepository $customFormRepository
     */
    public function setCustomFormRepository(CustomFormRepository $customFormRepository)
    {
        $this->customFormRepository = $customFormRepository;
    }

    /**
     * @param FileRepository $fileRepository
     */
    public function setFileRepository(FileRepository $fileRepository)
    {
        $this->fileRepository = $fileRepository;
    }

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param AuthorizationCheckerInterface $authorization
     */
    public function setAuthorizationChecker(AuthorizationCheckerInterface $authorization)
    {
        $this->authorization = $authorization;
    }

    /**
     * @return CustomPageRepository
     */
    public function getCustomPageRepository()
    {
        return $this->customPageRepository;
    }

    /**
     * @param CustomPageRepository $customPageRepository
     * @return $this
     */
    public function setCustomPageRepository(CustomPageRepository $customPageRepository)
    {
        $this->customPageRepository = $customPageRepository;

        return $this;
    }

    /**
     * @param CustomPageContentRepository $customPageContentRepository
     * @return $this
     */
    public function setCustomPageContentRepository(CustomPageContentRepository $customPageContentRepository)
    {
        $this->customPageContentRepository = $customPageContentRepository;

        return $this;
    }

    /**
     * @return ValidatorInterface
     */
    public function getValidator()
    {
        return $this->validator;
    }

    /**
     * @param ValidatorInterface $validator
     * @return $this
     */
    public function setValidator(ValidatorInterface $validator)
    {
        $this->validator = $validator;

        return $this;
    }


    public function getSeoService()
    {
        return $this->seoService;
    }

    public function setSeoService(SeoService $seoService)
    {
        $this->seoService = $seoService;
        return $this;
    }

    
    /**
     * @param int $eventId
     * @return Event
     */
    public function getEventForCustomPage($eventId)
    {
        $event = $this->eventRepository->findOneBy([
            'id' => $eventId
        ]);

        return $event;
    }

    /**
     * @param int $eventId
     * @return Event
     */
    public function getEvent($eventId)
    {
        $event = $this->eventRepository->findOneBy([
            'id' => $eventId,
            'user' => $this->getUser()
        ]);

        return $event;
    }

    public function getEvents()
    {
        return $this->eventRepository->getAll($this->getUser());
    }

    /**
     * @return Event[]
     */
    public function getEventsList()
    {
        $eventsList = [];
        $events = $this->eventRepository->getAllList();
        foreach ($events as $event) {
            $eventsList[] = [
                'id' => $event->getId(),
                'name' => $event->getName(),
                'description' => $event->getDescription(),
                'limit' => $event->getLimit(),
                'start_date' =>  $event->getStartDate(),
                'registration_date' =>  $event->getRegistrationDate(),
                'seo' => $event->getSeoLink()->getName()
            ];
        }

        return $eventsList;
    }




    /**
     * @param Event $event
     * @throws ValidationException
     */
    public function createEvent(Event $event)
    {
        if (!empty($event->getId())) {
            if (!$this->getEvent($event->getId())) {
                throw new NotFoundHttpException();
            }

        /*
         * if new event:
         * - set seoLink
         * - set active
         * - add default custom page
         * - add default custom form
         */
        } else {
            $this->seoService->validateSeoLink($event->getSeoLink());
            $event->getSeoLink()->setActive('Y');
            $event->getSeoLink()->setEvent($event);

            $event->setStatus(1);
            $event->setDiscount($this->getUser()->getDiscount());

            $banner = new CustomPageBanner();
            $banner->setTitle($event->getName())
                ->setDescription("Motto wydarzenia")
                ->setTextColor("white");
        
            $customPage = new CustomPage();
            $customPage->setEvent($event)
                ->setActive(false)
                ->setListView(false)
                ->setBody(new \Registreo\CustomPageBundle\Element\TemplateData());

            $customForm = new CustomForm();

            $nameField = new CustomFormField();
            $nameField->setCustomForm($customForm);
            $nameField->setEditable(false);
            $nameField->setType(1);
            $nameField->setOrder(1);
            $nameField->setLabel("Imię");

            $surnameField = new CustomFormField();
            $surnameField->setCustomForm($customForm);
            $surnameField->setEditable(false);
            $surnameField->setType(1);
            $surnameField->setOrder(2);
            $surnameField->setLabel("Nazwisko");

            $emailField = new CustomFormField();
            $emailField->setCustomForm($customForm);
            $emailField->setEditable(false);
            $emailField->setType(1);
            $emailField->setOrder(3);
            $emailField->setLabel("Email");

            $customForm->setActive(false)
                ->setEvent($event)
                ->setFields([$nameField, $surnameField, $emailField]);

            $event->setCustomForm($customForm)
                ->setCustomPage($customPage);
        }

        $event->setAccountNumber(str_replace(' ', '', $event->getAccountNumber()));
        $event->setUser($this->getUser());

        $errors = $this->getValidator()->validate($event);
        if (count($errors) > 0) {
            throw new ValidationException($errors);
        }

        if (!empty($event->getId())) {
            $this->eventRepository->update($event);
        } else {
            $this->eventRepository->save($event);
        }
    }



    /**
     * @param CustomForm $form
     * @throws ValidationException
     */
    public function saveCustomForm(CustomForm $form)
    {
        $updatedForm = $this->customFormRepository->findOneById($form->getId());
        $updatedForm->setActive((bool) $form->isActive());
        $this->updateFields($updatedForm, $form);

        $errors = $this->getValidator()->validate($updatedForm);
        if (count($errors) > 0) {
            throw new ValidationException($errors);
        }

        $this->customFormRepository->save($updatedForm);
    }

    /**
     * @param CustomForm $updatedForm
     * @param CustomForm $form
     */
    private function updateFields(CustomForm $updatedForm, CustomForm $form)
    {
        foreach ($updatedForm->getFields() as $formField) {
            $field = $this->customFormFieldRepository->findOneById($formField->getId());
            $updatedForm->removeField($field);
            $this->customFormFieldRepository->remove($field);
        }

        foreach ($form->getFields() as $field) {
            $field->setId(null);
            foreach ($field->getOptions() as $option) {
                $option->setId(null);
                $option->setCustomFormField($field);
            }
            $updatedForm->addField($field);
        }
    }

    /**
     * @param CustomPage $page
     * @throws ValidationException
     */
    public function saveCustomPage(CustomPage $page)
    {
        /* @var CustomPage $updatedPage*/
        $updatedPage = $this->getCustomPageRepository()->findOneById($page->getId());
        $updatedPage->setActive($page->isActive());
        $updatedPage->setListView((int) $page->isListView());
        $updatedPage->setBody($page->getBody());
        //$this->updateBanner($updatedPage->getBanner(), $page);
        //$this->updateContent($updatedPage, $page);

        $errors = $this->getValidator()->validate($updatedPage);
        if (count($errors) > 0) {
            throw new ValidationException($errors);
        }

        $this->getCustomPageRepository()->save($updatedPage);
    }

    /**
     * @param CustomPageBanner $updatedPageBanner
     * @param CustomPage $page
     */
    private function updateBanner(CustomPageBanner $updatedPageBanner, CustomPage $page)
    {
        $updatedPageBanner->setDescription($page->getBanner()->getDescription());
        $updatedPageBanner->setTitle($page->getBanner()->getTitle());
        $updatedPageBanner->setTextColor($page->getBanner()->getTextColor());

        if ($page->getBanner()->getFile()) {
            $file = $this->fileRepository->findOneById($page->getBanner()->getFile()->getId());
            $updatedPageBanner->setFile($file);
        } else {
            $updatedPageBanner->setFile(null);
        }
    }

    /**
     * @param CustomPage $updatedPage
     * @param CustomPage $page
     */
    private function updateContent(CustomPage $updatedPage, CustomPage $page)
    {
        foreach ($updatedPage->getContent() as $content) {
            $newPageContent = $this->getNewPageContent($content->getId(), $page);

            //update existing content or delete it
            if ($newPageContent) {
                $content->setOrder($newPageContent->getOrder());
                $content->setTitle($newPageContent->getTitle());
                $content->setDescription($newPageContent->getDescription());
                if ($content->getType() == self::CONTENT_TYPE_CONTACT) {
                    $content->getContactData()->setAddress($newPageContent->getContactData()->getAddress()) ;
                    $content->getContactData()->setEmail($newPageContent->getContactData()->getEmail());
                    $content->getContactData()->setTelephone($newPageContent->getContactData()->getTelephone());
                }
                else if ($content->getType() == self::CONTENT_TYPE_MAP) {
                    $content->getMapData()->setLatitude($newPageContent->getMapData()->getLatitude());
                    $content->getMapData()->setLongitude($newPageContent->getMapData()->getLongitude());
                }
            } else {
                $updatedPage->removeContent($content);
                $this->customPageContentRepository->remove($content);
            }
        }

        //add new content
        foreach ($page->getContent() as $content) {
            if (!$content->getId()) {
                $updatedPage->addContent($content);
            }
        }
    }

    /**
     * @param int $contentId
     * @param CustomPage $page
     * @return null | CustomPageContent
     */
    private function getNewPageContent($contentId, $page)
    {
        foreach ($page->getContent() as $content) {
            if ($contentId == $content->getId()) {
                return $content;
            }
        }

        return null;
    }

    /**
     * @return UserInterface|false
     */
    protected function getUser()
    {
        if (!$this->authorization->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw new AccessDeniedException();
        }
        
        return $this->tokenStorage->getToken()->getUser();
    }
}
