angular.module('registreoApp')
    .service('PaginService', function() {

        this.craetePagin = function(pagin) {
            return {
                visible: this.lastPage(pagin.limit, pagin.total) > 1,
                current: pagin.current,
                next: this.lastPage(pagin.limit, pagin.total) > pagin.current,
                prev: pagin.current > 1,
                pages: this.getPages(pagin.current, pagin.limit, pagin.total)
            };
        };

        this.getPages = function(current, limit, total) {
            
            var startPage = current - 3;
            if (startPage < 1) {
                startPage = 1;
            }
            var lastPage = startPage + 6;
            if (lastPage > this.lastPage(limit, total)) {
                lastPage = this.lastPage(limit, total);
                startPage = lastPage - 6;
                if (startPage < 1) {
                    startPage = 1;
                }
            }
            var pages = [];
            for (var i = startPage; i <= lastPage; i++) {
                pages.push(i);
            }
            return pages;
        };

        
        this.lastPage = function(limit, total) {
            return Math.ceil(total / limit);
        };
        
                
    });


