<?php
namespace Registreo\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Registreo\PaymentBundle\Repository\TransactionLogRepository")
 * @ORM\Table(name="transaction_log")
 */
class TransactionLog 
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=30)
     */
    protected $number;


    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=100)
     */
    protected $status;

    /**
     * @var string
     *
     * @ORM\Column(name="control", type="string", length=100)
     */
    protected $control;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float")
     */
    protected $ammount;

    /**
     * @var float
     *
     * @ORM\Column(name="commission", type="float")
     */
    protected $commission;

    /**
     * @var string
     *
     * @ORM\Column(name="log", type="string", length=4000)
     */
    protected $log;

    /**
     * @var bool
     *
     * @ORM\Column(name="dirty", type="boolean")
     */
    protected $dirty;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="added", type="datetime")
     * @Assert\GreaterThan("today", message="event.startDate.greater_than_today")
     */
    protected $added;

    function getId()
    {
        return $this->id;
    }

    function getNumber()
    {
        return $this->number;
    }

    function getLog()
    {
        return $this->log;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setNumber($number)
    {
        $this->number = $number;
    }

    function setLog($log)
    {
        $this->log = $log;
    }

    function getDirty()
    {
        return $this->dirty;
    }

    function setDirty($dirty)
    {
        $this->dirty = $dirty;
    }

    function getAmmount()
    {
        return $this->ammount;
    }

    function getCommission()
    {
        return $this->commission;
    }

    function setAmmount($ammount)
    {
        $this->ammount = $ammount;
    }

    function setCommission($commission)
    {
        $this->commission = $commission;
    }

    function getStatus()
    {
        return $this->status;
    }

    function setStatus($status)
    {
        $this->status = $status;
    }

    function getControl()
    {
        return $this->control;
    }

    function setControl($control)
    {
        $this->control = $control;
    }

    function getAdded()
    {
        return $this->added;
    }

    function setAdded(\DateTime $added)
    {
        $this->added = $added;
    }




}