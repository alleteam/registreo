angular.module('registreoAppCustom')
    .service('PaymentService', ['Restangular', '$q', function(restangular, q) {
    
        this.getPaymentDataForm = function (participantId) {
            var defer = q.defer();
            restangular.one('event/payment-data/' + participantId).get().then(
                function (data) {
                    defer.resolve(data);
                }
            );
            return defer.promise;
        };
                    
                    
    }]);

