angular.module('registreoApp')
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('newaccount', {
                url: "/newaccount",
                controller: 'NewAccountCtrl',
                controllerAs: 'newAccountCtrl',
                templateUrl: "app/user/views/newUserAccount.html"
            });    
        }
    ])
    .controller('NewAccountCtrl',['$scope', '$rootScope', 'Breadcrumb', '$location', '$http',
        function ($scope, $rootScope, $Breadcrumb) {
            $rootScope.$broadcast('activeMenu', '');

            $rootScope.ngview.show = true;
            $Breadcrumb.set('Rejestracja', [{name: 'Rejestracja', uri: '/signup'}]);


        }
    ]);



