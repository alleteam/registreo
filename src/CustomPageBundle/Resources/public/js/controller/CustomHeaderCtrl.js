angular.module('registreoAppCustom')
    .controller('CustomHeaderCtrl',['$rootScope', '$scope', '$location', '$state',
        function ($rootScope, $scope, $location, $state) {

            $scope.eventName = "#";

            $rootScope.$on('$stateChangeSuccess', function() {
              $scope.eventName = $state.params.eventName;
            });


            $scope.home = function() {
                $location.path('/');
                location.reload('/');
            };

            $scope.isActive = function(url) {
                return ("/" + url == $location.$$path) ? 'active' : '';
            };

        }
    ]);
