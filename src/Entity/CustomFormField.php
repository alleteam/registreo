<?php
namespace Registreo\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as ORMAssert;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\Exclude;

/**
 * Class CustomFormField
 *
 * @ORM\Entity(repositoryClass="Registreo\EventBundle\Repository\CustomFormFieldRepository")
 * @ORM\Table(name="custom_form_fields")
 */
class CustomFormField
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CustomForm", inversedBy="fields")
     * @ORM\JoinColumn(name="custom_form_id", referencedColumnName="id")
     * @Exclude
     */
    protected $customForm;

    /**
     * @var int
     *
     * @ORM\Column(name="order_by", type="integer")
     * @Assert\NotBlank
     */
    protected $order;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=50)
     * @Assert\NotBlank
     */
    protected $label;

    /**
     * @var string
     *
     * @ORM\Column(name="tooltip", type="string", length=50, nullable=true)
     */
    protected $tooltip;

    /**
     * @var bool
     *
     * @ORM\Column(name="editable", type="boolean")
     * @Assert\NotBlank
     */
    protected $editable;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer")
     * @Assert\NotBlank
     */
    protected $type;

    /**
     * @var CustomFormFieldOption[]
     *
     * @ORM\OneToMany(targetEntity="CustomFormFieldOption", mappedBy="customFormField", cascade={"persist", "remove"})
     */
    protected $options;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCustomForm()
    {
        return $this->customForm;
    }

    /**
     * @param mixed $customForm
     */
    public function setCustomForm($customForm)
    {
        $this->customForm = $customForm;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getTooltip()
    {
        return $this->tooltip;
    }

    /**
     * @param string $tooltip
     */
    public function setTooltip($tooltip)
    {
        $this->tooltip = $tooltip;
    }

    /**
     * @return boolean
     */
    public function isEditable()
    {
        return $this->editable;
    }

    /**
     * @param boolean $editable
     */
    public function setEditable($editable)
    {
        $this->editable = $editable;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return CustomFormFieldOption[]
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param CustomFormFieldOption[] $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }
}
