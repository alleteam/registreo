<?php
namespace Registreo\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Registreo\PaymentBundle\Repository\EventTransactionRepository")
 * @ORM\Table(name="event_transactions")
 */
class EventTransaction
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Event
     *
     * @ORM\ManyToOne(targetEntity="Event")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id", nullable=false)
     * @JMS\Exclude
     */
    protected $event;

    /**
     * @var Participant
     *
     * @ORM\ManyToOne(targetEntity="Participant")
     * @ORM\JoinColumn(name="participant_id", referencedColumnName="id", nullable=false)
     */
    protected $participant;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=30)
     */
    protected $number;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float")
     */
    protected $amount;

    /**
     * @var float
     *
     * @ORM\Column(name="dotpayCommission", type="float")
     */
    protected $dotpayCommission;

    /**
     * @var float
     *
     * @ORM\Column(name="percent", type="float")
     */
    protected $percent;

    /**
     * @var float
     *
     * @ORM\Column(name="commission", type="float")
     */
    protected $commission;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="added", type="datetime")
     */
    protected $added;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    protected $modified;

    function getId()
    {
        return $this->id;
    }

    function getParticipant()
    {
        return $this->participant;
    }

    function getNumber()
    {
        return $this->number;
    }

    function getAmount()
    {
        return $this->amount;
    }

    function getDotpayCommission()
    {
        return $this->dotpayCommission;
    }

    function getPercent()
    {
        return $this->percent;
    }

    function getCommission()
    {
        return $this->commission;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setNumber($number)
    {
        $this->number = $number;
    }

    function setAmount($amount)
    {
        $this->amount = $amount;
    }

    function setDotpayCommission($dotpayCommission)
    {
        $this->dotpayCommission = $dotpayCommission;
    }

    function setPercent($percent)
    {
        $this->percent = $percent;
    }

    function setCommission($commission)
    {
        $this->commission = $commission;
    }

    function getEvent()
    {
        return $this->event;
    }

    function setEvent(Event $event)
    {
        $this->event = $event;
    }

    function setParticipant(Participant $participant)
    {
        $this->participant = $participant;
    }
    
    function getAdded()
    {
        return $this->added;
    }

    function getModified()
    {
        return $this->modified;
    }

    function setAdded(\DateTime $added)
    {
        $this->added = $added;
    }

    function setModified(\DateTime $modified)
    {
        $this->modified = $modified;
    }

}