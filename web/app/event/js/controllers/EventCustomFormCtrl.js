angular.module('registreoApp.eventCustomFormController', [])
    .controller('EventCustomFormCtrl',['$scope', '$rootScope', 'Breadcrumb', '$location', '$stateParams', 'CustomPageService', '$uibModal', '$element', 'NotificationsService',
        function ($scope, $rootScope, $Breadcrumb, $location, $stateParams, CustomPageService, $uibModal, $element, NotificationsService) {
            $rootScope.$broadcast('activeMenu', 'myaccount');
            $rootScope.ngview.show = true;
            $Breadcrumb.set('Formularz wydarzenia', [
                {name: 'Moje konto', uri: '/myaccount'}, {name: 'Moje wydarzenia', uri: '/myevents'},
                {name: 'Formularz wydarzenia', uri: '/event/'+ $stateParams.eventId +'/form'}
            ]);
            window.scrollTo(0, 0);
            $scope.customFormLoading = true;

            CustomPageService.getEventCustomForm($stateParams.eventId).then(function (form) {
                $scope.form = form;
                $scope.customFormLoading = false;
            });

            $scope.addField = function(){
                var form = $scope.form.fields;
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/event/views/addFormField.html',
                    controller: function ($scope, $uibModalInstance) {
                        //$rootScope.ngview.show = true;
                        var selectedField = $element.find('#selectElement');
                        $scope.noOption = false;
                        $scope.emptyOption = false;
                        $scope.optionValue = "";
                        $scope.field = {
                            "order" : form.length + 1,
                            "type" : selectedField.val(),
                            "editable" : 1,
                            "options" : []
                        };
                        $scope.closeModal = function () {
                            $uibModalInstance.close();
                        };
                        $scope.saveField = function (isValid) {
                            var validateSelectResult = $scope.validateSelectOptions();
                            if(isValid && validateSelectResult) {
                                form.push($scope.field);
                                $uibModalInstance.close();
                            }
                        };

                        $scope.removeOption = function(option){
                            angular.forEach($scope.field.options, function (value, key) {
                                if(option.id ==  value.id) {
                                    $scope.field.options.splice(key, 1);
                                }
                            });
                        };

                        $scope.addOption = function() {
                            $scope.noOption = false;
                            $scope.emptyOption = false;
                            if (!$scope.optionValue) {
                                $scope.emptyOption = true;
                            } else {
                                $scope.emptyOption = false;
                                $scope.field.options.push({
                                    "value" : $scope.optionValue,
                                    "order" : $scope.field.options.length + 1
                                });
                                $scope.optionValue = "";
                            }
                        };

                        $scope.validateSelectOptions = function() {
                            if (selectedField.val() == 3 && !$scope.field.options.length) {
                                $scope.noOption = true;
                                return false;
                            }
                            $scope.noOption = false;
                            return true;
                        };

                        $scope.sortableSelectOptions = {
                            stop : function(e, ui) {
                                var i = 1;
                                angular.forEach($scope.field.options, function (value, key) {
                                    value.order = i;
                                    i++;
                                });
                            }
                        };
                    },
                    size: "",
                    windowClass: 'center-modal'
                });
            };

            $scope.editField = function(event, field){
                var form = $scope.form.fields;
                var oldField = angular.copy(field);

                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/event/views/editFormField.html',
                    controller: function ($scope, $uibModalInstance) {
                        $scope.noOption = false;
                        $scope.emptyOption = false;
                        $scope.optionValue = "";
                        $scope.field = field;

                        $scope.closeModal = function () {
                            angular.forEach(form, function (value, key) {
                                if(value.id ==  $scope.field.id) {
                                    form[key] = oldField;
                                }
                            });
                            $uibModalInstance.close();
                        };

                        $scope.saveField = function (isValid) {
                            var validateSelectResult = $scope.validateSelectOptions();
                            if(isValid && validateSelectResult) {
                                $uibModalInstance.close();
                            }
                        };

                        $scope.removeOption = function(option){
                            angular.forEach($scope.field.options, function (value, key) {
                                if(option.value ==  value.value) {
                                    $scope.field.options.splice(key, 1);
                                }
                            });
                        };

                        $scope.addOption = function() {
                            $scope.noOption = false;
                            $scope.emptyOption = false;
                            if (!$scope.optionValue) {
                                $scope.emptyOption = true;
                            } else {
                                $scope.emptyOption = false;
                                $scope.field.options.push({
                                    "value" : $scope.optionValue,
                                    "order" : $scope.field.options.length + 1
                                });
                                $scope.optionValue = "";
                            }
                        };

                        $scope.validateSelectOptions = function() {
                            if ($scope.field.type == 3 && !$scope.field.options.length) {
                                $scope.noOption = true;
                                return false;
                            }
                            $scope.noOption = false;
                            return true;
                        };

                        $scope.sortableSelectOptions = {
                            stop : function(e, ui) {
                                var i = 1;
                                angular.forEach($scope.field.options, function (value, key) {
                                    value.order = i;
                                    i++;
                                });
                            }
                        };
                    },
                    size: "",
                    windowClass: 'center-modal'
                });

                modalInstance.result.then(function () {

                }, function () {
                    angular.forEach(form, function (value, key) {
                        if(value.id ==  field.id) {
                            form[key] = oldField;
                        }
                    });
                });
            };

            $scope.removeField = function(event, field){
                angular.forEach($scope.form.fields, function (value, key) {
                    if(field.id ==  value.id) {
                        $scope.form.fields.splice(key, 1);
                    }
                });
            };

            $scope.sortableOptions = {
                stop : function(e, ui) {
                    var i = 1;
                    angular.forEach($scope.form.fields, function (value, key) {
                        value.order = i;
                        i++;
                    });
                }
            };

            $scope.saveForm = function() {
                $scope.savingForm = true;
                $scope.form.active = 0;
                CustomPageService.saveEventCustomForm($stateParams.eventId, $scope.form).then(function (form) {
                    $scope.savingForm = false;
                    window.scrollTo(0, 0);
                    NotificationsService.addNotification('Twoje zmiany zostały zapisane.');
                });
            };

            $scope.activateForm = function() {
                var form = $scope.form;
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'app/event/views/activateForm.html',
                    controller: function ($scope, $uibModalInstance) {
                        $scope.closeModal = function () {
                            $uibModalInstance.close();
                        };

                        $scope.activate = function () {
                            $scope.activatingForm = true;
                            form.active = 1;
                            CustomPageService.saveEventCustomForm($stateParams.eventId, form).then(function (form) {
                                $scope.activatingForm = false;
                                $uibModalInstance.close();
                                NotificationsService.addNotification('Formularz zgłoszeniowy został aktywowany.');
                                window.scrollTo(0, 0);
                            });
                        };
                    },
                    size: "",
                    windowClass: 'center-modal'
                });
            };
        }
    ]);
