<?php
namespace Registreo\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as ORMAssert;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * Class CustomPageContent
 *
 * @ORM\Entity(repositoryClass="Registreo\EventBundle\Repository\CustomPageContentRepository")
 * @ORM\Table(name="custom_page_content")
 */
class CustomPageContent
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="CustomPage", inversedBy="content")
     * @ORM\JoinColumn(name="custom_page_id", referencedColumnName="id")
     */
    protected $customPage;

    /**
     * @var int
     *
     * @ORM\Column(name="order_by", type="integer")
     * @Assert\NotBlank
     */
    protected $order;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer")
     * @Assert\NotBlank
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50)
     * @Assert\NotBlank
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    protected $description;

    /**
     * @var ContentContactData
     *
     * @ORM\OneToOne(targetEntity="ContentContactData", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="contact_data_id", referencedColumnName="id")
     */
    protected $contactData;

    /**
     * @var ContentMapData
     *
     * @ORM\OneToOne(targetEntity="ContentMapData", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="map_data_id", referencedColumnName="id")
     */
    protected $mapData;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCustomPage()
    {
        return $this->customPage;
    }

    /**
     * @param mixed $customPage
     */
    public function setCustomPage($customPage)
    {
        $this->customPage = $customPage;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return ContentContactData
     */
    public function getContactData()
    {
        return $this->contactData;
    }

    /**
     * @param ContentContactData $contactData
     */
    public function setContactData($contactData)
    {
        $this->contactData = $contactData;
    }

    /**
     * @return ContentMapData
     */
    public function getMapData()
    {
        return $this->mapData;
    }

    /**
     * @param ContentMapData $mapData
     */
    public function setMapData($mapData)
    {
        $this->mapData = $mapData;
    }
}