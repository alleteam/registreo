<?php

namespace Registreo\CustomPageBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use JMS\Serializer\SerializationContext;
use Registreo\CustomPageBundle\Service\ApplicationService;
use Registreo\Entity\Event;
use Registreo\EventBundle\Service\EventService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Registreo\AppBundle\Exception\ResourceNotFoundException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as WEB;
use FOS\RestBundle\Controller\Annotations\QueryParam;

class CustomPageController extends FOSRestController
{
    /**
     * @var ApplicationService
     */
    private $applicationService;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param ApplicationService $applicationService
     */
    public function setApplicationService(ApplicationService $applicationService)
    {
        $this->applicationService = $applicationService;
    }

    /**
     * @Rest\Route("/events/{event}/applications")
     * @WEB\ParamConverter("event", class="Registreo:Event")
     *
     * @param Event   $event
     * @param Request $request
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function postSaveCustomFormAction(Event $event, Request $request)
    {
        if (empty($event)) {
            throw new ResourceNotFoundException('Event not found');
        }

        $applicationForm = json_decode($request->getContent());

        if (!$applicationForm->regulation
            || !$applicationForm->regulation->general
            || !$applicationForm->regulation->data
        ) {
            throw new ResourceNotFoundException('Regulations not accepted');
        }

        $participant = $this->applicationService->saveApplicationForm($event, $applicationForm, $request->getSession()->get('participant'));

        return $this->handleView($this->view($participant));
    }

    /**
     * @Rest\Route("/events/{event}/participants/applications")
     * @Web\Method({"GET"})
     * @WEB\ParamConverter("event", class="Registreo:Event")
     * @QueryParam(name="page", requirements="\d+", default="1", description="Page of the overview.")
     * @QueryParam(name="limit", requirements="\d+", default="1", description="Limit of results on page.")
     *
     * @param Event $event
     * @param ParamFetcher $paramFetcher
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function getParticipantsApplicationsAction(Event $event, ParamFetcher $paramFetcher)
    {
        if (empty($event) || !$event->getCustomPage()->isListView()) {
            throw new ResourceNotFoundException('Event not found');
        }

        $page = $paramFetcher->get('page');
        $limit = $paramFetcher->get('limit');

        $view = $this->view([$this->applicationService->getParticipantsList($page, $limit, $event)]);
        $view->setSerializationContext(SerializationContext::create()->setGroups(array('list')));

        return $this->handleView($view);
    }
}
