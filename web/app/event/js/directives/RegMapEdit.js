angular.module('registreoApp')
    .directive('regMapEdit',['$window', function($window) {
        return {
            restrict: 'E',
            scope: {
                refresh: '=onShow',
                lat: '=lat',
                lng: '=lng'
            },
            link: function(scope, element) {
                
                scope.lat = !scope.lat ? 52 : scope.lat;
                scope.lng = !scope.lng ? 21 : scope.lng;
                
                var id = Math.ceil(Math.random() * 1000000000);
                element.attr('id', id);
                scope.map = false;

                scope.createMap = function() {
                    if (!scope.map) {
                        scope.map = new google.maps.Map(element[0], {
                            center: {lat: scope.lat, lng: scope.lng},
                            zoom: 6
                        });
                        
                        scope.marker = new google.maps.Marker({
                            position: {lat: scope.lat, lng: scope.lng},
                            map: scope.map,
                            draggable: true,
                            animation: google.maps.Animation.DROP,
                            title: 'Oznacz miejsce twojego wydarzenia'
                        });
                        
                        scope.marker.addListener('dragend', function() {
                           scope.lat = scope.marker.getPosition().lat();
                           scope.lng = scope.marker.getPosition().lng();
                        });
                        
                        
                        google.maps.event.addListener(scope.map, "idle", function(){
                            google.maps.event.trigger(scope.map, 'resize'); 
                        });
                    }
                    
                };
                
                scope.$watch('refresh', function(value) {
                    if (value) {
                        scope.createMap();
                    } 
                });
            }
        };
        
    }]);