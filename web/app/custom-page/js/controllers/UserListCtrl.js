angular.module('registreoAppCustom')
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('list', {
                url: "/{eventName}/list",
                controller: 'CustomPageListCtrl',
                controllerAs: 'customPageListCtrl',
                templateUrl: '/app/custom-page/views/list.html'
            });    
        }
    ])
    .controller('CustomPageListCtrl',['$rootScope', 'CustomPageService', '$scope', 'PaginService',
        function ($rootScope, CustomPageService, $scope, $PaginService) {
            $rootScope.ngview.show = true;
            window.scrollTo(0, 0);

            var self = this;
            $scope.loadParticipants = true;
            $scope.participants = [];
            $scope.limit = 50;
            $scope.pagin = {
                visible: false,
                current: 1
            };
            $scope.freeAccount = $rootScope.freeAccount;

            self.refreshList = function() {
                $scope.loadParticipants = true;
                CustomPageService.getApplicationsParticipants($rootScope.eId, $scope.pagin.current, $scope.limit).then(function (data) {
                    $scope.totalResult = data[0].pagin.total;
                    $scope.page = data[0].pagin.current;
                    $scope.pagin = $PaginService.craetePagin(data[0].pagin);
                    $scope.pagin.current = parseInt($scope.pagin.current);
                    var list = data[0].list;
                    for (var i in list) {
                        list[i].statusName = self.getPaymentStatusName(list[i].payment_status);
                    }
                    $scope.participants = list;
                    $scope.loadParticipants = false;
                    window.scrollTo(0, 0);
                });
            };

            $scope.setPage = function(current) {
                $scope.pagin.current = current;
                self.refreshList();
            };


            self.getPaymentStatusName = function(status) {
                if (status == 3) {
                    return "opłacony";
                } else if (status == 10) {
                    return "zweryfikowany";
                }

                return "nieopłacony";
            };

            self.refreshList();

        }
    ]);

