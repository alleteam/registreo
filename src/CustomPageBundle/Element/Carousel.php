<?php
namespace Registreo\CustomPageBundle\Element;

use JMS\Serializer\Annotation as JMS;

class Carousel extends ElementAbstract implements ElementTemplate
{
    /**
     * @JMS\Exclude
     */
    public static $TEMPLATE = '<reg-carousel count="templateData.section_list[#index#].element.count" '
        . ' data="templateData.section_list[#index#].element.brand_list"></reg-carousel>';


    /**
     * @JMS\Type("integer")
     * @var int
     */
    private $count;

    /**
     * @JMS\Type("array<Registreo\CustomPageBundle\Element\Brand>")
     * @var array
     */
    private $brandList;

    public function getCount()
    {
        return $this->count;
    }

    public function getBrandList()
    {
        return $this->brandList;
    }

    public function setCount($count)
    {
        $this->count = $count;
        return $this;
    }

    public function setBrandList($brandList)
    {
        $this->brandList = $brandList;
        return $this;
    }

    public function getTemplate()
    {
        return self::$TEMPLATE;
    }
}