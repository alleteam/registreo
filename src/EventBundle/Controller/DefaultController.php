<?php

namespace Registreo\EventBundle\Controller;

use Exception;
use JMS\Serializer\SerializationContext;
use Registreo\AppBundle\Exception\ResourceNotFoundException;
use Registreo\AppBundle\Exception\ValidationException;
use Registreo\CustomPageBundle\Service\ApplicationService;
use Registreo\Entity\CustomForm;
use Registreo\Entity\CustomPage;
use Registreo\Entity\Event;
use Registreo\Entity\SeoLink;
use Registreo\Entity\Participant;
use Registreo\EventBundle\Service\EventService;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as WEB;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\QueryParam;

/**
 * @WEB\Security("has_role('ROLE_USER')")
 */
class DefaultController extends FOSRestController
{
    /**
     * @var EventService
     */
    private $eventService;

    /**
     * @var ApplicationService
     */
    private $applicationService;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param EventService $eventService
     */
    public function setEventService(EventService $eventService)
    {
        $this->eventService = $eventService;
    }

    /**
     * @param ApplicationService $applicationService
     */
    public function setApplicationService(ApplicationService $applicationService)
    {
        $this->applicationService = $applicationService;
    }

    /**
     * @Rest\Route("/events")
     * @Web\Method({"GET"})
     * 
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function getEventsAction()
    {
        return $this->handleView($this->view($this->eventService->getEvents()));
    }

    /**
     * @Rest\Route("/events/{event}/custom-page")
     * @Web\Method({"GET"})
     * @WEB\ParamConverter("event", class="Registreo:Event")
     *
     * @param Event $event
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function getEventCustomPageAction(Event $event)
    {
        if (empty($event)) {
            throw new ResourceNotFoundException('Event not found');
        }

        return $this->handleView($this->view($event->getCustomPage()));
    }

    /**
     * @Rest\Route("/events/{event}/custom-page")
     * @Web\Method({"PUT"})
     * @WEB\ParamConverter("event", class="Registreo:Event")
     * @WEB\ParamConverter("page", converter="fos_rest.request_body")
     *
     * @param Event $event
     * @param CustomPage $page
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function putEventCustomPageAction(Event $event, CustomPage $page)
    {
        if (empty($event) || $this->getUser()->getId() != $event->getUser()->getId()) {
            throw new ResourceNotFoundException('Event not found');
        }
        try {
            $this->eventService->saveCustomPage($page);
            return $this->handleView($this->view(null, 204));
        } catch (ValidationException $e) {
            return $this->handleView($this->view($e->getValidationErrors(), 400));
        }
    }

    /**
     * @Rest\Route("/events/{event}/custom-form")
     * @Web\Method({"GET"})
     * @WEB\ParamConverter("event", class="Registreo:Event")
     *
     * @param Event $event
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function getEventCustomFormAction(Event $event)
    {
        if (empty($event)) {
            throw new ResourceNotFoundException('Event not found');
        }
        return $this->handleView($this->view($event->getCustomForm()));
    }

    /**
     * @Rest\Route("/events/{event}/custom-form")
     * @Web\Method({"PUT"})
     * @WEB\ParamConverter("event", class="Registreo:Event")
     * @WEB\ParamConverter("form", converter="fos_rest.request_body")
     *
     * @param Event $event
     * @param CustomForm $form
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function putEventCustomFormAction(Event $event, CustomForm $form)
    {
        if (empty($event) || $this->getUser()->getId() != $event->getUser()->getId()) {
            throw new ResourceNotFoundException('Event not found');
        }

        if ($event->getCustomForm()->isActive()) {
            return $this->handleView($this->view('Form cannot be edit when it has an active state', 403));
        }

        try {
            $this->eventService->saveCustomForm($form);
            return $this->handleView($this->view(null, 204));
        } catch (ValidationException $e) {
            return $this->handleView($this->view($e->getValidationErrors(), 400));
        }
    }

    /**
     * @Rest\Route("/events")
     * @WEB\ParamConverter("event", converter="fos_rest.request_body")
     *
     * @param Event $event
     * @param Request $request
     * @return Response
     * @throws Exception
     * @throws ResourceNotFoundException
     */
    public function postEventsAction(Event $event, Request $request)
    {
        if (empty($event) || empty($event->getSeoLink())) {
            throw new ResourceNotFoundException('Event not found');
        }

        if (!$request->get('regulation_accept')) {
            throw new Exception("Akceptacja regulaminu jest wymagana.");
        } else {
            $event->setRegulation(new \DateTime());
        }

        try {
            $this->eventService->createEvent($event);
            return $this->handleView($this->view($event->getId(), 200));
        } catch (ValidationException $e) {
            return $this->handleView($this->view($e->getValidationErrors(), 400));
        } catch (\RuntimeException $e) {
            return $this->handleView($this->view($e->getMessage(), 409));
        }
    }


    /**
     * @Rest\Route("/events")
     * @WEB\ParamConverter("event", converter="fos_rest.request_body")
     *
     * @param Event $event
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function putEventsAction(Event $event)
    {
        $newEvent = $this->eventService->getEvent($event->getId());
        if (empty($newEvent) || $this->getUser()->getId() != $newEvent->getUser()->getId()) {
            throw new ResourceNotFoundException('Event not found');
        }

        $newEvent->setName($event->getName());
        $newEvent->setDescription($event->getDescription());
        $newEvent->setFee($event->getFee());
        $newEvent->setLimit($event->getLimit());
        $newEvent->setStartDate($event->getStartDate());
        $newEvent->setRegistrationDate($event->getRegistrationDate());
        $newEvent->setBankName($event->getBankName());
        $newEvent->setAccountNumber($event->getAccountNumber());

        try {
            $this->eventService->createEvent($newEvent);
            return $this->handleView($this->view($newEvent->getId(), 200));
        } catch (ValidationException $e) {
            return $this->handleView($this->view($e->getValidationErrors(), 400));
        }
    }

    /**
     * @Rest\Route("/events/{eventId}")
     *
     * fuck param converter - don't know how to get this working
     *
     * @param int $eventId
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function getEventAction($eventId)
    {
        try {
            $event = $this->eventService->getEvent($eventId);
            if (empty($event) || $this->getUser()->getId() != $event->getUser()->getId()) {
                throw new ResourceNotFoundException('Event not found');
            }

            return $this->handleView($this->view($event, 200));
        } catch (ValidationException $e) {
            throw new ResourceNotFoundException('Event not found');
        }
    }

    /**
     * @Rest\Route("/events/{event}/participants")
     * @Web\Method({"GET"})
     * @WEB\ParamConverter("event", class="Registreo:Event")
     * @QueryParam(name="page", requirements="\d+", default="1", description="Page of the overview.")
     * @QueryParam(name="limit", requirements="\d+", default="1", description="Limit of results on page.")
     *
     * @param Event $event
     * @param ParamFetcher $paramFetcher
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function getParticipantsAction(Event $event, ParamFetcher $paramFetcher)
    {
        if (empty($event) || $this->getUser()->getId() != $event->getUser()->getId()) {
            throw new ResourceNotFoundException('Event not found');
        }

        $page = $paramFetcher->get('page');
        $limit = $paramFetcher->get('limit');

        $view = $this->view([$this->applicationService->getParticipantsList($page, $limit, $event)]);
        $view->setSerializationContext(SerializationContext::create()->setGroups(array('admin_list')));

        return $this->handleView($view);
    }

    /**
     * @Rest\Route("/events/{event}/participants/{participant}")
     * @Web\Method({"GET"})
     * @WEB\ParamConverter("event", class="Registreo:Event")
     * @WEB\ParamConverter("participant", class="Registreo:Participant")
     *
     * @param Event $event
     * @param Participant $participant
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function getParticipantDetailsAction(Event $event, Participant $participant)
    {
        if (empty($event) || $participant->getEvent()->getId() != $event->getId()) {
            throw new ResourceNotFoundException('Event not found');
        }

        $applicationFields = $this->applicationService->getApplicationFields($participant->getId());
        return $this->handleView($this->view($applicationFields));
    }

    /**
     * @Rest\Route("/events/{event}/participants/{participant}/payments")
     * @Web\Method({"PUT"})
     * @WEB\ParamConverter("event", class="Registreo:Event")
     * @WEB\ParamConverter("participant", class="Registreo:Participant")
     *
     * @param Event $event
     * @param Participant $participant
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function putParticipantPaymentStatusAction(Event $event, Participant $participant)
    {
        if (empty($event) || $participant->getEvent()->getId() != $event->getId()) {
            throw new ResourceNotFoundException('Event not found');
        }

        $this->applicationService->changePaymentStatus($participant, 3);

        return $this->handleView($this->view(null, 204));
    }

    /**
     * @Rest\Route("/events/{event}/participants/invite")
     * @Web\Method({"POST"})
     * @WEB\ParamConverter("event", class="Registreo:Event")
     * @WEB\ParamConverter("participant", converter="fos_rest.request_body")
     *
     * @param Event $event
     * @param Participant $participant
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function postInviteParticipantAction(Event $event, Participant $participant)
    {
        if (empty($event)) {
            throw new ResourceNotFoundException('Event not found');
        }
        $result = $this->applicationService->inviteParticipant($event, $participant);
        return $this->handleView($this->view($result, 201));
    }


    /**
     * @Rest\Route("/events/{event}/status")
     * @Web\Method({"PUT"})
     * @WEB\ParamConverter("event", class="Registreo:Event")
     *
     * @param Event $event
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function putEventStatusAction(Event $event)
    {
        if (empty($event) || $event->getUser()->getId() != $this->getUser()->getId()) {
            throw new ResourceNotFoundException('Event not found');
        }

        $event->setStatus(0);
        $this->eventService->getEventRepository()->save($event);

        return $this->handleView($this->view(null, 204));
    }

    /**
     * @Rest\Route("/events/iban")
     *
     * @param Request $request
     * @return Response
     */
    public function postEventsIbanAction(Request $request)
    {
        $validator = $this->eventService->getValidator();
        $errors = $validator->validatePropertyValue(new Event(), 'accountNumber', $request->get('bankAccount'));

        if (!strstr($request->get('bankAccount'), 'PL') || $errors->count()) {
            return $this->handleView($this->view("error", 200));
        }

        return $this->handleView($this->view(null, 204));
    }
}
