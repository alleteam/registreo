<?php

namespace Registreo\UserBundle\Service;

use Registreo\Entity\User;
use Registreo\UserBundle\Repository\UserRepository;

class UserService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function setUserRepository(UserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    /**
     * @return UserRepository
     */
    public function getUserRepository() {
        return $this->userRepository;
    }

    /**
     * @param string $token
     * @return int
     */
    public function activateUser($token) {
        /** @var User $user */
        $user = $this->getUserRepository()->findOneByConfirmationToken($token);
        if (!$user) {
            return 0;
        }

        $user->setEnabled(true);
        $this->getUserRepository()->save($user);

        return 1;
    }
}
