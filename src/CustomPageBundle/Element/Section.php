<?php
namespace Registreo\CustomPageBundle\Element;

use JMS\Serializer\Annotation as JMS;

class Section implements ElementTemplate
{
    /**
     * @JMS\Type("string")
     * @var string
     */
    private $class;

    /**
     * @JMS\Type("Registreo\CustomPageBundle\Element\ElementAbstract")
     * @var mixed
     */
    private $element;


    public function getType()
    {
        return $this->type;
    }

    public function getElement()
    {
        return $this->element;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function setElement($element)
    {
        $this->element = $element;
        return $this;
    }

    public function getClass()
    {
        return $this->class;
    }

    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }


    public function getTemplate()
    {
        return sprintf("<div class=\"%s\">%s</div>",  $this->class, $this->element ? $this->element->getTemplate() : '');
    }

    public function getPartOfPage() {
        if(preg_match('/col-md-(\d+)/', $this->class, $matches)) {
            return $matches[1];
        }
        return 12;
    }
}