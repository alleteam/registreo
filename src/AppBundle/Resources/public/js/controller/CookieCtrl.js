angular.module('registreoApp')
    .controller('CookieCtrl',['$scope', '$cookies',
        function ($scope, $cookies) {
            if ($cookies.get('registreo-cookie-policy') != 1) {
                $scope.readPolicy = 1;
            }

            $scope.closeCookieInfo = function() {
                $cookies.put('registreo-cookie-policy', 1);
                $scope.readPolicy = 0;
            }
        }
    ]);
