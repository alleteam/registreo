<?php

namespace Registreo\UserBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Registreo\Entity\User;

/**
 * @codeCoverageIgnore
 */
class UserRepository extends EntityRepository
{
    /**
     * @param User $user
     *
     * @return User
     */
    public function save($user)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($user);
        $entityManager->flush($user);

        return $user;
    }
}
