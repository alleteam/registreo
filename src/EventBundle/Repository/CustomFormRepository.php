<?php

namespace Registreo\EventBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Registreo\Entity\CustomForm;

class CustomFormRepository extends EntityRepository
{
    /**
     * @param CustomForm $form
     */
    public function save(CustomForm $form)
    {
        $this->getEntityManager()->persist($form);
        $this->getEntityManager()->flush();
    }
}
