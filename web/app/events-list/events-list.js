(function () {
    angular.module('registreoApp.eventsList', ['registreoApp.eventsListController'])
        .config(config);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('events', {
                url: '/events',
                templateUrl: 'app/events-list/events-list.html',
                controller: 'EventsListController',
                controllerAs: 'eventsListCtrl'
            });
    }
})();
