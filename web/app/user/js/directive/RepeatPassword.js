angular.module('registreoApp')
    .directive('repeatPassword', [function () {
        return {
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                var firstPassword = '#' + attrs.repeatPassword;
                elem.add(firstPassword).on('keyup', function () {
                    scope.$apply(function () {
                        var v = elem.val()===$(firstPassword).val();
                        ctrl.$setValidity('repeatPassword', v);
                    });
                });
            }
        };
    }]);

