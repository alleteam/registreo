<?php
namespace Registreo\PaymentBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Registreo\Entity\EventPayout;
use Registreo\Entity\Event;

class EventPayoutRepository extends EntityRepository
{
    /**
     * @param EventPayout $event
     */
    public function save(EventPayout $event)
    {
        $this->getEntityManager()->persist($event);
        $this->getEntityManager()->flush();
    }

    public function eventSummary(Event $event)
    {
        $query = $this->createQueryBuilder('t');
        $query->select('SUM(t.amount), SUM(t.dotpayCommission)');
        $query->where('t.event = :event and t.status <> :status');
        $query->setParameter('event', $event);
        $query->setParameter('status', 'rejected');
        return $query->getQuery()->getSingleResult();
    }

    public function getList(Event $event)
    {
        return $this->findBy(['event' => $event],['modified' => 'DESC']);
    }
}