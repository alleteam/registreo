<?php

namespace Registreo\CustomPageBundle\Controller;

use DateTime;
use Registreo\CustomPageBundle\Element;

use Registreo\AppBundle\Exception\ResourceNotFoundException;
use Registreo\Entity\CustomPage;
use Registreo\Entity\CustomPageBanner;
use Registreo\Entity\Event;
use Registreo\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Registreo\CustomPageBundle\Element\Banner;

class DefaultController extends Controller
{
    /**
     * @Template()
     *
     * @param int $id
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function indexAction($id, \Symfony\Component\HttpFoundation\Request $request)
    {
        /* @var Event $event */
        $event = $this->get('app.event.service')->getEventForCustomPage($id);

        if (empty($event) || $event->getStatus() != 1) {
            throw new ResourceNotFoundException('Event not found');
        }
        $body = $event->getCustomPage()->getBody();
        $this->fillBannerData($event, $body->getBanner());

        $serializer = $this->get('serializer');
        $isRegistrationFinished = $this->isRegistrationFinished($event);
        $customForm = $event->getCustomForm();

        $jsonContent = $serializer->serialize($customForm, 'json');
        $ownerData = $serializer->serialize($this->getOwnerData($event->getCustomPage(), $event->getUser()), 'json');
        $templateData = $serializer->serialize($body, 'json');
        $template = $event->getCustomPage()->getBody()->getTemplate();

        return [
            'participant' => $request->getSession()->get('participant'),
            'participantJson' => $serializer->serialize($request->getSession()->get('participant'), 'json'),
            'template' => $template,
            'templateData' => $templateData,
            'event' => $event,
            'style' => $body->getStyle(),
            'page' => $event->getCustomPage(),
            'form' => $event->getCustomForm(),
            'registrationFinished' => (int) $isRegistrationFinished,
            'formJson' => $jsonContent,
            'ownerData' => $ownerData,
            'endRegistration' => $serializer->serialize($this->getRegistrationEnd($event)->format('d-m-Y'), 'json')
        ];
    }

    private function getRegistrationEnd(Event $event)
    {
        if ($event->getRegistrationDate()) {
            return $event->getRegistrationDate();
        }

        return $event->getStartDate();
    }

    /**
     * @param $event
     * @return bool
     */
    private function isRegistrationFinished(Event $event)
    {
        $today = new DateTime();
        $registrationDate = $event->getRegistrationDate();
        $startDay = $event->getStartDate();
        if ((!is_null($registrationDate) && $registrationDate < $today) || $startDay < $today) {
            return true;
        }

        return false;
    }

    /**
     * @Template()
     *
     * @param int $id
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function paymentStatusAction($id)
    {
        $request = Request::createFromGlobals();

        return [
            'status' => $request->query->get('status'),
            'eventId' => $id
        ];
    }


    private function fillBannerData(Event $event, Banner $banner) {
        $banner
            ->setStart($event->getStartDate())
            ->setFee($event->getFree() ? 0 : $event->getFee())
            ->setLimit($event->getLimit())
            ->setParticipantCount($event->participantCount());
    }


    /*
     * @param $page CustomPage
     * @param $user User
     */
    private function getOwnerData(CustomPage $page, User $user)
    {
        //var_dump($page->getBody()->getTemplate());
        $data = [];
        //$data['address'] = 'address';
        //$data['email'] = $user->getEmail();
        //$data['telephone'] = 'phone';
        $data['isCompany'] = $user->getIsCompany();
        $data['nip'] = $user->getNip();
        $data['companyName'] = $user->getCompanyName();
        if (!isset($data['email'])) {
            $data['email'] = $user->getEmail();
        }

        return $data;
    }
}
