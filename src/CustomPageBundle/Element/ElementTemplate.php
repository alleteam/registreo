<?php
namespace Registreo\CustomPageBundle\Element;

interface ElementTemplate
{
    public function getTemplate();
}