<?php
namespace Registreo\CustomPageBundle\Element;

use JMS\Serializer\Annotation as JMS;

class Map extends ElementAbstract implements ElementTemplate
{
    /**
     * @JMS\Exclude
     */
    public static $TEMPLATE = '<map title="templateData.section_list[#index#].element.title" description="templateData.section_list[#index#].element.description"'
        . ' latitude="templateData.section_list[#index#].element.latitude" longitude="templateData.section_list[#index#].element.longitude"></map>';



    /**
     * @JMS\Type("string")
     * @var string
     */
    private $title;

    /**
     * @JMS\Type("string")
     * @var string
     */
    private $description;

    /**
     * @JMS\Type("double")
     * @var double
     */
    private $latitude;

    /**
     * @JMS\Type("double")
     * @var double
     */
    private $longitude;

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
        return $this;
    }

    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
        return $this;
    }

    public function getTemplate()
    {
        return self::$TEMPLATE;
    }

}