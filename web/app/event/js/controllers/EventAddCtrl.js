angular.module('registreoApp')
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('addevent', {
                url: "/event/add",
                controller: 'EventAddCtrl',
                controllerAs: 'eventAddCtrl',
                templateUrl: "app/event/views/eventAdd.html",
                resolve:{
                    checkAuth: ['UserService', function(UserService){
                        return UserService.checkAuth();
                    }]
                }
            });
    }
    ])
    .controller('EventAddCtrl',['$filter', '$scope', '$rootScope', 'Breadcrumb', '$location', '$stateParams', '$http', 'EventService', 'NotificationsService', 'UserService',
        function ($filter, $scope, $rootScope, $Breadcrumb, $location, $stateParams, $http, EventService, NotificationsService, UserService) {
            $rootScope.$broadcast('activeMenu', 'myaccount');
            $rootScope.ngview.show = true;
            $Breadcrumb.set('Dodaj wydarzenie', [
                {name: 'Moje konto', uri: '/myaccount'}, {name: 'Moje wydarzenia', uri: '/myevents'},
                {name: 'Dodaj wydarzenie', uri: '/event/add'}
            ]);
            window.scrollTo(0, 0);
            $scope.event = {};

            var today = new Date();
            $scope.today = new Date(today.getTime() + 24 * 60 * 60 * 1000);
            $scope.maxDate = new Date(today.setFullYear(today.getFullYear() + 2));
            $scope.event.account_number = "PL";
            $scope.eventSaveing = false;
            $scope.edit = false;
            $scope.user = {};
            $scope.loadCurrentUser = true;

            $scope.$watch('event.event_date', function() {
                if (!$scope.event.registration_date && $scope.event.event_date) {
                    $scope.event.registration_date = $scope.event.event_date;
                }
            });

            UserService.getCurrentUser().then(function (user) {
                $scope.user = user;
                $scope.loadCurrentUser = false;
            });

            $scope.$watch('event.closed', function(value) {
                if (value) {
                    $scope.event.limit = 100;
                } else {
                    $scope.event.limit = null;
                }
            });

            $scope.$watch('event.free', function(value) {
                if (value) {
                    $scope.event.fee = 20;
                    $scope.event.account_number = "PL60102010260000042270201111";
                    $scope.event.bank_name = "bank_name";
                } else {
                    $scope.event.fee = null;
                    $scope.event.account_number = null;
                    $scope.event.bank_name = null;
                }
            });

            
            $scope.saveForm = function(isValid) {
                if(isValid) {
                    $scope.eventSaveing = true;

                    if($scope.event.free) {
                        $scope.saveData();
                    } else {
                        EventService.validateIban($scope.event.account_number).then(function (response) {
                            if (response.status == 204) {
                               $scope.saveData();
                            } else {
                                $scope.invalidIban = true;
                                $scope.eventSaveing = false;
                            }
                        });
                    }
                }
            };

            $scope.saveData = function() {
                var startHour = new Date($scope.event.event_hour).toLocaleTimeString();
                var originalRegistrationDate = $scope.event.registration_date;
                var startDate = EventService.formatPickerDate($scope.event.event_date);
                var registrationDate = EventService.formatPickerDate($scope.event.registration_date);

                $scope.event.start_date = startDate + " " + startHour;
                $scope.event.registration_date = registrationDate + "T23:59:59+02:00";
                EventService.createEvent($scope.event).then(function (status) {
                    //window.scrollTo(0, 0);
                    $scope.eventSaveing = false;
                    NotificationsService.addNotification('Twoje zmiany zostały zapisane.');
                    $location.path('/myevents');
                }, function(error) {
                    $scope.eventSaveing = false;
                });
            };
        }
    ]);
