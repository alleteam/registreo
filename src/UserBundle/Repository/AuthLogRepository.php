<?php

namespace Registreo\UserBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Registreo\Entity\AuthLog as AuthLogModel;

class AuthLogRepository extends EntityRepository
{
    /**
     * @param $authenticationLog AuthLogModel
     * @return AuthLogModel
     */
    public function save(AuthLogModel $authenticationLog)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($authenticationLog);
        $entityManager->flush($authenticationLog);
        
        return $authenticationLog;
    }
}
