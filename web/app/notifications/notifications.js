(function () {
    angular.module('notifications', ['notificationsService'])
        .directive('notifications', notifications);

    function notifications() {
        return {
            restrict: 'E',
            scope: {
                notificationId: "@notificationId"
            },
            templateUrl: '/app/notifications/notifications.html',
            controller: 'NotificationsController',
            controllerAs: 'notificationsCtrl'
        }
    }
})();
