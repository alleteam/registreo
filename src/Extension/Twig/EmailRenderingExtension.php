<?php

namespace Registreo\Extension\Twig;

class EmailRenderingExtension extends \Twig_Extension
{
    private $twig, $encoding = 'UTF-8';

    public function initRuntime(\Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    public function __call($name, $arguments)
    {
        $content = $this->twig->loadTemplate(':Email:elements/'.$name.'.html.twig')
            ->render(['params' => $arguments]);

        return new \Twig_Markup($content, $this->encoding);
    }

    public function getName()
    {
        return 'email_extension';
    }
}