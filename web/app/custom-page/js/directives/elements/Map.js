angular.module('registreoAppCustom').directive('map', function() {
    return {
        restrict: 'E',
        scope: {
            title: '=title',
            description: '=description',
            latitude: '=latitude',
            longitude: '=longitude'
        },
        templateUrl: '/app/custom-page/views/elements/map.html',
        link: function($scope) {
            angular.extend($scope, {
                markers: [ {
                  latitude: $scope.latitude,
                  longitude: $scope.longitude,
                  id: 1
                }],
                markerNo: 1,
                map: {
                center: {
                  latitude: $scope.latitude,
                  longitude: $scope.longitude
                },
                zoom: 16,
                options: {
                    zoomControl: true,
                    scaleControl: true,
                    scrollwheel: false,
                    disableDoubleClickZoom: false
                },
                events: {},
                }
            });
        }
    };
});
