angular.module('registreoApp')
    .controller('EditPageItemCtrl',['$scope', '$rootScope', '$uibModalInstance', 'param', 'uiGmapGoogleMapApi',
        function ($scope, $rootScope, $uibModalInstance, param, GoogleMapApi) {

            var contents = param.contents;
            var item = param.item;
            var oldItem = param.oldItem;

            $rootScope.ngview.show = true;
            $scope.item = item;

            if ($scope.item.type == 3) {
                angular.extend($scope, {
                  markers: [ {
                      latitude: $scope.item.map_data.latitude,
                      longitude: $scope.item.map_data.longitude,
                      id: 1
                  }],
                  markerNo: 1,
                  map: {
                    center: {
                      latitude: $scope.item.map_data.latitude,
                      longitude: $scope.item.map_data.longitude
                    },
                    zoom: 16,
                    options: {

                    },
                    events: {
                      rightclick: function(map, eventName, events) {
                        var event = events[0];
                        var lat = event.latLng.lat();
                        var lng = event.latLng.lng();
                        $scope.$apply(function() {
                          $scope.item.map_data = {};
                          $scope.markers = new Array();
                          $scope.item.map_data = {longitude: lng + "", latitude: lat + ""};

                          $scope.markers.push({
                              latitude: lat,
                              longitude: lng,
                              id: $scope.markerNo
                          });
                        });

                      },
                    },

                  }
                });
            }

            $scope.tinymceOptions = {
                inline: false,
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code'
                ],
                toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                skin: 'lightgray',
                theme : 'modern'
            };

            $scope.closeModal = function () {
                angular.forEach(contents, function (value, key) {
                    if(value.id ==  $scope.item.id) {
                        contents[key] = oldItem;
                    }
                });
                $uibModalInstance.close();
            };
            $scope.saveItem = function (isValid) {
                if(isValid) {
                    $uibModalInstance.close();
                }
            };
        }
    ]);

