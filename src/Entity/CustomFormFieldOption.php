<?php
namespace Registreo\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as ORMAssert;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * Class CustomFormFieldOption
 *
 * @ORM\Entity(repositoryClass="Registreo\EventBundle\Repository\CustomFormFieldOptionRepository")
 * @ORM\Table(name="custom_form_field_options")
 */
class CustomFormFieldOption
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=50)
     * @Assert\NotBlank
     */
    protected $value;

    /**
     * @var int
     *
     * @ORM\Column(name="order_by", type="integer")
     * @Assert\NotBlank
     */
    protected $order;

    /**
     * @ORM\ManyToOne(targetEntity="CustomFormField", inversedBy="options")
     * @ORM\JoinColumn(name="form_field_id", referencedColumnName="id")
     */
    protected $customFormField;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getCustomFormField()
    {
        return $this->customFormField;
    }

    /**
     * @param mixed $customFormField
     */
    public function setCustomFormField($customFormField)
    {
        $this->customFormField = $customFormField;
    }
}
