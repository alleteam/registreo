<?php
namespace Registreo\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as ORMAssert;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\Exclude;
use Registreo\CustomPageBundle\Element\TemplateData;

/**
 * Class CustomPage
 *
 * @ORM\Entity(repositoryClass="Registreo\EventBundle\Repository\CustomPageRepository")
 * @ORM\Table(name="custom_page")
 */
class CustomPage
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\OneToOne(targetEntity="Event", cascade={"remove"})
     * @Assert\NotBlank
     * @Exclude
     */
    protected $event;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    protected $active;

    /**
     * @var bool
     *
     * @ORM\Column(name="list_view", type="boolean")
     * @Assert\NotBlank
     */
    protected $listView;

    /**
     *
     * @var string
     * @JMS\Type("Registreo\CustomPageBundle\Element\TemplateData")
     * @JMS\Accessor(getter="getBody",setter="setBody")
     * @ORM\Column(name="body", type="text", length=1048576)
     */
    protected $body;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param mixed $event
     * @return CustomPage
     */
    public function setEvent(Event $event)
    {
        $this->event = $event;
        
        return $this;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     * @return CustomPage
     */
    public function setActive($active)
    {
        $this->active = $active;
        
        return $this;
    }

    /**
     * @return boolean
     */
    public function isListView()
    {
        return $this->listView;
    }

    /**
     * @param boolean $listView
     * @return CustomPage
     */
    public function setListView($listView)
    {
        $this->listView = $listView;
        
        return $this;
    }

    public function getBody()
    {
        $data =  unserialize($this->body);
        if (!$data instanceof TemplateData) {
            $data = new TemplateData();
        }

        return $data;
    }

    public function setBody(TemplateData $body)
    {
        $this->body = serialize($body);
        return $this;
    }

}
