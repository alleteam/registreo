angular.module('registreoAppCustom').directive('paragraph', function() {
    return {
        restrict: 'E',
        scope: {
            title: '=title',
            description: '=description'
        },
        templateUrl: '/app/custom-page/views/elements/paragraph.html'
    };
});