angular.module('registreoAppCustom')
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('payment-status', {
                url: "/{eventName}/payment-status",
                controller: 'PaymentStatusCtrl',
                controllerAs: 'paymentStatusCtrl',
                templateUrl: '/app/custom-page/views/paymentStatus.html'
            });    
        }
    ])
    .controller('PaymentStatusCtrl',['$rootScope', '$scope', '$location',
        function ($rootScope, $scope, $location) {
            $rootScope.ngview.show = true;
            window.scrollTo(0, 0);
            $scope.paymentStatus = $location.search().status == 'OK' ? 1 : 0;
        }
    ]);

