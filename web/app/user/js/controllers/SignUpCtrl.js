angular.module('registreoApp')
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('signup', {
                url: "/signup",
                controller: 'SignUpCtrl',
                controllerAs: 'signUpCtrl',
                templateUrl: "app/user/views/signup.html"
            });    
        }
    ])
    .controller('SignUpCtrl',['$scope', '$rootScope', 'Breadcrumb', '$location', '$http',
        function ($scope, $rootScope, $Breadcrumb, $location, $http) {
            $rootScope.$broadcast('activeMenu', '');
            $rootScope.ngview.show = true;
            $Breadcrumb.set('Rejestracja', [{name: 'Rejestracja', uri: '/signup'}]);

            $scope.signupLoading = false;
            $scope.signUp = function() {

                if ($scope.signupForm.regulation.$viewValue) {
                    $scope.signupLoading = true;
                    $http({
                        method: 'POST',
                        url: Routing.generate('fos_user_registration_register'),
                        data: $.param({
                            'fos_user_registration_form[email]': $scope.signupForm.email.$viewValue,
                            'fos_user_registration_form[plainPassword][first]': $scope.signupForm.password.$viewValue,
                            'fos_user_registration_form[plainPassword][second]': $scope.signupForm.repeatPassword.$viewValue,
                            'regulation': $scope.signupForm.regulation.$viewValue,
                            'isCompany': $scope.signupForm.isCompany.$viewValue,
                            'companyName': $scope.signupForm.companyName.$viewValue,
                            'nip': $scope.signupForm.nip.$viewValue
                        }),
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                            //'X-Requested-with' : 'XMLHttpRequest'
                        }
                    })
                        .success(function (data) {
                            $scope.signupLoading = false;
                            if (!data.success && (data.error.email || data.error.password || data.error.regulation || data.error.nip || data.error.companyName)) {

                                if (data.error.email) {
                                    $scope.signupForm.email.$setValidity('uniqueEmail', false);
                                }

                                if (data.error.regulation) {
                                    $scope.signupForm.regulation.$setValidity('required', false);
                                }

                                if (data.error.nip) {
                                    $scope.signupForm.nip.$setValidity('required', false);
                                }

                                if (data.error.companyName) {
                                    $scope.signupForm.companyName.$setValidity('required', false);
                                }

                                if (data.error.password.toString().indexOf('short') > -1) {
                                    $scope.signupForm.password.$setValidity('minlength', false);
                                } else if (data.error.password.toString().indexOf('match') > -1) {
                                    $scope.signupForm.password.$setValidity('notSame', false);
                                }

                            } else {
                                $scope.signupForm.email.$setValidity('uniqueEmail', true);
                                $scope.signupForm.password.$setValidity('minlength', true);
                                $location.path('/newaccount');
                            }
                        });
                }
            };
            
            $scope.clearError = function() {
                $scope.signupForm.email.$setValidity('uniqueEmail', true);
                $scope.signupForm.password.$setValidity('notSame', true);
                $scope.signupForm.password.$setValidity('minlength', true);
            };
        }
    ]);



