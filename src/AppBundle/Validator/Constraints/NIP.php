<?php
namespace Registreo\AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NIP extends Constraint
{
    public $message = 'Nieprawidłowy numer NIP.';

    public function validatedBy() {
        return 'app.signup.validator.nip';
    }
}