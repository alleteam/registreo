angular.module('registreoApp.eventCustomPageController', ['ui.sortable', 'ui.bootstrap'])
    .controller('EventCustomPageCtrl',['$scope', '$rootScope', 'Breadcrumb', '$location', '$stateParams', 'FileUploader', '$element', 'NotificationsService', 'CustomPageService', '$uibModal',
        function ($scope, $rootScope, $Breadcrumb, $location, $stateParams, FileUploader, $element, NotificationsService, CustomPageService, $uibModal) {
            
            $rootScope.$broadcast('activeMenu', 'myaccount');
            $rootScope.ngview.show = true;
            $Breadcrumb.set('Strona wydarzenia', [
                {name: 'Moje konto', uri: '/myaccount'}, {name: 'Moje wydarzenia', uri: '/myevents'},
                {name: 'Strona wydarzenia', uri: '/event/'+ $stateParams.eventId +'/page'}
            ]);
            window.scrollTo(0, 0);
            $scope.uploadError = '';
            $scope.customPageLoading = true;
            $scope.dragWidth = '100%';

            $scope.regBanners = [
                [ '/banner/banner_1.jpg', '/banner/banner_2.jpg', '/banner/banner_3.jpg'],
                [ '/banner/banner_4.jpg', '/banner/banner_5.jpg', '/banner/banner_6.jpg'],
                [ '/banner/banner_7.jpg', '/banner/banner_8.jpg', '/banner/banner_9.jpg']
            ];
            
            $scope.sections = [
                { title: 'cała szerokość strony', class: 'col-md-12 col-sm-12 col-xs-12', element: {type: 'Paragraph'}},
                { title: '1/2 szerokości strony', class: 'col-md-6 col-sm-6 col-xs-12', element: {type: 'Paragraph'}},
                { title: '1/3 szerokości strony', class: 'col-md-4 col-sm-4 col-xs-6', element: {type: 'Paragraph'}},
                { title: '1/6 szerokości strony', class: 'col-md-2 col-sm-2 col-xs-6', element: {type: 'Paragraph'}},
                { title: '3/4 szerokości strony', class: 'col-md-9 col-sm-9 col-xs-12', element: {type: 'Paragraph'}},
                { title: '1/4 szerokości strony', class: 'col-md-3 col-sm-3 col-xs-6', element: {type: 'Paragraph'}}
            ];

            $scope.pageSectionList = [];


            /** uploader */
            
            $scope.uploader = new FileUploader({
                url: '/api/files',
                autoUpload: true,
                queueLimit: 1
            });

            $scope.uploader.filters.push({
                name: 'fileType', fn: function (item) {
                    $scope.uploadError = $scope.isImage(item) ? '' : 'Możesz uploadować obrazki o rozszerzeniu jpg i png.';
                    if ($scope.uploadError == '') {
                        return true;
                    }
                }
            });
            
            $scope.uploader.onCompleteItem = function(fileItem, response, status, headers) {
                if (status == 422) {
                    NotificationsService.addNotification(response[0].message, NotificationsService.ERROR_NOTIFICATION, 2);
                    $scope.clearQueueLastElement();
                    self.resetForm();
                } else if (status == 500) {
                    NotificationsService.addNotification('Wystąpił problem w trakcie uploadu zdjęcia. Spróbuj ponownie.', NotificationsService.ERROR_NOTIFICATION, 2);
                    self.clearQueueLastElement();
                    self.resetForm();
                } else {
                    
                    if (typeof response[0] != 'undefined') {
                        $scope.setBanner(response[0].file_path);
                    }
                    $scope.clearQueueLastElement();

                }
                $scope.inProgress = false;
            };
            
            $scope.uploader.onWhenAddingFileFailed = function(item, filter, options) {
                if($scope.uploadError != '') {
                    NotificationsService.addNotification($scope.uploadError, NotificationsService.ERROR_NOTIFICATION, 2);
                }
            };
            
            /** uploader service */
            
            $scope.onUploadClick = function () {
                if ($scope.uploader.queue.length >= 1){
                    NotificationsService.addNotification('Aby dodać nowe zdjęcie, należy usunąć obecne.', NotificationsService.ERROR_NOTIFICATION, 2);
                } else {
                    angular.element('.upload-button').click();
                }
            };

            $scope.isImage = function(item) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|'.indexOf(type) !== -1;
            };

            $scope.removeBanner = function() {
                $scope.page.banner.file = {};
                self.clearQueueLastElement();
                self.resetForm();
            };

            $scope.addValidators  = function () {
                $scope.uploader.filters.push({
                    name: 'fileType', fn: function (item) {
                        $scope.uploadError = $scope.isImage(item) ? '' : 'Możesz uploadować obrazki o rozszerzeniu jpg i png.';
                        if ($scope.uploadError == '') {
                            return true;
                        }
                    }
                });
            };
            
            $scope.clearQueueLastElement = function () {
                if ($scope.uploader.queue.length > 0) {
                    $scope.uploader.queue[$scope.uploader.queue.length - 1].remove();
                }
            };
            
            $scope.addFakeItemsToQueue = function (itemNumber) {
                for (var i = 1; i <= itemNumber; i++) {
                    var fakeItem = new FileUploader.FileItem($scope.uploader, {
                        lastModifiedDate: new Date(),
                        size: 16,
                        type: 'image/jpeg',
                        name: 'fake_file_name'
                    });

                    $scope.uploader.queue.push(fakeItem)
                }
            };
            
            $scope.setBanner = function(url) {
                if (typeof $scope.page.body.banner  == 'undefined')  {
                    $scope.page.body.banner = {};
                }
                $scope.page.body.banner.url = url;
            };
            
            CustomPageService.getEventCustomPage($stateParams.eventId).then(function (page) {
                $scope.page = page;
                $scope.customPageLoading = false;
                if ($scope.page.banner && $scope.page.banner.file) {
                    $scope.addFakeItemsToQueue(1);
                }
            });

            $scope.editSection = function(element, lockType) {
                $uibModal.open({
                    animation: true,
                    backdrop  : 'static',
                    keyboard  : false,
                    templateUrl: 'app/event/views/editSection.html',
                    controller: 'EditSectionCtrl',
                    resolve: {
                       param: function () {
                           return {element: element, lock: lockType, eventId: $stateParams.eventId};
                       }
                    },
                    size: "lg",
                    windowClass: 'center-modal'
                });
            };

            self.resetForm  = function () {
                var form = $element.find('.uploader-form');
                form[0].reset();
            };

            self.clearQueueLastElement = function () {
                if ($scope.uploader.queue.length > 0) {
                    $scope.uploader.queue[$scope.uploader.queue.length - 1].remove();
                }
            };


            $scope.sortableOptions = {
                stop : function(e, ui) {
                    //var item = ui.item.scope().item;
                    //var fromIndex = ui.item.sortable.index;
                    //var toIndex = ui.item.sortable.dropindex;
                    //console.log('moved', item, fromIndex, toIndex);
                    //item.order = toIndex + 1;

                    var i = 1;
                    angular.forEach($scope.page.content, function (value, key) {
                        value.order = i;
                        i++;
                    });
                }
            };

            $scope.savePage = function() {
                $scope.savingPage = true;
                angular.forEach($scope.page.content, function (value, key) {
                    value.custom_page = {"id" : $scope.page.id};
                });
                CustomPageService.saveEventCustomPage($stateParams.eventId, $scope.page).then(function (page) {
                    $scope.savingPage = false;
                    window.scrollTo(0, 0);
                    NotificationsService.addNotification('Twoje zmiany zostały zapisane.');
                });
            };
            
            $scope.dragStartCopy = function(index) {
                var item = $scope.sections[index];
                var re = /col-md-(\d+)/g;
                var arr = re.exec(item.class);
                $scope.dragWidth = Math.floor((arr[1] / 12) * 100) + '%';
            };
           
           
            $scope.dragStartMove = function(index) {
                var item  = $scope.page.body.section_list[index];
                $scope.page.body.section_list.splice(index, 1);
                var re = /col-md-(\d+)/g;
                var arr = re.exec(item.class);
                $scope.dragWidth = Math.floor((arr[1] / 12) * 100) + '%';
            };
            
            $scope.dropCallback = function(index, item, external, type) {
                return item;
            };
            
            $scope.dragOver = function(index) {
                var element = angular.element("#event_page")[0].children[index];
                angular.element(element).css('width', $scope.dragWidth);
                return true;
            };
            

        }
    ]);
