<?php

namespace Registreo\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Registreo\Entity\User;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Validator;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'text', [
                'label' => 'form.email',
                'translation_domain' => 'FOSUserBundle',
                //'error_bubbling' => true, - nie wlaczajcie dla tego pola ;)
                'constraints' => [
                    new Validator\NotBlank(['groups' => 'custom_registration']),
                    new Validator\Email(['groups' => 'custom_registration']),
                    new Validator\Length([
                        'groups' => 'custom_registration',
                        'min' => 3,
                        'max' => 255
                    ])
                ]
            ])
            ->add('plainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'fos_user.password.mismatch',
                'error_bubbling' => true,
                'first_options' => [
                    'constraints' => [
                        new Validator\NotBlank(['groups' => 'custom_registration']),
                        new Validator\Length([
                            'groups' => 'custom_registration',
                            'min' => 3,
                            'max' => 100
                        ]),
                    ]
                ],
                'second_options' => [
                    'constraints' => [
                        new Validator\NotBlank(['groups' => 'custom_registration']),
                        new Validator\Length([
                            'groups' => 'custom_registration',
                            'min' => 3,
                            'max' => 100
                        ]),
                    ]
                ]

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
            'validation_groups' => ['custom_registration'],
            //'csrf_token_id' => 'registration',
            // BC for SF < 2.8
            //'intention'  => 'registration',
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}