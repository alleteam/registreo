<?php
namespace Registreo\AppBundle\Event;

use Symfony\Component\EventDispatcher\Event as SymfonyEvent;
use Registreo\Entity\Participant;

class InviteParticipantEvent extends SymfonyEvent
{
    const NAME = "registreo.participant.invite";

    /**
     *
     * @var Participant
     */
    private $participant;

    public function __construct(Participant $participant)
    {
        $this->participant = $participant;
    }

    public function getParticipant()
    {
        return $this->participant;
    }


    public function getName()
    {
        return self::NAME;
    }

}