angular.module('registreoApp')
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('eventTransactions', {
                url: "/event/{eventId:int}/transactions",
                controller: 'EventTransactionsCtrl',
                controllerAs: 'eventTransactionsCtrl',
                templateUrl: "app/event/views/event-transactions.html",
                resolve:{
                    checkAuth: ['UserService', function(UserService){
                        return UserService.checkAuth();
                    }]
                }
            });    
        }
    ])
    .controller('EventTransactionsCtrl',['$scope', '$rootScope', 'Breadcrumb', 'TransactionService', 
                'NotificationsService', '$stateParams',
        function ($scope, $rootScope, $Breadcrumb, transactionService, notificationsService, $stateParams) {
                        
            $rootScope.$broadcast('activeMenu', 'myaccount');
            $rootScope.ngview.show = true;
            window.scrollTo(0, 0);
            $Breadcrumb.set('Lista uczestników', [
                {name: 'Moje konto', uri: '/myaccount'}, {name: 'Moje wydarzenia', uri: '/myevents'},
                {name: 'Transakcje', uri: '/event/'+ $stateParams.eventId +'/transactions'}
            ]);

            $scope.summary = {};
            $scope.summary.payment = 0;
            $scope.summary.commission = 0;
            $scope.summary.dotpay_commission = 0;
            $scope.summary.registreo_commission = 0;
            $scope.summary.payout = 0;
            $scope.summary.discount = 0;

            $scope.amount = 0;
            $scope.payoutList = [];
            $scope.transactionList = [];
            $scope.showAll = true;
            $scope.buttonVisible = true;
            
            $scope.payoutClick = function() {
                $scope.buttonVisible = false;
                transactionService.createPayout($stateParams.eventId, $scope.amount).then(function(data) {
                    transactionService.getPayoutsList($stateParams.eventId).then(function(data) {
                        $scope.payoutList = data;
                    });    
                });
            };

            $scope.getAll = function() {
                $scope.showAll = false;
                transactionService.getList($stateParams.eventId).then(function(data) {
                    $scope.transactionList = data;
                });
            };

            transactionService.getPayoutsList($stateParams.eventId).then(function(data) {
                $scope.payoutList = data;
            });
            
            transactionService.getShortList($stateParams.eventId).then(function(data) {
                $scope.transactionList = data;
            });
            
            transactionService.getSummary($stateParams.eventId).then(function(data) {
                $scope.summary = data;
                $scope.amount = data.payment + data.commission - data.payout;
            });
    }]);

