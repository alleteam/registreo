<?php
namespace Registreo\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as ORMAssert;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\Exclude;

/**
 * Class CustomPage
 *
 * @ORM\Entity(repositoryClass="Registreo\EventBundle\Repository\CustomFormRepository")
 * @ORM\Table(name="custom_form")
 */
class CustomForm
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Event
     *
     * @ORM\OneToOne(targetEntity="Event", cascade={"remove"})
     * @Exclude
     */
    protected $event;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    protected $active;

    /**
     * @var CustomFormField[]
     *
     * @ORM\OneToMany(targetEntity="CustomFormField", mappedBy="customForm", cascade={"persist", "remove"})
     */
    protected $fields;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @return Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param Event $event
     * @return CustomForm
     */
    public function setEvent(Event $event)
    {
        $this->event = $event;
        
        return $this;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     * @return CustomForm
     */
    public function setActive($active)
    {
        $this->active = $active;
        
        return $this;
    }

    /**
     * @return CustomFormField[]
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param CustomFormField[] $fields
     * @return CustomForm
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
        
        return $this;
    }

    /**
     * @param CustomFormField $field
     * @return $this
     */
    public function addField(CustomFormField $field)
    {
        $field->setCustomForm($this);
        $this->fields[] = $field;

        return $this;
    }

    /**
     * @param CustomFormField $field
     * @return $this
     */
    public function removeField(CustomFormField $field)
    {
        $field->setCustomForm(null);
        $this->fields->removeElement($field);

        return $this;
    }
}
