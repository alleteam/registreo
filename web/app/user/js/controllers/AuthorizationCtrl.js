angular.module('registreoApp')
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('signin', {
                url: "/signin",
                controller: 'AuthorizationCtrl',
                controllerAs: 'authorizationCtrl',
                templateUrl: "app/user/views/signin.html"
            });    
        }
    ])
    .controller('AuthorizationCtrl',['$scope', '$rootScope', 'Breadcrumb', '$location', '$http',
        function ($scope, $rootScope, $Breadcrumb, $location, $http) {
            $rootScope.$broadcast('activeMenu', 'login');

            $rootScope.ngview.show = true;
            $Breadcrumb.set('Logowanie', [{name: 'Logowanie', uri: '/signin'}]);
            $scope.emptyFields = false;
            $scope.submission = false;


            $scope.signIn = function() {

                $scope.submission = true;

                if ($scope.signinForm.$valid) {

                    $http({
                        method: 'POST',
                        url: Routing.generate('fos_user_security_check'),
                        data: $.param({
                            _username: $scope.signinForm.email.$viewValue,
                            _password: $scope.signinForm.password.$viewValue,
                            _remember_me: false
                        }),
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                            //'X-Requested-with' : 'XMLHttpRequest'
                        }
                    })
                        .success(function (data) {
                            if (!data.success) {
                                $scope.signinForm.password.$setValidity('invalidSignIn', false);
                                $scope.submission = false;
                            } else {
                                $rootScope.$broadcast('loginSuccess', true);
                                $scope.signinForm.password.$setValidity('invalidSignIn', true);
                                $location.path('/myevents');
                            }
                        });

                } else {
                    $scope.emptyFields = true;
                    $scope.submission = false;
                }

            };
            
            $scope.clearError = function() {
                $scope.signinForm.password.$setValidity('invalidSignIn', true);
                $scope.emptyFields = false;
            };
            
            $scope.rememberPassword = function() {
                $location.path('/rememberPassword');
            };
            
            $scope.register = function() {
                $location.path('/signup');
            };
        }
    ]);

