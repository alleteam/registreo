(function () {
    'use strict';

    angular.module('customPageService', ['restangular'])
        .service('CustomPageService', customPageService);

    customPageService.$inject = ['Restangular', '$q'];

    function customPageService(Restangular, $q) {
        var self = this;

        self.getEventCustomPage = function (eventId) {
            var defer = $q.defer();
            Restangular.one('events', eventId).one('custom-page').get().then(function (data) {
                defer.resolve(data);
            });
            return defer.promise;
        };

        self.saveEventCustomPage = function (eventId, page) {
            var defer = $q.defer();
            Restangular.one('events', eventId).one('custom-page').doPUT(page).then(function (data) {
                defer.resolve(data);
            });
            return defer.promise;
        };

        self.getEventCustomForm = function (eventId) {
            var defer = $q.defer();
            Restangular.one('events', eventId).one('custom-form').get().then(function (data) {
                defer.resolve(data);
            });
            return defer.promise;
        };

        self.saveEventCustomForm = function (eventId, form) {
            var defer = $q.defer();
            Restangular.one('events', eventId).one('custom-form').doPUT(form).then(function (data) {
                defer.resolve(data);
            });
            return defer.promise;
        };

        self.saveApplicationForm = function (eventId, application, regulation) {
            var defer = $q.defer();
            Restangular.one('events', eventId).all('applications').post({application : application, regulation: regulation}).then(function (data) {
                defer.resolve(data);
            });
            return defer.promise;
        };

        self.getApplicationsParticipants = function (eventId, current, limit) {
            var defer = $q.defer();
            Restangular.one('events', eventId).all('participants').all('applications').getList({'page': current, 'limit': limit}).then(function (data) {
                defer.resolve(data);
            });
            return defer.promise;
        };
    }
})();