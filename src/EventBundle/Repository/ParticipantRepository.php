<?php

namespace Registreo\EventBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Registreo\Entity\Participant;
use Registreo\Entity\Event;

class ParticipantRepository extends EntityRepository
{
    /**
     * @param Participant $participant
     */
    public function save(Participant $participant)
    {
        $this->getEntityManager()->persist($participant);
        $this->getEntityManager()->flush();
    }


    /**
     *
     * @param string $hash
     * @param \Registreo\Entity\Event $event
     * @return \Registreo\Entity\Participant
     */
    public function findByHashAndEvent($hash, Event $event)
    {
        $query = $this->createQueryBuilder('t');
        $query->where('t.hash = :hash and t.event = :event');
        $query->setParameter('hash', $hash);
        $query->setParameter('event', $event);
        return $query->getQuery()->getOneOrNullResult();
    }
}