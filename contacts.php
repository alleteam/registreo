<?php

for ($i=1; $i <= 23; $i++) {
    $document = file_get_contents("http://www.kluby-sportowe.com.pl/szukaj-klubu.html?page=$i&per_page=100");
    preg_match_all('/<td class="tabmiddle"><a href="(.*)" title="(.*)">/', $document, $clubs);
    $subClubs= array_unique($clubs[1]);

    foreach ($subClubs as $club) {
        $clubDocument = file_get_contents("http://www.kluby-sportowe.com.pl" . $club);
        preg_match_all('/<title>(.*)<\/title>/', $clubDocument, $name);
        $clubName = $name[1][0];
        $clubName =  str_replace("&rdquo;", "", $clubName);

        $clubDocument = str_replace("WWW:", "", $clubDocument);
        $clubDocument = str_replace("FAX:", "", $clubDocument);

        preg_match_all('/TEL:(.*)E-MAIL:/s', $clubDocument, $telephone);
        $data = explode("<br/>", $telephone[1][0]);
        if (count($data) > 1) {
            $clubTelephone = trim(strip_tags($data[1]));
        } else {
            $clubTelephone = trim(strip_tags($telephone[1][0]));
        }

        preg_match_all('/E-MAIL:(.*)DYSCYPLINY:/s', $clubDocument, $email);
        $data = explode("<br/>", $email[1][0]);
        $clubEmail = trim(strip_tags($data[1]));

        preg_match_all('/DYSCYPLINY:(.*)<div class="kdrbl" ><\/div>/s', $clubDocument, $discipline);
        $clubDiscipline = trim(strip_tags($discipline[1][0]));

        if (!$clubTelephone && !$clubEmail) {
            continue;
        }

        $record = mb_convert_encoding($clubName, "UTF-8") . ";" . $clubTelephone . ";" . mb_convert_encoding($clubEmail, "UTF-8") . ";" . mb_convert_encoding($clubDiscipline, "UTF-8");
        file_put_contents("club_contacts.txt", $record . "\n", FILE_APPEND);

        if ($clubEmail) {
            file_put_contents("club_contacts_emails.txt", $record . "\n", FILE_APPEND);
        }
    }
}