angular.module('registreoAppCustom')
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('payment', {
                url: "/{eventName}/payment/{participant}",
                controller: 'PaymentFormCtrl',
                controllerAs: 'paymentFormCtrl',
                templateUrl: '/app/custom-page/views/paymentForm.html'
            });    
        }
    ])
    .controller('PaymentFormCtrl',['$rootScope', '$scope', '$stateParams', 'PaymentService',
        function ($rootScope, $scope, $stateParams, paymentService) {
            $rootScope.ngview.show = true;
            window.scrollTo(0, 0);
            $scope.params = {};
            $scope.participant = {};
            $scope.paymentLoading = true;
            
            paymentService.getPaymentDataForm($stateParams.participant).then(
                function(data) {
                    angular.element('#payment_form').attr('action', data.address);
                    angular.element('#payment_form').attr('class', '');
                    $scope.params = data.params;
                    $scope.participant = data.participant;
                    $scope.paymentLoading = false;
                }
            );
        }
    ]);

