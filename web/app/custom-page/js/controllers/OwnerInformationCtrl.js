angular.module('registreoAppCustom')
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('owner-information', {
                url: "/{eventName}/owner-information",
                controller: 'OwnerInformationCtrl',
                controllerAs: 'ownerInformationCtrl',
                templateUrl: '/app/custom-page/views/ownerInformation.html'
            });    
        }
    ])
    .controller('OwnerInformationCtrl',['$rootScope', '$scope', '$location',
        function ($rootScope, $scope, $location) {
            $rootScope.ngview.show = true;
            window.scrollTo(0, 0);
            $scope.ownerData = $rootScope.ownerData;
        }
    ]);

