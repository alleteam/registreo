module.exports = function (grunt) {
    require('load-grunt-tasks')(grunt);
    require('time-grunt')(grunt);
    
    grunt.initConfig({
        dir: {
            tmp: '.tmp',
            deploy: 'deploy',
            adminLayout: '../src/AppBundle/Resources/views/Layout',
            views: 'app/views'
            
        },

        gruntfile: {
            files: ['Gruntfile.js']
        },

        clean: {
            all: {
                files: [{
                    dot: true,
                    src: [
                        '<%= dir.tmp %>',
                        '<%= dir.deploy %>/*'
                    ]
                }]
            }
        },
        
        useminPrepare: {
            admin: {
                src: ['<%= dir.adminLayout %>/index.html.twig'],
                options: {
                    dest: '<%= dir.tmp %>'
                }
            }
        },
        
        autoprefixer: {
            options: {
                browsers: ['last 1 version']
            },
            admin: {
                files: [{
                    expand: true,
                    cwd: '<%= dir.tmp %>/styles/',
                    src: '{,*/}*.css',
                    dest: '<%= dir.tmp %>/styles/'
                }]
            }
        },
       
        
        ngAnnotate: {
            admin: {
                files: [{
                    expand: true,
                    cwd: '<%= dir.tmp %>/concat/admin/scripts',
                    src: '*.js',
                    dest: '<%= dir.tmp %>/concat/admin/scripts'
                }]
            }
        },
        
        uglify: {
            generated: {
                files: [
                    {
                        dest: '<%= dir.deploy %>/admin/scripts/external.js',
                        src: ['<%= dir.tmp %>/concat/admin/scripts/external.js']
                    },
                    {
                        dest: '<%= dir.deploy %>/admin/scripts/application.js',
                        src: ['<%= dir.tmp %>/concat/admin/scripts/application.js']
                    }
                ]
            }
        }

        
        
        

        
    });
    
    
    
    grunt.registerTask('build', [
        'clean:all',
        'useminPrepare',
        'autoprefixer',
        'concat',
        'ngAnnotate',
        'uglify'
    ]);
};


