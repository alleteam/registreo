<?php

namespace Registreo\AppBundle\Service;

use Registreo\Entity\Event;
use Registreo\Entity\Participant;
use Symfony\Component\Templating\EngineInterface;
use Registreo\AppBundle\Event\InviteParticipantEvent;
use Symfony\Component\HttpFoundation\RequestStack;

class NotificationService
{
    const REGISTREO_DEFAULT_EMAIL = 'registreo@registreo.pl',
          NOTIFICATION_DIRECTORY = 'AppBundle:Notifications:',
          CONTACT_FORM_MESSAGE = 'ContactFormMessage',
          CUSTOMER_QUESTION = 'CustomerQuestion',
          TRANSACTION_CANCELED = 'TransactionCanceled',
          PARTICIPANT_INVITE =  'ParticipantInvite'
    ;

    protected $messages = [
        self::CONTACT_FORM_MESSAGE => 'Zapytanie od klienta',
        self::CUSTOMER_QUESTION => 'Zapytanie od klienta',
        self::TRANSACTION_CANCELED => 'Twoja transakcja została anulowana',
        self::PARTICIPANT_INVITE => 'Zaproszenie do wzięcia udziału w wydarzeniu'
    ];

    /**
     * @var \Swift_Mailer
     */
    public $mailer;

    /**
     * @var EngineInterface
     */
    public $templating;


    /**
     * @var RequestStack
     */
    public $requestStack;

    /**
     * @param \Swift_Mailer $mailer
     */
    public function setMailer(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param EngineInterface $templating
     */
    public function setTemplating(EngineInterface $templating)
    {
        $this->templating = $templating;
    }

    /**
     *
     * @param RequestStack $stack
     */
    public function setRequestStack(RequestStack $stack)
    {
        $this->requestStack = $stack;
    }

    /**
     * @param string $notification
     * @param string $to
     * @param array  $data
     *
     * @return boolean
     */
    public function sendNotification($notification, $from, $to, $data = [])
    {
        $message = \Swift_Message::newInstance()
            ->setSubject($this->messages[$notification])
            ->setFrom($from, 'Registreo.pl')
            ->setTo($to)
            ->setBody(
                $this->templating->render(
                    self::NOTIFICATION_DIRECTORY . $notification . '.html.twig',
                    $data
                ),
                'text/html'
            );

        return $this->mailer->send($message);
    }

    public function sendContactMessage($clientName, $clientEmail, $messageTitle, $messageBody)
    {
        $this->sendNotification(
            NotificationService::CONTACT_FORM_MESSAGE,
            $clientEmail,
            NotificationService::REGISTREO_DEFAULT_EMAIL,
            [
                'title' => $messageTitle,
                'body'  => $messageBody,
                'clientName' => $clientName,
                'clientEmail' => $clientEmail
            ]
        );
    }

    /**
     * Send customer question
     *
     * @param Event $event
     * @param $clientName
     * @param $clientEmail
     * @param $messageTitle
     * @param $messageBody
     */
    public function sendCustomerMessage(Event $event, $clientName, $clientEmail, $messageTitle, $messageBody)
    {
        $this->sendNotification(
            NotificationService::CUSTOMER_QUESTION,
            $clientEmail,
            $event->getUser()->getEmail(), // to event administrator
            [
                'title' => $messageTitle,
                'body'  => $messageBody,
                'clientName' => $clientName,
                'clientEmail' => $clientEmail,
                'eventName' => $event->getName()
            ]
        );
    }

    /**
     * Send transaction canceled message
     *
     * @param Event $event
     * @param Participant $participant
     */
    public function sendTransactionCanceledMessage(Event $event, Participant $participant)
    {
        $this->sendNotification(
            NotificationService::TRANSACTION_CANCELED,
            self::REGISTREO_DEFAULT_EMAIL,
            $participant->getEmail(),
            [
                'participantName' => $participant->getName(),
                'title' => $this->messages[self::TRANSACTION_CANCELED],
                'paymentLink'  => 'http://link.pl/1212221343534253',
                'eventName' => $event->getName()
            ]
        );
    }

    public function inviteParticipant(InviteParticipantEvent $event) {
        $this->sendNotification(
            NotificationService::PARTICIPANT_INVITE,
            $event->getParticipant()->getEvent()->getUser()->getEmail(),
            $event->getParticipant()->getEmail(),
            [
                'participantName' => $event->getParticipant()->getName(),
                'title' => "Zaproszenie do wzięcia udziału w wydarzeniu",
                'eventName' => $event->getParticipant()->getEvent()->getName(),
                'eventLink' => sprintf("%s://%s/%s?usr=%s",
                    $this->requestStack->getCurrentRequest()->isSecure() ? "https" : "http",
                    $this->requestStack->getCurrentRequest()->getHost(),
                    $event->getParticipant()->getEvent()->getSeoLink()->getName(),
                    $event->getParticipant()->getHash()
                    )
            ]
        );
    }
}
