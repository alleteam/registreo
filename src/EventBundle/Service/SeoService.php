<?php
namespace Registreo\EventBundle\Service;

use Registreo\EventBundle\Repository\SeoLinkRepository;
use Registreo\EventBundle\Repository\ParticipantRepository;
use Symfony\Component\HttpFoundation\Request;
use Registreo\Entity\SeoLink;
use Registreo\Entity\Event;
use Registreo\AppBundle\Exception\ValidationException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SeoService
{

    /**
     * @var SeoLinkRepository
     */
    private $seoLinkRepository;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @var SessionIdInterface
     */
    protected $session;

    /**
     *
     * @var ParticipantRepository
     */
    protected $participantRepository;

    private $forbiddenLink = [ 'api', 'event' , 'login', 'login_check', 'logout', 'profile', 'register', 'resetting',
        'js', 'app', 'export','payment'];

    public function __construct(SeoLinkRepository $seoLinkRepository, ParticipantRepository $participantRepository,
        ValidatorInterface $validator, SessionInterface $session)
    {
        $this->seoLinkRepository = $seoLinkRepository;
        $this->validator = $validator;
        $this->session = $session;
        $this->participantRepository = $participantRepository;
    }

    public function validateSeoLink(SeoLink $seoLink) {
        $errors = $this->validator->validate($seoLink);
        if (count($errors) > 0) {
            throw new ValidationException($errors);
        }
        if ($this->checkForbidden($seoLink->getName())) {
            throw  new \RuntimeException("This link is forbidden");
        }
        if ($this->seoLinkRepository->getLink($seoLink->getName())) {
            throw  new \RuntimeException("This link already exists");
        }
    }


    /**
     * 
     */
    public function getEvent(Request $request)
    {
        $url = explode('?', $request->getRequestUri())[0];
        $data = explode('/',$url);
        if (count($data)>=2 && strlen($data[1]) > 0) {
            $link = $data[1];
            if (!$this->checkForbidden($link)) {
                return $this->searchEvent($request, $link, $request->get('usr', null));
            }
        }
        return null;
    }


    private function checkForbidden($link) {
        return in_array($link, $this->forbiddenLink);
    }

    private function searchEvent(Request $request, $link, $hash) {
        $seoLink = $this->seoLinkRepository->getLink($link);
        if (!is_null($seoLink) && !is_null($hash)) {
            return $this->redirect($request,  $seoLink, $hash);
        }
        return $seoLink;
    }


    private function redirect(Request $request, SeoLink $seoLink, $hash)
    {
        $aprticipant = $this->participantRepository->findByHashAndEvent($hash, $seoLink->getEvent());
        if (!is_null($aprticipant)) {
            $this->session->set('participant', $aprticipant);
            $url = sprintf("%s://%s/%s", $request->isSecure() ? "https" : "http", $request->getHost(), $seoLink->getName());
            $response = new RedirectResponse($url);
            $response->send();
        }
        return null;
    }

}