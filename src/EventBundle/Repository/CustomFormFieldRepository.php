<?php

namespace Registreo\EventBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Registreo\Entity\CustomFormField;

class CustomFormFieldRepository extends EntityRepository
{
    /**
     * @param CustomFormField $formField
     */
    public function remove(CustomFormField $formField)
    {
        $this->getEntityManager()->remove($formField);
        $this->getEntityManager()->flush();
    }
}
