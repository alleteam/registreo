<?php
namespace Registreo\PaymentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NotificationController extends Controller
{
    /**
     */
    public function indexAction()
    {
        echo $this->container->get('app.dotpay.service')->receivedNotice();
        exit;
    }
}