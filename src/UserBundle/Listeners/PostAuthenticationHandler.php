<?php

namespace Registreo\UserBundle\Listeners;

use Registreo\AppBundle\Service\SecurityService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class AuthenticationHandler
 * @package Registreo\UserBundle\Listeners
 */
class PostAuthenticationHandler implements AuthenticationSuccessHandlerInterface, AuthenticationFailureHandlerInterface
{
    /**
     * @var TokenStorage
     */
    protected $tokenStorage;

    /**
     * @var SecurityService
     */
    protected $security;

    /**
     * @param TokenStorage $tokenStorage
     * @param SecurityService $securityService
     */
    public function __construct(TokenStorage $tokenStorage, SecurityService $securityService)
    {
        $this->tokenStorage = $tokenStorage;
        $this->security = $securityService;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $result = array('success' => false, 'message' => $exception->getMessage());

        return new JsonResponse($result);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        // keep a trace of user
        $this->security->logClientIp($this->tokenStorage->getToken()->getUser(), $request->getClientIp());

        $result = array('success' => true, 'message' => 'ok');

        return new JsonResponse($result);
    }

}