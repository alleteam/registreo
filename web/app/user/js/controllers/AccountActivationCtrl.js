angular.module('registreoApp')
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('activationinfo', {
                url: "/activationinfo",
                controller: 'AccountActivationCtrl',
                controllerAs: 'accountActivationCtrl',
                templateUrl: "app/user/views/accountActivation.html"
            });    
        }
    ])
    .controller('AccountActivationCtrl',['$scope', '$rootScope', 'Breadcrumb', '$location',
        function ($scope, $rootScope, $Breadcrumb, $location) {
            $rootScope.$broadcast('activeMenu', '');

            $rootScope.ngview.show = true;
            $Breadcrumb.set('Rejestracja', [{name: 'Rejestracja', uri: '/signup'}]);

            $scope.status = $location.search().status;

            $scope.goToSignIn = function() {
                $location.path('/signin');
            }
        }
    ]);



