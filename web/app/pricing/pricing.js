(function () {
    angular.module('registreoApp.pricing', ['registreoApp.pricingController'])
        .config(config);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('pricing', {
                url: '/pricing',
                templateUrl: 'app/pricing/pricing.html',
                controller: 'PricingController',
                controllerAs: 'pricingCtrl'
            });
    }
})();
