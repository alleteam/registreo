(function () {
    String.prototype.replaceAll = function (target, replacement) {
        return this.split(target).join(replacement);
    };

    angular.module('registreoAppCustom', [
            'ngMaterial',
            'ui.router',
            'ui.router.stateHelper',
            'restangular',
            'angularSpinner',
            'mailerService',
            'notifications',
            'customPageService',
            'vcRecaptcha',
            'nemLogging',
            'uiGmapgoogle-maps',
            'ngSanitize'
        ])
        .config(function(uiGmapGoogleMapApiProvider) {
            uiGmapGoogleMapApiProvider.configure({
              key: 'AIzaSyAufpB2DQMSV7op-P64PxDZ4oBPQWA-6PM'
            })
        })
        .run(function ($window, $mdDialog, Restangular) {
            Restangular.setErrorInterceptor(function (response) {
                if (response.status == 401){
                    $window.location.href = '/signin';
                } else {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .clickOutsideToClose(true)
                            .title('Wystąpił błąd')
                            .content("W trakcie wykonywania operacji, wystąpił błąd. Spróbuj ponownie.")
                            .parent(angular.element(document.querySelector('body')))
                            .ok('Ok')
                    );
                }
                return true;
            });
        })
        .config(function ($urlRouterProvider, stateHelperProvider, RestangularProvider, api) {
            RestangularProvider.setBaseUrl(api.url);
        })
        .config(function($locationProvider) {
            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });
        })
        .constant('api', {
            url: '/api'
        });

})();