angular.module('registreoAppCustom')
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('registration-status', {
                url: "/{eventName}/registration-status",
                controller: 'RegistrationStatusCtrl',
                controllerAs: 'registrationStatusCtrl',
                templateUrl: '/app/custom-page/views/registrationStatus.html'
            });    
        }
    ])
    .controller('RegistrationStatusCtrl',['$rootScope', '$scope', '$location',
        function ($rootScope, $scope, $location) {
            $rootScope.ngview.show = true;
            window.scrollTo(0, 0);

        }
    ]);

