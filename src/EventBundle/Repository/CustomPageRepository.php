<?php

namespace Registreo\EventBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Registreo\Entity\CustomPage;

class CustomPageRepository extends EntityRepository
{
    /**
     * @param CustomPage $page
     */
    public function save(CustomPage $page)
    {
        $this->getEntityManager()->persist($page);
        $this->getEntityManager()->flush();
    }
}
