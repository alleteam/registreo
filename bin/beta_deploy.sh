#!/bin/bash

default="\e[39m"
def=$default
green="\e[92m>>> "
blue="\e[34m>>> "
red="\e[31m>>> "
green_="\e[92m"
basepath=$(pwd)
host=beta.registreo.pl

source "bin/path.sh"

if [ -z "$clean_repo_path" ]; then
  echo -e $red"Var clean_repo_path is not set."$default
  exit 1
fi

ssh="ssh -l www-data -p 2666 -o StrictHostKeyChecking=no" 
ssh_cmd=$ssh" $host cd /var/www;"

# local
cd $clean_repo_path
#git checkout master; git pull;

# rsync stuff
echo -e $green"Rsyncing files..."$default
rsync -avz -e "$ssh" --compress --progress --delete --exclude-from $basepath'/bin/rsync_exclude.txt' ./ $host:/var/www/
echo -e $green"Rsync done."$default
echo -e

echo -e $blue"Installing..."$default
$ssh_cmd 'php composer.phar install'
$ssh_cmd 'php app/console doctrine:schema:update --force -n -e prod'
$ssh_cmd 'bower cache clean'
$ssh_cmd 'bower install'
$ssh_cmd 'sudo rm -rf app/cache/prod; sudo rm -rf app/cache/dev; sudo chmod 777 -R app/cache'
$ssh_cmd 'php app/console assets:install --env=prod'
$ssh_cmd 'php app/console assetic:dump --env=prod'
echo -e $blue"Installed."$default
echo -e

