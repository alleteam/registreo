angular.module('registreoApp')
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('resetPassword', {
                url: "/resetPassword",
                controller: 'ResetPasswordCtrl',
                controllerAs: 'resetPasswordCtrl',
                templateUrl: "app/user/views/resetPassword.html"
            });
    }
    ])
    .controller('ResetPasswordCtrl',['$scope', '$rootScope', 'Breadcrumb', '$location', '$http', 'Restangular',
        function ($scope, $rootScope, $Breadcrumb, $location, $http, $Restangular) {
            $rootScope.$broadcast('activeMenu', '');
            $rootScope.ngview.show = true;
            $Breadcrumb.set('Zmień hasło', [{name: 'Zmień hasło', uri: '/resetPassword'}]);

            $scope.submission = false;
            $scope.passwordPolicy = false;
            $scope.notSame = false;
            var hash = $location.search().hash;

            $scope.send = function() {

                $scope.submission = true;

                if ($scope.resetPasswordForm.$valid) {

                    if ($scope.resetPasswordForm.passwordFirst.$viewValue != $scope.resetPasswordForm.passwordSecond.$viewValue) {
                        $scope.notSame = true;
                        $scope.submission = false;
                    } else {

                        $http({
                            method: 'POST',
                            url: Routing.generate('fos_user_resetting_reset') + '/' + hash,
                            data: $.param({
                                'fos_user_resetting_form[plainPassword][first]': $scope.resetPasswordForm.passwordFirst.$viewValue,
                                'fos_user_resetting_form[plainPassword][second]': $scope.resetPasswordForm.passwordSecond.$viewValue
                            }),
                            headers: {
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        })
                            .then(function successCallback(response) {
                                if (!response.error) {
                                    $location.path('/myevents');
                                }
                            }, function errorCallback(response) {
                                $scope.passwordPolicy = true;
                                $scope.submission = false;
                            });

                    }

                } else {
                    $scope.passwordPolicy = true;
                    $scope.submission = false;
                }
            };

            $scope.clearError = function() {
                $scope.notSame = false;
                $scope.passwordPolicy = false;
            };

        }
    ]);

