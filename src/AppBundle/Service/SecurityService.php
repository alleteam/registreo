<?php

namespace Registreo\AppBundle\Service;

use Registreo\Entity\AuthLog;
use Registreo\Entity\User;
use Registreo\UserBundle\Repository\AuthLogRepository;

class SecurityService
{
    /**
     * @var AuthLogRepository
     */
    private $logRepository;

    /**
     * @param AuthLogRepository $logRepository
     */
    public function __construct(AuthLogRepository $logRepository)
    {
        $this->logRepository = $logRepository;
    }

    /**
     * @param string $input
     * @return string
     */
    public function sanitize($input='')
    {
        return strip_tags(trim($input));
    }

    /**
     * Log client IP address when logging into the service
     * 
     * @param User $user
     * @param $ipAddress
     */
    public function logClientIp(User $user, $ipAddress)
    {
        $authenticationLog = new AuthLog();
        $authenticationLog->setUser($user)
            ->setIp($ipAddress)
            ->setWhen(new \DateTime('now'));
        
        $this->logRepository->save($authenticationLog);
    }
}