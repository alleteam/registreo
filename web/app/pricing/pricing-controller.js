angular.module('registreoApp.pricingController', [])
    .controller('PricingController',['$rootScope', 'Breadcrumb',
        function ($rootScope, $Breadcrumb) {
            $rootScope.$broadcast('activeMenu', 'pricing');
            $rootScope.ngview.show = true;
            window.scrollTo(0, 0);
            $Breadcrumb.set('Cennik', [{name: 'Cennik', uri: '/pricing'}]);
        }
    ]);


