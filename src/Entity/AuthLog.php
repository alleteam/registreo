<?php
namespace Registreo\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Registreo\Entity\User as UserModel;

/**
 * Authentication logs model
 *
 * @ORM\Entity(repositoryClass="Registreo\UserBundle\Repository\AuthLogRepository")
 * @ORM\Table(name="auth_log")
 */
class AuthLog
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var UserModel
     *
     * @ORM\ManyToOne(targetEntity="User")
     */
    protected $user;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string")
     */
    protected $ip;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="`when`", type="datetime")
     */
    protected $when;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return AuthLog
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     * @return AuthLog
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getWhen()
    {
        return $this->when;
    }

    /**
     * @param DateTime $when
     * @return AuthLog
     */
    public function setWhen($when)
    {
        $this->when = $when;
        return $this;
    }
}