angular.module('registreoApp')
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('rememberPassword', {
                url: "/rememberPassword",
                controller: 'RememberPasswordCtrl',
                controllerAs: 'rememberPasswordCtrl',
                templateUrl: "app/user/views/rememberPassword.html"
            });    
        }
    ])
    .controller('RememberPasswordCtrl',['$scope', '$rootScope', 'Breadcrumb', '$location', '$http', 'NotificationsService',
        function ($scope, $rootScope, $Breadcrumb, $location, $http, $NotificationsService) {
            $rootScope.$broadcast('activeMenu', '');
            $rootScope.ngview.show = true;
            $Breadcrumb.set('Przypomnij hasło', [{name: 'Przypomnij hasło', uri: '/rememberPassword'}]);

            $scope.submission = false;
            $scope.emptyFields = false;
            $scope.notRecognized = false;
            $scope.emailSend = false;

            $scope.send = function() {

                $scope.submission = true;

                if ($scope.rememberPasswordForm.$valid) {

                   $http({
                        method: 'POST',
                        url: Routing.generate('fos_user_resetting_send_email'),
                        data: $.param({
                            username: $scope.rememberPasswordForm.email.$viewValue
                        }),
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded'
                        }
                    })
                       .then(function successCallback(response) {
                           if (!response.error) {
                               $NotificationsService.addNotification('Na podany adres e-mail zostało wysłane hasło', 'info');
                               $scope.submission = false;
                               $scope.emailSend = true;
                           }
                       }, function errorCallback(response) {
                           $scope.notRecognized = true;
                           $scope.submission = false;
                       });

                } else {
                    $scope.emptyFields = true;
                    $scope.submission = false;
                }
            };

            $scope.clearError = function() {
                $scope.notRecognized = false;
                $scope.emptyFields = false;
            };
            
        }
    ]);

