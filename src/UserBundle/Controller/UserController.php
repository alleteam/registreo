<?php

namespace Registreo\UserBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as WEB;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * @codeCoverageIgnore
 */
class UserController extends FOSRestController
{
    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return Response
     */
    public function getSessionUserAction()
    {
        return $this->handleView($this->view($this->getUser()));
    }
}
