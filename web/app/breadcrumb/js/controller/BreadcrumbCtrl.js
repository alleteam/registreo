angular.module('registreoApp')
    .controller('BreadcrumbCtrl',['$scope', '$rootScope' ,'$location', '$window', 'Breadcrumb',
        function ($scope, $rootScope, $location, $window, $Breadcrumb) {
            $scope.breadcrumbVisible = false;
            $scope.breadcrumb = {
                title: "",
                links: []
            };
           
            $scope.openHomePage = function() {
                $location.path('/');
                $window.open('/', "_self");
            };
            
            $scope.openPage = function(uri) {
                $location.path(uri);
            };

            
            
            $scope.$on('onChangeBreadcrumb', function () {
                $scope.breadcrumbVisible = $Breadcrumb.visible;
                $scope.breadcrumb = $Breadcrumb.breadcrumb;
            });
            
            $rootScope.$broadcast('onLoadedHeader', {});
        }
    ]);

