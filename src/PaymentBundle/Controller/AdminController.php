<?php

namespace Registreo\PaymentBundle\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as WEB;
use Registreo\AppBundle\Exception\ResourceNotFoundException;
use Registreo\PaymentBundle\Service\TransactionService;
use Registreo\Entity\Event;
use Registreo\Entity\EventPayout;

class AdminController extends FOSRestController
{

    /**
     * @var TransactionService
     */
    private $transactionService;

    /**
     * @param TransactionService $transactionService
     */
    public function __construct(ContainerInterface $container, TransactionService $transactionService)
    {
        $this->container = $container;
        $this->transactionService = $transactionService;
    }

    /**
     * @Rest\Route("/event/{event}/transactions/summary")
     * @Web\Method({"GET"})
     * @WEB\ParamConverter("event", class="Registreo:Event")
     *
     * @param Event $event
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function getTransactionsSummaryAction(Event $event)
    {
        if (empty($event)) {
            throw new ResourceNotFoundException('Event not found');
        }
        $view = $this->view($this->transactionService->getSummaryEvent($event));
        return $this->handleView($view);
    }

    /**
     * @Rest\Route("/event/{event}/transactions/short")
     * @Web\Method({"GET"})
     * @WEB\ParamConverter("event", class="Registreo:Event")
     *
     * @param Event $event
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function getTransactionsShortAction(Event $event)
    {
        if (empty($event)) {
            throw new ResourceNotFoundException('Event not found');
        }
        $view = $this->view($this->transactionService->getTransactionShortList($event));
        return $this->handleView($view);
    }

    /**
     * @Rest\Route("/event/{event}/transactions")
     * @Web\Method({"GET"})
     * @WEB\ParamConverter("event", class="Registreo:Event")
     *
     * @param Event $event
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function getTransactionsAction(Event $event)
    {
        if (empty($event)) {
            throw new ResourceNotFoundException('Event not found');
        }
        $view = $this->view($this->transactionService->getTransactionList($event));
        return $this->handleView($view);
    }

    /**
     * @Rest\Route("/event/{event}/payouts")
     * @Web\Method({"GET"})
     * @WEB\ParamConverter("event", class="Registreo:Event")
     *
     * @param Event $event
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function getPayoutsAction(Event $event)
    {
        if (empty($event)) {
            throw new ResourceNotFoundException('Event not found');
        }
        $view = $this->view($this->transactionService->getPayoutList($event));
        return $this->handleView($view);
    }

    /**
     * @Rest\Route("/event/{event}/payout")
     * @Web\Method({"POST"})
     * @WEB\ParamConverter("event", class="Registreo:Event")
     * @WEB\ParamConverter("payout", converter="fos_rest.request_body")
     *
     * @param Event $event
     * @param EventPayout $payout
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function postPayoutAction(Event $event, EventPayout $payout)
    {
        if (empty($event)) {
            throw new ResourceNotFoundException('Event not found');
        }
        $view = $this->view($this->transactionService->createPayout($event, $payout));
        return $this->handleView($view);
    }
}