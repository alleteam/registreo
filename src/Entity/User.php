<?php

namespace Registreo\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as ORMAssert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="Registreo\UserBundle\Repository\UserRepository")
 * @ORM\Table(name="users")
 * @ORM\HasLifecycleCallbacks
 * @ORMAssert\UniqueEntity(fields={"email"}, groups={"custom_registration"}, message="Email must be unique")
 * @JMS\ExclusionPolicy("all")
 */
class User extends BaseUser implements AdvancedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var float
     *
     * @ORM\Column(name="discount", type="float", options={"default" = 0})
     */
    protected $discount;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="regulation_general_accepted", type="datetime")
     * @Assert\NotNull
     */
    protected $regulation;

    /**
     * @var int
     *
     * @ORM\Column(name="is_company", type="integer", options={"default" = 0})
     */
    protected $isCompany;

    /**
     * @var string
     *
     * @ORM\Column(name="nip", type="string", nullable=true)
     */
    protected $nip;

    /**
     * @var float
     *
     * @ORM\Column(name="free_account", type="integer", options={"default" = 0})
     * @JMS\Expose
     */
    protected $freeAccount;

    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", nullable=true)
     */
    protected $companyName;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function fillUsername()
    {
        $this->setUsername($this->getEmail());
    }

    /**
     * @JMS\PreSerialize
     */
    public function preSerialize()
    {
        $this->password = null;
        $this->salt = null;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return DateTime
     */
    public function getRegulation()
    {
        return $this->regulation;
    }

    /**
     * @param DateTime $regulation
     */
    public function setRegulation($regulation)
    {
        $this->regulation = $regulation;
    }

    /**
     * @return int
     */
    public function getIsCompany()
    {
        return $this->isCompany;
    }

    /**
     * @param int $isCompany
     */
    public function setIsCompany($isCompany)
    {
        $this->isCompany = $isCompany;
    }

    /**
     * @return int
     */
    public function getNip()
    {
        return $this->nip;
    }

    /**
     * @param int $nip
     */
    public function setNip($nip)
    {
        $this->nip = $nip;
    }

    /**
     * @return int
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param int $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return int
     */
    public function getFreeAccount()
    {
        return $this->freeAccount;
    }

    /**
     * @param int $freeAccount
     */
    public function setFreeAccount($freeAccount)
    {
        $this->freeAccount = $freeAccount;
    }
}
