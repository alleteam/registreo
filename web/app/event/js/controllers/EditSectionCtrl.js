angular.module('registreoApp')
    .controller('EditSectionCtrl',['$scope', '$rootScope', '$uibModalInstance', 'param',  'FileUploader', 
        function ($scope, $rootScope, $uibModalInstance, param, FileUploader) {
        
            
        $rootScope.ngview.show = true;    
        $scope.element = param.element;
        $scope.eventId = param.eventId;
        $scope.lock = param.lock;
        $scope.uploader = {isHTML5: true};

        if (!$scope.element.type) {
            $scope.element.type = 'Paragraph';
        }

        $scope.mapLoaded = false;

        $scope.sectionTypeList = [
            {type: 'Paragraph', title: 'Pole tekstowe'},
            {type: 'Contact', title: 'Dane kontaktowe'},
            {type: 'ContactForm', title: 'Formularz kontaktowy'},
            {type: 'Map', title: 'Lokalizacja na mapie'},
            {type: 'Image', title: 'Zdjęcie'},
            {type: 'Carousel', title: 'Lista sponsorów'}
        ];
    
            
        $scope.closeModal = function () {
            if ($scope.element.type == 'ContactForm') {
                $scope.element.event_id = $scope.eventId;
            }
            $uibModalInstance.close();
        };     
        
        
        $scope.tinymceOptions = {
            inline: false,
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            skin: 'lightgray',
            theme : 'modern'
        };
        
        /** uploader */
        $scope.uploader = new FileUploader({
            url: '/api/files',
            autoUpload: true,
            queueLimit: 20
        });
        
        $scope.uploadImageClick = function() {
            angular.element('.upload-button-image').click();
        };
        
        $scope.uploadCarouselClick = function() {
            angular.element('.upload-button-carousel').click();
        };
        
        $scope.clearQueueLastElement = function () {
            if ($scope.uploader.queue.length > 0) {
                $scope.uploader.queue[$scope.uploader.queue.length - 1].remove();
            }
        };
        
        $scope.uploader.onCompleteItem = function(fileItem, response, status, headers) {
            if (status != 422 && status != 500 && typeof response[0] != 'undefined') {

                if ($scope.element.type == 'Image') {
                    $scope.element.url = response[0].file_path;
                } else if ($scope.element.type == 'Carousel') {
                    if (!$scope.element.brand_list) {
                        $scope.element.brand_list = [];
                    }
                    $scope.element.brand_list.push({url: response[0].file_path});
                }
            }
            $scope.clearQueueLastElement();
        };
        
        
    }]);

