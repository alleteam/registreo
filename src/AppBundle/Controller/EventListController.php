<?php

namespace Registreo\AppBundle\Controller;

use Registreo\AppBundle\Exception\ResourceNotFoundException;
use FOS\RestBundle\Controller\FOSRestController;
use Registreo\EventBundle\Service\EventService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as WEB;

class EventListController extends FOSRestController
{
    /**
     * @var EventService
     */
    private $eventService;

    /**
     * @param EventService $eventService
     */
    public function setEventService(EventService $eventService)
    {
        $this->eventService = $eventService;
    }

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @Rest\Route("/events-list")
     * @Web\Method({"GET"})
     *
     * @return Response
     * @throws ResourceNotFoundException
     */
    public function getEventsListAction()
    {
        return $this->handleView($this->view($this->eventService->getEventsList()));
    }
}
