<?php

namespace Registreo\UserBundle\Controller;

use DateTime;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Registreo\AppBundle\Validator\Constraints\NIP;
use Registreo\UserBundle\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class RegistrationController extends Controller
{
    /**
     * @Route("register/confirm/{token}")
     * @param string $token
     * @return Response
     */
    public function confirmAction($token)
    {
        $userService = $this->get('app.user.service');
        $status = $userService->activateUser($token);
        return $this->redirect('/#/activationinfo?status='.$status);
    }

    /**
     * @param Request $request
     * @return null|JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request)
    {
        if ($request->getMethod() !== Request::METHOD_POST) {
            return new RedirectResponse('/#/signup');
        }

        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $success = false;
        $companyError = false;
        $errorObject = new \stdClass();
        $errorObject->email = '';
        $errorObject->password = '';

        $user = $userManager->createUser();
        $user->setEnabled(true);
        $user->setRegulation(new DateTime());
        $user->setIsCompany(($request->get('isCompany') == "true") ? 1 : 0);
        $user->setNip($request->get('nip'));
        $user->setCompanyName($request->get('companyName'));
        $user->setDiscount(0);

        if ($request->get('isCompany') == "true") {
            $nip = $request->get('nip');
            $validator = $this->get('app.signup.validator.nip');
            $nipConstraint = new NIP();

            if (!$validator->validate($nip, $nipConstraint)) {
                $errorObject->nip = 'Niepoprawny numer nip.';
                $companyError = true;
            }

            if (!$request->get('companyName')) {
                $errorObject->companyName = 'Pole nie może być puste.';
                $companyError = true;
            }
        }

        if (!$request->get('regulation')) {
            $errorObject->regulation = 'Akceptacja regulaminu jest wymagana.';
            return $this->getFailureResponse($errorObject);
        }

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid() && !$companyError) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

            $userManager->updateUser($user);

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('fos_user_registration_confirmed');
                $response = new RedirectResponse($url);
                $success = true;
            }

            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
        } else {
            foreach ($form->get('email')->getErrors() as $error) {
                $errorObject->email .= $error->getMessage();
            }

            foreach ($form->getErrors() as $error) {
                $errorObject->password .= $error->getMessage();
            }
        }

        return ($success) ? $this->getSuccessResponse() : $this->getFailureResponse($errorObject);
    }

    protected function getSuccessResponse()
    {
        $result = array('success' => true, 'message' => 'User registered.');

        return new JsonResponse($result);
    }

    protected function getFailureResponse($errorObject)
    {
        $result = array('success' => false, 'error' => $errorObject);

        return new JsonResponse($result);
    }
}