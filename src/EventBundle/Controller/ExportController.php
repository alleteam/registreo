<?php

namespace Registreo\EventBundle\Controller;

use Registreo\Entity\Participant;
use Registreo\EventBundle\Service\EventService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as WEB;

/**
 * @Route("/export", service="app.export.controller")
 * @WEB\Security("has_role('ROLE_USER')")
 */
class ExportController extends Controller
{
    /**
     * @var EventService
     */
    private $eventService;

    /**
     * @param EventService $eventService
     */
    public function setEventService(EventService $eventService)
    {
        $this->eventService = $eventService;
    }

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @Route("/participants/{eventId}", name="participants-export", requirements={"eventId": "\d+"})
     */
    public function indexAction($eventId)
    {
        $event = $this->eventService->getEvent($eventId);

        if (empty($event) || $this->getUser()->getId() != $event->getUser()->getId()) {
            throw new ResourceNotFoundException('Event not found');
        }

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator("Registreo")
            ->setTitle($event->getName())
            ->setSubject($event->getName())
            ->setKeywords("Registreo " . $event->getName());

        //setData
        $this->setData($phpExcelObject, $event->getParticipants());


        $phpExcelObject->getActiveSheet()->setTitle('Lista uczestników wydarzenia');
        $phpExcelObject->setActiveSheetIndex(0);

        $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
        $response = $this->get('phpexcel')->createStreamedResponse($writer);
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'registreo-participants.xls'
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }

    /**
     * @param $phpExcelObject
     * @param Participant[] $participants
     */
    private function setData(&$phpExcelObject, $participants)
    {
        $activeIndex = $phpExcelObject->setActiveSheetIndex(0);
        //prepare header
        $i = 1;
        foreach ($participants[0]->getFields() as $field) {
            $cell = $this->excelIndexes[$i - 1] . "1";
            $activeIndex->setCellValue($cell, $field->getFormField()->getLabel());
            $i++;
        }
        $cell = $this->excelIndexes[$i - 1] . "1";
        $activeIndex->setCellValue($cell, "Status płatności");

        $i = 1;
        $row = 2;
        foreach ($participants as $participant) {
            foreach ($participant->getFields() as $field) {
                $cell = $this->excelIndexes[$i - 1] . $row;
                $activeIndex->setCellValue($cell, $field->getValue());
                $i++;
            }
            $cell = $this->excelIndexes[$i - 1] . $row;
            $activeIndex->setCellValue($cell, $participant->getTranslatedPaymentStatus());
            $i = 1;
            $row++;
        }
    }

    private $excelIndexes = [
        "A",
        "B",
        "C",
        "D",
        "E",
        "F",
        "G",
        "H",
        "I",
        "J",
        "K",
        "L",
        "M",
        "N",
        "O",
        "P",
        "Q",
        "R",
        "S",
        "T",
        "U",
        "V",
        "W",
        "X",
        "Y",
        "Z"
    ];
}
