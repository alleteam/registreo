<?php
namespace Registreo\CustomPageBundle\Service;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Registreo\EventBundle\Service\SeoService;

class SeoLinkService
{
    /**
     * @var SeoService
     */
    private $seoService;

    public function __construct(SeoService $seoService)
    {
        $this->seoService = $seoService;
    }



    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $entity = $this->seoService->getEvent($request);
        if(!is_null($entity)) {
            $request->attributes->set('_controller', 'Registreo\CustomPageBundle\Controller\DefaultController::indexAction');
            $request->attributes->set('id', $entity->getEvent()->getId());
            $request->attributes->set('_route_params', ['id' => $entity->getEvent()->getId()]);
            $request->attributes->set('route', 'custom-homepage');
        }
    }
}