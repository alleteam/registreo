angular.module('registreoAppCustom').directive('banner', function() {
    return {
        restrict: 'E',
        scope: {
            bannerUrl: '=url'
        },
        templateUrl: '/app/custom-page/views/elements/banner.html'
    };
});