<?php
namespace Registreo\PaymentBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Registreo\Entity\EventTransaction;
use Registreo\Entity\Event;

class EventTransactionRepository extends EntityRepository
{
    /**
     * @param EventTransaction $event
     */
    public function save(EventTransaction $event)
    {
        $this->getEntityManager()->persist($event);
        $this->getEntityManager()->flush();
    }

    public function eventCount(Event $event)
    {
        $query = $this->createQueryBuilder('t');
        $query->select('COUNT(t)');
        $query->where('t.event = :event');
        $query->setParameter('event', $event);
        return (int)$query->getQuery()->getSingleScalarResult();
    }

    public function eventSummary(Event $event)
    {
        $query = $this->createQueryBuilder('t');
        $query->select('SUM(t.amount), SUM(t.dotpayCommission), SUM(t.commission)');
        $query->where('t.event = :event');
        $query->setParameter('event', $event);
        return $query->getQuery()->getSingleResult();
    }

    public function getShortList(Event $event)
    {
        return $this->findBy(
            ['event' => $event],
            ['modified' => 'DESC'],
            20
            );
    }

    public function getList(Event $event)
    {
        return $this->findBy(['event' => $event],['modified' => 'DESC']);
    }
}