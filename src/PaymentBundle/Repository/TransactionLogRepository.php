<?php
namespace Registreo\PaymentBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Registreo\Entity\TransactionLog;

class TransactionLogRepository extends EntityRepository
{
    /**
     * @param TransactionLog $log
     */
    public function save(TransactionLog $log)
    {
        $this->getEntityManager()->persist($log);
        $this->getEntityManager()->flush();
    }
}