<?php
namespace Registreo\CustomPageBundle\Element;

use JMS\Serializer\Annotation as JMS;

class ContactForm extends ElementAbstract implements ElementTemplate
{
    /**
     * @JMS\Exclude
     */
    public static $TEMPLATE = '<contact-form event-id="templateData.section_list[#index#].element.event_id" title="templateData.section_list[#index#].element.title" ></contact-form>';


    /**
     * @JMS\Type("string")
     * @var string
     */
    private $title;

    /**
     * @JMS\Type("integer")
     * @var integer
     */
    private $eventId;

    /**
     * @return integer
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    /**
     * @param integer $eventId
     */
    public function setEventId($eventId)
    {
        $this->eventId = $eventId;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getTemplate()
    {
        return self::$TEMPLATE;
    }

}