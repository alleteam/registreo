<?php

namespace Registreo\UserBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as WEB;
use Symfony\Component\HttpFoundation\Request;

/**
 * @WEB\Route("/event")
 */
class EventController extends FOSRestController
{
    /**
     * WEB\Security("has_role('ROLE_USER')")
     * WEB\Route("/views/list.html", name="user_events_list")
     * WEB\Template()
     */
    public function listAction()
    {
        return [];
    }

}