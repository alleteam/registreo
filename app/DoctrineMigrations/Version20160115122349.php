<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Entity\Privilege;
use UserBundle\Entity\UserType;
use UserBundle\Entity\UserTypePrivilege;
use Application\Migrations\databaseMigration;

class Version20160115122349 extends AbstractMigration implements ContainerAwareInterface
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        /*
        $table = $schema->createTable("users");
        $table->addColumn("id", "integer")->setAutoincrement(true);
        $table->addColumn("username", "string");
        $table->addColumn("email", "string");
        $table->addColumn("password", "string");
        $table->addColumn("is_active", "boolean");
        $table->addColumn("type_id", "integer");
        $table->setPrimaryKey(["id"]);

        $table = $schema->createTable("privileges");
        $table->addColumn("id", "integer")->setAutoincrement(true);
        $table->addColumn("name", "string");
        $table->setPrimaryKey(["id"]);

        $table = $schema->createTable("user_types");
        $table->addColumn("id", "integer")->setAutoincrement(false);
        $table->addColumn("name", "string");
        $table->setPrimaryKey(["id"]);

        $table = $schema->createTable("user_type_privilege");
        $table->addColumn("user_type_id", "integer");
        $table->addColumn("privilege_id", "integer");
        $table->addForeignKeyConstraint('user_types', array('user_type_id'), array('id'));
        $table->addForeignKeyConstraint('privileges', array('privilege_id'), array('id'));
        $table->setPrimaryKey(["user_type_id", "privilege_id"]);
        */
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        //$schema->dropTable("users");
        //$schema->dropTable("privileges");
        //$schema->dropTable("user_types");
        //$schema->dropTable("user_type_privileges");
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        $em = $this->getEntityManager();

        /* add roles */
        $admin = new UserType();
        $admin->setId(UserType::TYPE_ADMIN_ID);
        $admin->setName(UserType::TYPE_ADMIN);
        $em->persist($admin);

        $organizer = new UserType();
        $organizer->setId(UserType::TYPE_EVENT_ORGANIZER_ID);
        $organizer->setName(UserType::TYPE_EVENT_ORGANIZER);
        $em->persist($organizer);

        $participant = new UserType();
        $participant->setId(UserType::TYPE_EVENT_PARTICIPANT_ID);
        $participant->setName(UserType::TYPE_EVENT_PARTICIPANT);
        $em->persist($participant);

        $em->flush();
        /* add privilege */
        $userPrivilege = new Privilege();
        $userPrivilege->setName(Privilege::ROLE_USER);
        $em->persist($userPrivilege);

        $em->flush();
        /* bind privilege and user type */
        $newTypePrivilege = new UserTypePrivilege();
        $newTypePrivilege->setUserTypeId(UserType::TYPE_EVENT_ORGANIZER_ID);
        $newTypePrivilege->setPrivilegeId($organizer->getId());
        $em->persist($newTypePrivilege);

        $em->flush();
    }

    /**
     * @var ContainerInterface
     */
    protected $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getDoctrine()
    {
        return $this->container->get('doctrine');
    }

    public function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }
}
