<?php

namespace Registreo\FileBundle\Controller;

use Registreo\FileBundle\Service\FileService;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\DependencyInjection\ContainerInterface;

class DefaultController extends FOSRestController
{
    /**
     * @var FileService
     */
    private $fileService;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param FileService $fileService
     */
    public function setFileService(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    /**
     * @Rest\Route("/files")
     *
     * @param Request $request
     * @return Response
     */
    public function postFileAction(Request $request)
    {
        $files = '';
        foreach ($request->files as $file) {
            $files[] = $this->fileService->saveFile($file);
        }
        $view = $this->view($files, 200);
        return $this->handleView($view);
    }
}
