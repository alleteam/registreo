angular.module('registreoApp.aboutController', ['ui.bootstrap'])
    .controller('AboutController',['$rootScope', '$scope', 'Breadcrumb',
        function ($rootScope, $scope, $Breadcrumb) {
            $rootScope.$broadcast('activeMenu', 'about');
            $rootScope.ngview.show = true;
            window.scrollTo(0, 0);
            $Breadcrumb.set('O nas', [{name: 'O nas', uri: '/about'}]);
            
            $scope.groups = [
                {
                    title: 'W systemie nie ma potrzebnej mi funkcjonalności',
                    templateUrl: 'app/about/item1.html'
                },
                {
                    title: 'Jak zintegrować konto płatności z Registreo?',
                    templateUrl: 'app/about/item5.html'
                },
                {
                    title: 'Czy można utworzyć tylko formularz rejestracji bez strony informacyjnej?',
                    templateUrl: 'app/about/item2.html'
                },
                {
                    title: 'W jaki sposób rozliczyć się z Registreo?',
                    templateUrl: 'app/about/item3.html'
                },
                {
                    title: 'Czy otrzymam fakturę VAT?',
                    templateUrl: 'app/about/item4.html'
                }
            ];

            $scope.status = {
                isOpen: new Array($scope.groups.length)
            };

            for (var i = 0; i < $scope.status.isOpen.length; i++) {
                $scope.status.isOpen[i] = (i === 0);
            }
        }
    ]);


