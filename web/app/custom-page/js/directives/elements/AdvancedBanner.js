angular.module('registreoAppCustom').directive('advancedBanner', function() {
    return {
        restrict: 'E',
        scope: {
            bannerUrl: '=url',
            title: '=title',
            description: '=description',
            start: '=start',
            time: '=time',
            fee: '=fee',
            participantCount: '=participantCount',
            limit: '=limit'
        },
        templateUrl: '/app/custom-page/views/elements/advanced_banner.html',
        link: function($scope, $this) {
            $scope.registerParticipant = 0;
            $scope.freeTickets = 0;
            
            var section = angular.element($this[0].children[0]);
            var participant = section.find('.participant');
            var ticket = section.find('.ticket');
            
            section.waypoint({
                triggerOnce: true,
                offset: '70%',
                handler: function() {
                    section.addClass('animated fadeInUp');
                }
            });
            
            $scope.addRegisterParticipant = function() {
                if ($scope.registerParticipant < $scope.participantCount) {
                    $scope.registerParticipant++;
                    participant.text($scope.registerParticipant);
                    setTimeout($scope.addRegisterParticipant, 50);
                }
            };

            $scope.addFreeTickets = function() {
                if ($scope.freeTickets < $scope.limit - $scope.participantCount) {
                    $scope.freeTickets++;
                    ticket.text($scope.freeTickets);
                    setTimeout($scope.addFreeTickets, 50);
                }
            };
            
            $scope.addRegisterParticipant();
            $scope.addFreeTickets();
            
        }
        
    };
});
