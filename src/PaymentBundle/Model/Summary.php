<?php

namespace Registreo\PaymentBundle\Model;

class Summary
{
    /**
     * @var double
     */
    private $payment;

    /**
     * @var double
     */
    private $commission;

    /**
     * @var double
     */
    private $dotpayCommission;

    /**
     * @var double
     */
    private $registreoCommission;

    /**
     * @var double
     */
    private $payout;

    /**
     * @var double
     */
    private $discount;

    public function getPayment()
    {
        return $this->payment;
    }

    public function getCommission()
    {
        return $this->commission;
    }

    public function getDotpayCommission()
    {
        return $this->dotpayCommission;
    }

    public function getRegistreoCommission()
    {
        return $this->registreoCommission;
    }

    public function getPayout()
    {
        return $this->payout;
    }

    public function setPayment($payment)
    {
        $this->payment = $payment;
    }

    public function setCommission($commission)
    {
        $this->commission = $commission;
    }

    public function setDotpayCommission($dotpayCommission)
    {
        $this->dotpayCommission = $dotpayCommission;
    }

    public function setRegistreoCommission($registreoCommission)
    {
        $this->registreoCommission = $registreoCommission;
    }

    public function setPayout($payout)
    {
        $this->payout = $payout;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }



}