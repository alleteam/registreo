<?php
namespace Registreo\CustomPageBundle\Element;

use JMS\Serializer\Annotation as JMS;

class Contact extends ElementAbstract implements ElementTemplate
{
    /**
     * @JMS\Exclude
     */
    public static $TEMPLATE = '<contact address="templateData.section_list[#index#].element.address" phone="templateData.section_list[#index#].element.phone"'
        . ' email="templateData.section_list[#index#].element.email" facebook="templateData.section_list[#index#].element.facebook"'
        . ' twitter="templateData.section_list[#index#].element.twitter" google-plus="templateData.section_list[#index#].element.google_plus"'
        . ' pinterest="templateData.section_list[#index#].element.pinterest" youtube="templateData.section_list[#index#].element.youtube" ></contact>';

    /**
     * @JMS\Type("string")
     * @var string
     */
    private $address;

    /**
     * @JMS\Type("string")
     * @var string
     */
    private $phone;
    
    /**
     * @JMS\Type("string")
     * @var string
     */
    private $email;
    
    /**
     * @JMS\Type("string")
     * @var string
     */
    private $facebook;
    
    /**
     * @JMS\Type("string")
     * @var string
     */
    private $twitter;
    
    /**
     * @JMS\Type("string")
     * @var string
     */
    private $googlePlus;
    
    /**
     * @JMS\Type("string")
     * @var string
     */
    private $pinterest;
    
    /**
     * @JMS\Type("string")
     * @var string
     */
    private $youtube;

    public function getAddress()
    {
        return $this->address;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getFacebook()
    {
        return $this->facebook;
    }

    public function getTwitter()
    {
        return $this->twitter;
    }

    public function getGooglePlus()
    {
        return $this->googlePlus;
    }

    public function getPinterest()
    {
        return $this->pinterest;
    }

    public function getYoutube()
    {
        return $this->youtube;
    }

    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;
        return $this;
    }

    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;
        return $this;
    }

    public function setGooglePlus($googlePlus)
    {
        $this->googlePlus = $googlePlus;
        return $this;
    }

    public function setPinterest($pinterest)
    {
        $this->pinterest = $pinterest;
        return $this;
    }

    public function setYoutube($youtube)
    {
        $this->youtube = $youtube;
        return $this;
    }

    public function getTemplate()
    {
        return self::$TEMPLATE;
    }


}