<?php

namespace Registreo\AppBundle\Exception;

/**
 * @codeCoverageIgnore
 */
class ResourceNotFoundException extends \Exception
{
}
