<?php

namespace Registreo\PaymentBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Registreo\Entity\TransactionLog;
use Registreo\Entity\EventTransaction;
use Registreo\Entity\EventPayout;

class TransactionCalculateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('transaction:calculate')
            ->setDescription('calculate provision for transactions');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('calculate Start');
        $repository = $this->getContainer()->get('app.repository.transaction.log');
        $logList = $repository->findByDirty(true);
        foreach($logList as $log) {
            $this->processLog($output, $log);
        }
        $output->writeln('calculate End');
    }


    private function processLog(OutputInterface $output, TransactionLog $log)
    {
        $output->writeln('Start process for: ' . $log->getNumber());
        $logRepository = $this->getContainer()->get('app.repository.transaction.log');
        if ($log->getStatus() == 'completed') {
            $this->createTransaction($log);
        }
        $log->setDirty(false);
        $logRepository->save($log);
    }

    private function createTransaction(TransactionLog $log)
    {
        $controlArr = explode(':',$log->getControl());
        if (count($controlArr) == 2) {
            if ($controlArr[0] == 'p') {
                $this->createEventTransaction($log, $controlArr[1]);
            } else if ($controlArr[0] == 'ep') {
                $this->createPayoutTransaction($log, $controlArr[1]);
            }
        }
    }


    private function createEventTransaction(TransactionLog $log, $participantId)
    {
        $participant = $this->getContainer()->get('app.repository.participant')->find($participantId);
        if ($participant) {
            $repo = $this->getContainer()->get('app.repository.event.transaction');
            $count = $repo->eventCount($participant->getEvent());
            $transaction = $repo->findOneBy(['participant' => $participant, 'event' => $participant->getEvent()]);
            if (!$transaction) {
                $transaction = new EventTransaction();
                $transaction->setParticipant($participant);
                $transaction->setEvent($participant->getEvent());
                $transaction->setAdded(new \DateTime());
            }
            $transaction->setModified(new \DateTime());
            $transaction->setAmount($log->getAmmount());
            $transaction->setNumber($log->getNumber());
            $this->calculateDotpayCommission($transaction, $log);
            $this->calculateCommission($transaction, $log->getAmmount(), $count);
            $repo->save($transaction);
        }
    }

    private function createPayoutTransaction(TransactionLog $log, $payoutId)
    {
        $repo = $this->getContainer()->get('app.repository.event.payout');
        $payout = $repo->find($payoutId);
        if ($payout)
        {
            $payout->setNumber($log->getNumber());
            $payout->setDotpayCommission($log->getCommission());
            $payout->setStatus($log->getStatus());
            $payout->setModified(new \DateTime());
            $repo->save($payout);
        }
    }

    private function calculateCommission(EventTransaction $transaction, $amount, $count)
    {
        if ($count < 100) {
            $transaction->setPercent(1.2);
            $transaction->setCommission(-$amount * 0.012);
        } else if ($count <= 500) {
            $transaction->setPercent(1);
            $transaction->setCommission(-$amount * 0.01);
        } else {
            $transaction->setPercent(0.8);
            $transaction->setCommission(-$amount * 0.008);
        }
    }

    private function calculateDotpayCommission(EventTransaction $transaction, TransactionLog $log)
    {
        //prowizja z VAT
        if ($log->getCommission() == 0) {
            $commission = $log->getCommission() * 0.0235;
            if ($commission < 0.37) {
                $commission = 0.37;
            }
            $transaction->setDotpayCommission($commission);
        } else {
            $transaction->setDotpayCommission($log->getCommission() * 1.23);
        }
    }


}
