angular.module('registreoAppCustom').directive('contactForm', ['MailerService', 'NotificationsService', 'vcRecaptchaService',
    function(MailerService, NotificationsService, recaptcha) {
    return {
        restrict: 'E',
        scope: {
            title: '=title',
            eventId: '=eventId'
        },
        templateUrl: '/app/custom-page/views/elements/contact_form.html',
        link: function($scope) {
            $scope.contact = {};
            $scope.recaptchaPublicKey = '6LeQgxQUAAAAABDHmMR3iAKdI8Grh3uEx9_jcLGn';
            $scope.widgetId = '';
            $scope.sendMessageLoader = false;

            $scope.reset = function(form) {
                $scope.contact = {};
                form.$setPristine();
                form.$setUntouched();
                recaptcha.reload($scope.widgetId);
            };

            $scope.setWidgetId = function (widgetId) {
                $scope.widgetId = widgetId;
            };
            
            $scope.sendContactMessage = function (form) {
                form.$submitted = true;
                if(form.$valid && $scope.sendMessageLoader == false) {
                    $scope.sendMessageLoader = true;

                    MailerService.sendEventMessage($scope.contact, $scope.eventId).then(function (response) {
                        if (response.status == 204) {
                            NotificationsService.addNotification('Twoje zapytanie zostało wysłane.');
                            $scope.reset(form);
                        } else {
                            NotificationsService.addNotification('Wystąpił problem techniczny. Spróbuj wysłać wiadomość ponownie', NotificationsService.ERROR_NOTIFICATION);
                        }
                        $scope.sendMessageLoader = false;
                    });
                }
            };
        }
    };
}]);
