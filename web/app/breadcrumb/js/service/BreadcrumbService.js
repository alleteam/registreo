angular.module('registreoApp')
    .service('Breadcrumb', ['$rootScope', function($rootScope) {
        this.visible = false;
        this.breadcrumb = {
                title: "",
                links: []
            };
        
        this.set = function(title, links) {
            this.breadcrumb.title = title;
            this.breadcrumb.links = links;
            this.visible = true;
            $rootScope.$broadcast('onChangeBreadcrumb', {});
        };    
        
        this.hide = function() {
            this.visible = false;
            $rootScope.$broadcast('onChangeBreadcrumb', {});
        };    

        $rootScope.$on('onLoadedHeader', function () {
            $rootScope.$broadcast('onChangeBreadcrumb', {});
        });
    }]);

