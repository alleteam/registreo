angular.module('registreoAppCustom').directive('mainParagraph', function() {
    return {
        restrict: 'E',
        scope: {
            title: '=title',
            description: '=description'
        },
        templateUrl: '/app/custom-page/views/elements/main_paragraph.html'
    };
});