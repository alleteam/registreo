angular.module('registreoApp')
    .config(function ($urlRouterProvider, $stateProvider) {
        $stateProvider
            .state('event', {
                abstract: true,
                url: '/event/{eventId:int}',
                templateUrl: "app/event/views/manageTabs.html",
                controller: function ($scope, $state, $stateParams, $rootScope) {
                    $rootScope.ngview.show = true;
                    var self = this;
                    self.eventId = $stateParams.eventId;
                    $scope.$on('$stateChangeSuccess', function (event, toState) {
                        $scope.currentTab = toState.data.selectedTab;
                    });
                },
                controllerAs: 'eventsManageCtrl',
                resolve:{
                    checkAuth: ['UserService', function(UserService){
                        return UserService.checkAuth();
                    }]
                }
            })
            .state('event.edit', {
                url: '/edit',
                data: {
                    'selectedTab': 0
                },
                'views': {
                    'edit': {
                        templateUrl: "app/event/views/tabEdit.html",
                        controller: 'EventEditCtrl'
                    }
                }
            })
            .state('event.page', {
                url: '/page',
                data: {
                    'selectedTab': 1
                },
                'views': {
                    'page': {
                        templateUrl: "app/event/views/tabCustomPage.html",
                        controller: "EventCustomPageCtrl",
                        controllerAs: 'customPageCtrl'
                    }
                }
            })
            .state('event.form', {
                url: '/form',
                data: {
                    'selectedTab': 2
                },
                'views': {
                    'form': {
                        templateUrl: "app/event/views/tabCustomForm.html",
                        controller: "EventCustomFormCtrl"
                    }
                }
            })
    });
