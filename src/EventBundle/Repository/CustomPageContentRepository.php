<?php

namespace Registreo\EventBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Registreo\Entity\CustomPageContent;

class CustomPageContentRepository extends EntityRepository
{

    /**
     * @param CustomPageContent $pageContent
     */
    public function remove(CustomPageContent $pageContent)
    {
        $this->getEntityManager()->remove($pageContent);
        $this->getEntityManager()->flush();
    }
}
