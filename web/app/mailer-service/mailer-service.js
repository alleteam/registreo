(function () {
    'use strict';

    angular.module('mailerService', ['restangular'])
        .service('MailerService', mailerService);

    mailerService.$inject = ['Restangular', '$q'];

    function mailerService(Restangular, $q) {
        var self = this;

        self.restangularFullResponse = Restangular.withConfig(function(RestangularConfigurer) {
            RestangularConfigurer.setFullResponse(true);
        });

        self.sendMessage = function (content) {
            var defer = $q.defer();
            self.restangularFullResponse.all('contact').all('notifications').post({content : content}).then(function (data) {
                defer.resolve(data);
            });
            return defer.promise;
        };

        self.sendEventMessage = function (content, eventId) {
            var defer = $q.defer();
            self.restangularFullResponse.one('events', eventId).all('notifications').post({content : content}).then(function (data) {
                defer.resolve(data);
            });
            return defer.promise;
        };
    }
})();