angular.module('registreoApp.eventEditController', [])
    .controller('EventEditCtrl',['$filter', '$scope', '$rootScope', 'Breadcrumb', '$location', '$stateParams', 'EventService', 'NotificationsService', 'UserService',
        function ($filter, $scope, $rootScope, $Breadcrumb, $location, $stateParams, EventService, NotificationsService, UserService) {
            $rootScope.$broadcast('activeMenu', 'myaccount');
            $rootScope.ngview.show = true;
            $Breadcrumb.set('Edytuj wydarzenie', [
                {name: 'Moje konto', uri: '/myaccount'}, {name: 'Moje wydarzenia', uri: '/myevents'},
                {name: 'Edytuj wydarzenie', uri: '/event/'+ $stateParams.eventId +'/edit'}
            ]);
            window.scrollTo(0, 0);
            $scope.editEventLoading = true;
            $scope.edit = true;
            $scope.required = "";
            $scope.user = {};
            $scope.loadCurrentUser = true;

            UserService.getCurrentUser().then(function (user) {
                $scope.user = user;
                $scope.loadCurrentUser = false;
            });

            EventService.getEvent($stateParams.eventId).then(function (event) {
                $scope.event = event;
                $scope.event.id = $stateParams.eventId;
                event.event_date = new Date(event.start_date);
                event.event_hour = new Date(event.start_date);
                event.registration_date = new Date(event.registration_date);

                var today = new Date();
                $scope.today = new Date(today.getTime() + 24 * 60 * 60 * 1000);
                $scope.maxDate = new Date(today.setFullYear(today.getFullYear() + 2));
                $scope.editEventLoading = false;
                $scope.event.regulation_accept = true;
            });

            $scope.saveForm = function(isValid) {
                if(isValid) {
                    $scope.eventSaveing = true;
                    if($scope.user.free_account == 1) {
                            $scope.saveData();
                    } else {
                        EventService.validateIban($scope.event.account_number).then(function (response) {
                            if (response.status == 204) {
                                $scope.saveData();
                            } else {
                                $scope.invalidIban = true;
                                $scope.eventSaveing = false;
                            }
                        });
                    }
                }
            };

            $scope.saveData = function() {
                var startHour = new Date($scope.event.event_hour).toLocaleTimeString();
                var originalRegistrationDate = $scope.event.registration_date;
                var startDate = EventService.formatPickerDate($scope.event.event_date);
                var registrationDate = EventService.formatPickerDate($scope.event.registration_date);

                $scope.event.start_date = startDate + " " + startHour;
                $scope.event.registration_date = registrationDate + "T23:59:59+02:00";

                EventService.saveEvent($scope.event).then(function (status) {
                    window.scrollTo(0, 0);
                    $scope.eventSaveing = false;
                    $scope.event.registration_date = originalRegistrationDate;
                    NotificationsService.addNotification('Twoje zmiany zostały zapisane.');
                });
            }

        }
    ]);
