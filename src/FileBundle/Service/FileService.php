<?php

namespace Registreo\FileBundle\Service;

use Registreo\Entity\File;
use Registreo\FileBundle\Repository\FileRepository;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileService
{
    /**
     * @var FileRepository
     */
    protected $fileRepository;

    /**
     * @param FileRepository $fileRepository
     */
    public function setFileRepository(FileRepository $fileRepository)
    {
        $this->fileRepository = $fileRepository;
    }

    /**
     * @param UploadedFile $file
     * @return File;
     */
    public function saveFile($file)
    {
        //TODO : send file to S3, save file entity and return file enity with file path
        $newName = '/upload/' . $this->generateFileName() . '.jpg';

        $path = WEB_DIRECTORY . $newName;
        if (!file_exists(WEB_DIRECTORY . "/upload")) {
            mkdir(WEB_DIRECTORY . "/upload", '0755', true);
        }
        ob_start();
        system('convert '. $file->getPathname() . ' ' . $path);
        ob_end_clean();
        $fileEntity = new File();
        $fileEntity->setFilePath($newName);
        $this->fileRepository->save($fileEntity);

        return $fileEntity;
    }

    /**
     * @return string random hash
     */
    protected function generateFileName()
    {
        return bin2hex(openssl_random_pseudo_bytes(4)) . '-' . bin2hex(openssl_random_pseudo_bytes(10));
    }
}
