#!/bin/bash

default="\e[39m"
def=$default
green="\e[92m>>> "
blue="\e[34m>>> "
red="\e[31m>>> "
green_="\e[92m"
basepath=$(pwd)
host='registreo.pl'

source "bin/path.sh"

if [ -z "$clean_repo_path" ]; then
  echo -e $red"Var clean_repo_path is not set."$default
  exit 1
fi

ssh="ssh -l root -oStrictHostKeyChecking=no" 
ssh_cmd=$ssh" $host cd /var/www;"

# local
cd $clean_repo_path
git checkout master; git pull;

# extra cache clear
$ssh_cmd 'rm -rf app/cache/prod; chmod 777 -R app/cache'

# rsync stuff
echo -e $green"Rsyncing files..."$default
rsync -avz -e "$ssh" --compress --progress --delete --exclude-from $basepath'/bin/rsync_exclude.txt' ./ $host:/var/www/
echo -e $green"Rsync done."$default
echo -e

echo -e $blue"Installing..."$default
$ssh_cmd 'export SYMFONY_ENV=prod; php composer.phar install --no-dev --optimize-autoloader'
$ssh_cmd 'php app/console doctrine:schema:update --force -n -e prod'
$ssh_cmd 'bower cache clean'
$ssh_cmd 'bower install'
$ssh_cmd 'rm -rf app/cache/prod; chmod 777 -R app/cache'
$ssh_cmd 'php app/console assets:install --env=prod'
$ssh_cmd 'php app/console assetic:dump --env=prod'
$ssh_cmd 'sudo /etc/init.d/nginx restart && sudo /etc/init.d/php7.0-fpm restart'
echo -e $blue"Installed."$default
echo -e

