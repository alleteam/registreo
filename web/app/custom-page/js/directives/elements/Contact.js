angular.module('registreoAppCustom').directive('contact', function() {
    return {
        restrict: 'E',
        scope: {
            address: '=address',
            phone: '=phone',
            email: '=email',
            facebook: '=facebook',
            twitter: '=twitter',
            googlePlus: '=googlePlus',
            pinterest: '=pinterest',
            youtube: '=youtube'
        },
        templateUrl: '/app/custom-page/views/elements/contact.html'
    };
});