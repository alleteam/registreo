(function () {
    angular.module('notifications')
        .controller('NotificationsController', notificationController);

    notificationController.$inject = ['NotificationsService', '$scope'];

    function notificationController(NotificationsService, $scope) {
        var self = this;

        self.notificationsSrvc = NotificationsService;

        if (typeof $scope.notificationId  == 'undefined')  {
            $scope.notificationId = 1;
        }

        self.getNotifications = function () {
            return self.notificationsSrvc.getNotifications();
        };

        self.removeNotification = function (notification) {
            self.notificationsSrvc.removeNotification(notification);
        };

        self.isAnyNotification = function () {
            return self.getNotifications().length > 0;
        }
    }
})();