(function () {
    'use strict';

    angular.module('registreoApp.eventService', ['restangular'])
        .service('EventService', eventService);

    eventService.$inject = ['Restangular', '$q'];

    function eventService(Restangular, $q) {
        var self = this;

        self.formatPickerDate = function(pickerDate){
            var formattedDate =  new Date(pickerDate).toLocaleDateString('pl-PL', {day:"2-digit", month:"2-digit", year:"numeric"});
            return formattedDate.slice(6,11) + "-" + formattedDate.slice(3,5) + "-" + formattedDate.slice(0,2);
        };

        self.restangularFullResponse = Restangular.withConfig(function(RestangularConfigurer) {
            RestangularConfigurer.setFullResponse(true);
        });

        self.getEvent = function (eventId) {
            var defer = $q.defer();
            Restangular.one('events', eventId).customGET().then(function (data) {
                defer.resolve(data);
            });
            return defer.promise;
        };

        self.inviteParticipant = function (eventId, participant) {
            var defer = $q.defer();
            Restangular.all('events/' + eventId + '/participants/invite').post(participant).then(function (data) {
                defer.resolve(data);
            });
            return defer.promise;
        };


        self.getParticipants = function (eventId, current, limit) {
            var defer = $q.defer();
            Restangular.one('events', eventId).all('participants').getList({'page': current, 'limit': limit}).then(function (data) {
                defer.resolve(data);
            });
            return defer.promise;
        };

        self.getParticipantDetails = function (eventId, participantId) {
            var defer = $q.defer();
            Restangular.one('events', eventId).one('participants', participantId).getList().then(function (data) {
                defer.resolve(data);
            });
            return defer.promise;
        };

        self.changePaymentStatus = function (eventId, participantId) {
            var defer = $q.defer();
            Restangular.one('events', eventId).one('participants', participantId).one('payments').doPUT({status : 3}).then(function (data) {
                defer.resolve(data);
            });
            return defer.promise;
        };

        self.changeEventStatus = function (eventId, status) {
            var defer = $q.defer();
            Restangular.one('events', eventId).one('status').doPUT({status : status}).then(function (data) {
                defer.resolve(data);
            });
            return defer.promise;
        };

        self.getEventsList = function () {
            var defer = $q.defer();
            Restangular.one('events-list').getList().then(function (data) {
                defer.resolve(data);
            });
            return defer.promise;
        };
        
        self.getEvents = function(current) {
            var defer = $q.defer();
            Restangular.one('events.json').get().then(function (results) {

                var data = {
                    pagin: {
                        current: current,
                        limit: 1000,
                        total: results.length
                    },
                    events: []
                };

                $.each(results, function( index, value ) {
                    data.events.push(value);
                });
                defer.resolve(data);
            });
            return defer.promise;
        };

        self.saveEvent = function (event) {
            var defer = $q.defer();

            event.start_date = self.formatDate(event.start_date);
            event.dateStart = undefined;
            if (event.id !== undefined) {
                event.user = undefined;
            }
            Restangular.one('events').doPUT(event).then(function (data) {
                defer.resolve(data);
            });

            return defer.promise;
        };

        self.createEvent = function (event) {
            var defer = $q.defer();

            event.start_date = self.formatDate(event.start_date);
            event.dateStart = undefined;
            if (event.id !== undefined) {
                event.user = undefined;
            }
            Restangular.all('events').post(event).then(function (data) {
                defer.resolve(data);
            }, function() {
                defer.reject();
            });

            return defer.promise;
        };

        self.validateIban = function (bankAccount) {
            var defer = $q.defer();
            self.restangularFullResponse.all('events').all('iban').post({bankAccount : bankAccount}).then(function (response) {
                defer.resolve(response);
            });
            return defer.promise;
        };

        self.formatDate = function(d) {
            return moment(d).format();
        };
    }
})();