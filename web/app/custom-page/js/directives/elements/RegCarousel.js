angular.module('registreoAppCustom').directive('regCarousel', function() {
    return {
        restrict: 'E',
        scope: {
            count: '=count',
            images: '=data'
        },
        templateUrl: '/app/custom-page/views/elements/carousel.html',
        link: function($scope, element) {
            var owlElement = element.find('.our-client');
            for (var i in $scope.images) {
                owlElement.append('<div class="client-item"><img src="' + $scope.images[i].url + '" ></div>');
            }
            
            owlElement.owlCarousel({
                items : $scope.count,
                itemsDesktop : [1000,3], //5 items between 1000px and 901px
                itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
                itemsTablet: [600,2], //2 items between 600 and 0;
                itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
                lazyLoad: false,
                autoPlay: true,
                pagination : false
            });
        }
    };
});
