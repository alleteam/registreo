<?php

namespace Registreo\AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use \Http\Client\Curl\Client;
use \Http\Discovery\MessageFactoryDiscovery;
use \Http\Discovery\StreamFactoryDiscovery;
use Symfony\Component\Validator\Constraints\Email;

class InviteNewsletterCommand extends ContainerAwareCommand
{
    const EMAIL_PATH = 'AppBundle:Newsletters:';


    protected function configure()
    {
        $this
            ->setName('newsletter:invite')
            ->setDescription('Send invitation email to specified mailing list')
            //->addOption('list', null, InputOption::VALUE_REQUIRED, 'Mailing list ID')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $htmlMessage = $this->getContainer()->get('templating')->render(self::EMAIL_PATH . 'conference.html.twig');
        //$this->sendToMailingList($htmlMessage);

        $constraints = [ new Email() ];
        $validator = $this->getContainer()->get('validator');
        $fileData = file_get_contents(__DIR__ . '/../../../mails/newsletter-conference.txt');

        foreach ( explode("\n", $fileData) as $email ) {
            $delay = mt_rand(1, 10);
            sleep($delay);
            $errors = $validator->validateValue($email, $constraints);
            if (count($errors) > 0) {
                $output->writeln('Skipping invalid email ' . $email . '!');
                continue;
            }

            $output->writeln('Sending to ' . $email . '...');
            $this->sendInvitation($email, $htmlMessage);
        }

        $output->writeln('All done.');
    }

    protected function sendInvitation($to, $htmlMessage='')
    {


        $mailer = $this->getContainer()->get('swiftmailer.mailer.default');

        $message = \Swift_Message::newInstance()
            ->setSubject("Registreo.pl - rejestracja uczestników online")
            ->setFrom('noreply@registreo.pl', 'Registreo.pl')
            ->setTo($to)
            ->setBody($htmlMessage, 'text/html');

        return $mailer->send($message);
    }

    /**
     * @param string $htmlMessage
     */
    protected function sendToMailingList($htmlMessage='')
    {
     /*   curl -s --user 'api:YOUR_API_KEY' \
    https://api.mailgun.net/v3/YOUR_DOMAIN_NAME/messages \
    -F from='Sender Bob <sbob@YOUR_DOMAIN_NAME>' \
    -F to='alice@example.com' \
    -F subject='Hello' \
    -F text='Testing some Mailgun awesomness!' \
    -F o:deliverytime='Fri, 14 Oct 2011 23:10:10 -0000'
     */

        //$client =  $this->getHttpClient();

        //var_dump($this->getContainer()->parameters['mailgun_key']);

    }

    protected function getHttpClient()
    {
        $options = [
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERPWD => "api:",
            CURLOPT_HTTPAUTH => CURLAUTH_BASIC
        ];

        return new Client(MessageFactoryDiscovery::find(), StreamFactoryDiscovery::find(), $options);
    }
}