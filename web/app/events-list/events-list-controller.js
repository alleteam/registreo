angular.module('registreoApp.eventsListController', [])
    .controller('EventsListController',['$rootScope', 'Breadcrumb', '$scope', 'EventService',
        function ($rootScope, $Breadcrumb, $scope, EventService) {
            $rootScope.$broadcast('activeMenu', 'events');
            $rootScope.ngview.show = true;
            window.scrollTo(0, 0);
            $Breadcrumb.set('Wydarzenia', [{name: 'Wydarzenia', uri: '/events'}]);

            if (!$scope.editEventLoading) {
                $scope.editEventLoading = true;
                EventService.getEventsList().then(function (events) {
                    $scope.editEventLoading = false;
                    $scope.event = events[0];
                    $scope.events = events;
                });
            }
        }
    ])
    .directive('owlCarouselItem', [function() {
        return {
            restrict: 'A',
            transclude: false,
            link: function(scope, element) {
                if(scope.$last) {
                    var owl = $("#latest-post");
                    owl.owlCarousel({
                        items: 3, //10 items above 1000px browser width
                        itemsDesktop: [1000, 3], //5 items between 1000px and 901px
                        itemsDesktopSmall: [900, 2], // 3 items betweem 900px and 601px
                        itemsTablet: [600, 1], //2 items between 600 and 0;
                        itemsMobile: false, // itemsMobile disabled - inherit from itemsTablet option
                        autoPlay: false,
                        navigation: true,
                        mouseDrag: false,
                        lazyLoad: false,
                        navigationText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"]
                    });
                }
            }
        };
    }]);


