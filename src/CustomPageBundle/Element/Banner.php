<?php
namespace Registreo\CustomPageBundle\Element;

use JMS\Serializer\Annotation as JMS;

class Banner implements ElementTemplate
{
    /**
     * @JMS\Exclude
     */
    public static $TEMPLATE = '<banner url="templateData.banner.url" ></banner>';

    /**
     * @JMS\Exclude
     */
    public static $ADVANCED_TEMPLATE = '<advanced-banner url="templateData.banner.url"'
        . ' title="templateData.banner.title" description="templateData.banner.description"'
        . ' start="templateData.banner.start" time="templateData.banner.time"'
        . ' fee="templateData.banner.fee" participant-count="templateData.banner.participant_count"'
        . ' limit="templateData.banner.limit" ></advanced-banner>';

    
    /**
     * @JMS\Type("string")
     * @var string
     */
    private $url;

    /**
     * @JMS\Type("boolean")
     * @var bool
     */
    private $advanced;

    /**
     * @JMS\Type("string")
     */
    private $title;

    /**
     * @JMS\Type("string")
     */
    private $description;

    /**
     * @JMS\Type("DateTime<'d-m-Y'>")
     */
    private $start;

    /**
     * @JMS\Type("DateTime<'H:i'>")
     */
    private $time;

    /**
     * @JMS\Type("float")
     */
    private $fee;

    /**
     * @JMS\Type("integer")
     */
    private $participantCount;

    /**
     * @JMS\Type("integer")
     */
    private $limit;


    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getStart()
    {
        return $this->start;
    }

    public function getTime()
    {
        return $this->time;
    }

    public function getFee()
    {
        return $this->fee;
    }

    public function getParticipantCount()
    {
        return $this->participantCount;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function setStart($start)
    {
        $this->start = $start;
        $this->time = $start;
        return $this;
    }

    public function setFee($fee)
    {
        $this->fee = $fee;
        return $this;
    }

    public function setParticipantCount($participantCount)
    {
        $this->participantCount = $participantCount;
        return $this;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    public function getAdvanced()
    {
        return $this->advanced;
    }

    public function setAdvanced($advanced)
    {
        $this->advanced = $advanced;
        return $this;
    }


    public function getTemplate()
    {
        return $this->advanced ? self::$ADVANCED_TEMPLATE : self::$TEMPLATE;
    }


}