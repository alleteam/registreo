<?php

namespace Registreo\PaymentBundle\Service;

use Registreo\Entity\Participant;
use Symfony\Component\HttpFoundation\RequestStack;
use mirolabs\dotpay\client\payment\DotPay;
use mirolabs\dotpay\client\payment\model\RequestParam;


class PaymentService
{

    /**
     * @var RequestStack
     */
    private $request;

    /**
     * @var DotPay
     */
    private $dotPay;

    /**
     * @var int
     */
    private $accountId;

    public function __construct(RequestStack $request, DotPay $dotpay, $accountId)
    {
        $this->request = $request;
        $this->dotPay = $dotpay;
        $this->accountId = $accountId;
    }

    /**
     * @return RequestParam
     */
    public function getDataForm(Participant $participant)
    {
        $request = new RequestParam();
        $request
            ->setApiVersion('dev')
            ->setId($this->accountId)
            ->setAmount($participant->getEvent()->getFree() ? 0 : $participant->getEvent()->getFee())
            ->setCurrency('PLN')
            ->setChannelGroups([
                RequestParam::CH_CREDIT_CARD,
                RequestParam::CH_FAST_TRANSFER,
                RequestParam::CH_TRANSFER_ONLINE])
            ->setDescription(sprintf("opłata za event: %s, uczestnik: %s",
                $participant->getEvent()->getName(), $participant->getName() . " " . $participant->getSurname()))
            ->setType(RequestParam::RETURN_TYPE_BUTTON_AND_POST)
            ->setButtonText("Wróć do Registreo.pl")
            ->setControl('p:' . $participant->getId())
            ->setFirstname($participant->getName())
            ->setLastname($participant->getSurname())
            ->setEmail($participant->getEmail())
            ->setUrl('https://' . $this->request->getCurrentRequest()->getHost() .
                '/event/' . $participant->getEvent()->getId() . '/payment');
        $this->dotPay->addCheckControlToRequest($request);
        return $request;
    }


}