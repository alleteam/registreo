(function () {
    'use strict';

    angular.module('registreoApp.userService', ['restangular'])
        .service('UserService', userService);

    userService.$inject = ['Restangular', '$q', '$location'];

    function userService(Restangular, $q, $location) {
        var self = this;
        self._currentUser = null;
        var defer = $q.defer();

        self.getCurrentUser = function () {
            if (self._currentUser == null) {
                Restangular.one('session', 'user').get().then(function (data) {
                    self._currentUser = data;
                    defer.resolve(self._currentUser);
                });
            } else {
                defer.resolve(self._currentUser);
            }
            return defer.promise;
        };

        self.logout = function () {
            self._currentUser = null;
        };

        self.checkAuth = function () {
            if(self._currentUser) {
                return;
            }

            Restangular.one('session', 'user').get().then(function (data) {
                self._currentUser = data;
                if (!self._currentUser) {
                    $location.url('/signin');
                }
            });
        };
    }
})();