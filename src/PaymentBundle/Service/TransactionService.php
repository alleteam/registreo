<?php
namespace Registreo\PaymentBundle\Service;

use mirolabs\dotpay\client\payment\Notification;
use mirolabs\dotpay\client\payment\model\Notice;
use Registreo\PaymentBundle\Repository\TransactionLogRepository;
use Registreo\PaymentBundle\Repository\EventTransactionRepository;
use Registreo\PaymentBundle\Repository\EventPayoutRepository;
use Registreo\Entity\TransactionLog;
use Registreo\Entity\Event;
use Registreo\Entity\EventPayout;
use Registreo\PaymentBundle\Model\Summary;

class TransactionService implements Notification
{

    const PAYMENT_MISSING = 0;
    const PAYMENT_NEW = 1;
    const PAYMENT_PROCESSING = 2;
    const PAYMENT_COMPLETED = 3;
    const PAYMENT_REJECTED = 4;
    const PAYMENT_WAITING = 5;
    const PAYMENT_REALIZATION = 6;
    const PAYMENT_FREE = 10;
    const SENT_INVITE = 11;

    /**
     * @var TransactionLogRepository
     */
    private $transacrtioLogRepository;

    /**
     * @var EventTransactionRepository
     */
    private $eventTransactionRepository;

    /**
     * @var EventPayoutRepository
     */
    private $eventPayoutRepository;

    function __construct(TransactionLogRepository $transacrtioLogRepository,
        EventTransactionRepository $eventTransactionRepository,
        EventPayoutRepository $eventPayoutRepository)
    {
        $this->transacrtioLogRepository = $transacrtioLogRepository;
        $this->eventTransactionRepository = $eventTransactionRepository;
        $this->eventPayoutRepository = $eventPayoutRepository;
    }


    public function callNotification(Notice $notice)
    {

	// bug on dotpay test didn't return commision in notification
	$commission = is_null($notice->getOperationCommissionAmount()) ? $notice->getOperationAmount() * 0.028 : $notice->getOperationCommissionAmount();

        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        $log = new TransactionLog();
        $log->setNumber($notice->getOperationNumber());
        $log->setAmmount($notice->getOperationAmount());
        $log->setCommission($commission);
        $log->setControl($notice->getControl());
        $log->setStatus($notice->getOperationStatus());
        $log->setLog($serializer->serialize($notice, 'json'));
        $log->setDirty(true);
        $log->setAdded(new \DateTime());
        $this->transacrtioLogRepository->save($log);
    }

    /**
     *
     * @param Event $event
     * @return Summary
     */
    public function getSummaryEvent(Event $event)
    {
        $transactionSummary = $this->eventTransactionRepository->eventSummary($event);
        $payoutSummary = $this->eventPayoutRepository->eventSummary($event);

        $summary = new Summary();
        $summary->setPayment((float)$transactionSummary[1]);
        $summary->setDotpayCommission($transactionSummary[2]+$payoutSummary[2]);
        $summary->setRegistreoCommission((float)$transactionSummary[3]);
        $summary->setPayout((float)$payoutSummary[1]);
        $summary->setDiscount($event->getDiscount());
        $commission = round(($summary->getDotpayCommission() + $summary->getRegistreoCommission()) * ((100 - $event->getDiscount())/100), 2);
        $summary->setCommission($commission);
        return $summary;
    }

    /**
     *
     * @param Event $event
     * @return array
     */
    public function getTransactionList(Event $event)
    {
        return $this->eventTransactionRepository->getList($event);
    }

    /**
     *
     * @param Event $event
     * @return array
     */
    public function getTransactionShortList(Event $event)
    {
        return $this->eventTransactionRepository->getShortList($event);
    }

    /**
     *
     * @param Event $event
     * @return array
     */
    public function getPayoutList(Event $event)
    {
        return $this->eventPayoutRepository->getList($event);
    }

    /**
     * @param Event $event
     * @param EventPayout $payout
     * @return EventPayout
     */
    public function createPayout(Event $event, EventPayout $payout)
    {
        $maxAmout = $this->getMaxPayout($event);
        if (is_null($payout->getAmount()) || $payout->getAmount() == 0) {
            throw new \Exception('Amount isn\'t set');
        }
        if ($maxAmout < $payout->getAmount()) {
            throw new \Exception('Amount is too big');
        }

        $payout->setEvent($event);
        $payout->setAdded(new \DateTime());
        $payout->setDirty(true);
        $payout->setDotpayCommission(0);
        $payout->setNumber('');
        $payout->setStatus('new');
        $payout->setModified(new \DateTime());
        $payout->setLog('');
        $this->eventPayoutRepository->save($payout);
        return $payout;
    }




    public function getMaxPayout(Event $event)
    {
        $summary = $this->getSummaryEvent($event);
        return $summary->getPayment() + $summary->getCommission() - $summary->getPayout();
    }
}