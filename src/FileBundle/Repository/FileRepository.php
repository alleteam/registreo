<?php

namespace Registreo\FileBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Registreo\Entity\File;

class FileRepository extends EntityRepository
{
    /**
     * @param File $file
     */
    public function save(File $file)
    {
        $this->getEntityManager()->persist($file);
        $this->getEntityManager()->flush();
    }
}
