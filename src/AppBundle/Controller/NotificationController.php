<?php

namespace Registreo\AppBundle\Controller;

use Registreo\AppBundle\Service\NotificationService;
use FOS\RestBundle\Controller\FOSRestController;
use Registreo\AppBundle\Service\SecurityService;
use Registreo\Entity\Event;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as WEB;

class NotificationController extends FOSRestController
{
    /**
     * @var NotificationService
     */
    private $notificationService;

    /**
     * @var SecurityService
     */
    private $security;

    /**
     * @param NotificationService $notificationService
     * @param SecurityService $securityService
     */
    public function __construct(NotificationService $notificationService, SecurityService $securityService)
    {
        $this->notificationService = $notificationService;
        $this->security = $securityService;
    }

    /**
     * @Rest\Route("/contact/notifications")
     *
     * @param Request $request
     * @return Response
     */
    public function postContactNotificationAction(Request $request)
    {
        $content = $request->get('content');
        $this->notificationService->sendContactMessage(
            $content['name'],
            $content['email'],
            $content['title'],
            $content['comment']
        );

        return new Response(null, 204);
    }

    /**
     * @Rest\Route("/events/{event}/notifications")
     * @Web\Method({"POST"})
     * @WEB\ParamConverter("event", class="Registreo:Event")
     *
     * @param Event   $event
     * @param Request $request
     * @return Response
     */
    public function postEventContactNotificationAction(Event $event, Request $request)
    {
        $content = $request->get('content');
        
        $this->notificationService->sendCustomerMessage(
            $event,
            $this->security->sanitize($content['name']),
            $this->security->sanitize($content['email']),
            $this->security->sanitize($content['title']),
            $this->security->sanitize($content['comment'])
        );

        return new Response(null, 204);
    }
}
