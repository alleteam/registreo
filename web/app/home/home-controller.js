angular.module('registreoApp.homeController', [])
    .controller('HomeController',['$rootScope', 'Breadcrumb',
        function ($rootScope, $Breadcrumb) {
            $rootScope.$broadcast('activeMenu', '');
            $rootScope.ngview.show = false;
            window.scrollTo(0, 0);
            $Breadcrumb.hide();
        }
    ]);


