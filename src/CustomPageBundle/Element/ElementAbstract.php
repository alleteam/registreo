<?php
namespace Registreo\CustomPageBundle\Element;

use JMS\Serializer\Annotation as JMS;

/**
 * @JMS\Discriminator(field = "type", disabled = false, map = {
 * "Paragraph": "Registreo\CustomPageBundle\Element\Paragraph",
 * "ContactForm": "Registreo\CustomPageBundle\Element\ContactForm",
 * "Map": "Registreo\CustomPageBundle\Element\Map",
 * "Contact": "Registreo\CustomPageBundle\Element\Contact",
 * "Image": "Registreo\CustomPageBundle\Element\Image",
 * "Carousel": "Registreo\CustomPageBundle\Element\Carousel",
 * })
 */
abstract class ElementAbstract 
{
    
}