angular.module('registreoApp')
    .controller('AddPageItemCtrl',['$scope', '$rootScope', '$uibModalInstance', 'param', 'uiGmapIsReady',
        function ($scope, $rootScope, $uibModalInstance, param, uiGmapIsReady) {

            uiGmapIsReady.promise(1).then(function(instances) {
                console.log(instances);
                
            });
            
            var contents = param.contents;
            if(!contents) {
                contents = new Array();
            }


            $rootScope.ngview.show = true;
            var selectedItem = angular.element('#selectElement');
            var order = 1;
            if(contents && contents.length) {
                order = contents.length + 1;
            }

            if (selectedItem.val() == 3) {
                angular.extend($scope, {
                  markers: [],
                  markerNo: 1,
                  map: {
                    center: {
                      latitude: 51.919438,
                      longitude: 19.145136
                    },
                    zoom: 8,
                    options: {

                    },
                    events: {
                      rightclick: function(map, eventName, events) {
                        var event = events[0];
                        var lat = event.latLng.lat();
                        var lng = event.latLng.lng();
                        $scope.$apply(function() {
                          $scope.item.map_data = {};
                          $scope.markers = new Array();
                          $scope.item.map_data = {longitude: lng + "", latitude: lat + ""};

                          $scope.markers.push({
                              latitude: lat,
                              longitude: lng,
                              id: $scope.markerNo
                          });
                        });

                      },
                    },

                  }
                });
            }

            $scope.tinymceOptions = {
                inline: false,
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code'
                ],
                toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                skin: 'lightgray',
                theme : 'modern'
            };

            $scope.item = {
                "order" : order,
                "type" : selectedItem.val()
            };

            $scope.closeModal = function () {
                $uibModalInstance.close();
            };

            $scope.saveItem = function (isValid) {
                if(selectedItem.val() == 3 && $scope.markers.length <= 0) {
                    return;
                }

                if(isValid) {
                    contents.push($scope.item);
                    $uibModalInstance.close();
                }
            };
        }
    ]);

