angular.module('registreoApp')
    .controller('HeaderCtrl',['$scope', '$location', 'UserService',
        function ($scope, $location, UserService) {

            $scope.loggedUser = false;

            UserService.getCurrentUser().then(function (user) {
                if(user){
                    $scope.loggedUser = true;
                } else {
                    //$scope.signIn();
                }
            });

            $scope.$on('loginSuccess', function (event, data) {
                $scope.loggedUser = true;
            });

            $scope.signOut = function() {
                UserService.logout();
                $scope.loggedUser = false;
            };

            $scope.isActive = function(url) {
                return ("/" + url == $location.$$path) ? 'active' : '';
            };

            $scope.aboutUs = function() {
                $location.path('/about');
            };

            $scope.home = function() {
                $location.path('/');
            };

            $scope.pricing = function() {
                $location.path('/pricing');
            };

            $scope.contact = function() {
                $location.path('/contact');
            };

            $scope.eventsList = function() {
                $location.path('/events');
            };
            
            $scope.signIn = function() {
                $location.path('/signin');
            };

            $scope.myaccount = function() {
                $location.path('/myevents');

            };

        }
    ]);
