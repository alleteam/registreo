<?php

namespace Registreo\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class UserTypePrivilege
 *
 * ORM\Entity()
 * ORM\Table(name="user_type_privilege")
 */
class UserTypePrivilege
{

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(name="user_type_id", type="integer")
     */
    protected $userTypeId;

    /**
     * @var integer
     *
     * @ORM\Column(name="privilege_id", type="integer")
     */
    protected $privilegeId;

    /**
     * @return int
     */
    public function getUserTypeId()
    {
        return $this->userTypeId;
    }

    /**
     * @param int $userTypeId
     *
     * @return $this
     */
    public function setUserTypeId($userTypeId)
    {
        $this->userTypeId = $userTypeId;

        return $this;
    }

    /**
     * @return int
     */
    public function getPrivilegeId()
    {
        return $this->privilegeId;
    }

    /**
     * @param int $privilegeId
     *
     * @return $this
     */
    public function setPrivilegeId($privilegeId)
    {
        $this->privilegeId = $privilegeId;

        return $this;
    }
}
