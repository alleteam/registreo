<?php

namespace Registreo\PaymentBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Registreo\Entity\EventPayout;
use mirolabs\dotpay\client\admin\Accounts;
use mirolabs\dotpay\client\admin\model\TransferRequest;
use mirolabs\dotpay\client\admin\model\Transfer;
use mirolabs\dotpay\client\admin\model\TransferRecipient;

class TransactionPayoutCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('transaction:payout')
            ->setDescription('create payout for event in dotpay')
            ->addArgument('argument', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $repository = $this->getContainer()->get('app.repository.event.payout');
        foreach ($repository->findByDirty(true) as $payout) {
            $this->processPayout($output, $payout);
        }
    }

    private function processPayout(OutputInterface $output, EventPayout $payout)
    {
        $output->writeln('Create payout for event id: '. $payout->getEvent()->getId());
        $repository = $this->getContainer()->get('app.repository.event.payout');
        $accountId = $this->getContainer()->getParameter('dotpay_account_id');
        $payout->setDirty(false);
        $repository->save($payout);
        try {
            $transfer = $this->createTransferRequest($payout);
            $response = $this->getDotpayAccounts()->createTransfers($accountId, $transfer);
            if ($response->getDetail() == 'ok') {
                $payout->setLog('OK');
            } else {
                $payout->setLog('ERROR');
            }
        } catch (\Exception $e) {
            $payout->setLog(mb_substr('ERROR: ' . $e->getMessage(), 0 , 1999));
        }
        $payout->setModified(new \DateTime());
        $repository->save($payout);
    }

    /**
     * @return Accounts
     */
    private function getDotpayAccounts()
    {
        return $this->getContainer()->get('app.dotpay.admin.accounts');
    }

    /**
     * @param EventPayout $payout
     * @return TransferRequest
     */
    private function createTransferRequest(EventPayout $payout)
    {
        $recipient = new TransferRecipient();
        $recipient
            ->setAccountNumber($payout->getEvent()->getAccountNumber())
            ->setName($payout->getEvent()->getName());


        $transfer = new Transfer();
        $request = new TransferRequest();
        $request->setCurrency('PLN');
        $request->setTransfers([$transfer]);
        $transfer
            ->setAmount($payout->getAmount())
            ->setControl("ep:" . $payout->getId())
            ->setDescription('payout for event id: ' . $payout->getEvent()->getId())
            ->setRecipient($recipient);
        return $request;
    }

}
