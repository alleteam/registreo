angular.module('registreoAppCustom')
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('home', {
                url: "/{eventName}",
                controller: 'CustomPageHomeCtrl',
                controllerAs: 'customPageHomeCtrl'
            });    
        }
    ])
    .controller('CustomPageHomeCtrl',['$rootScope', '$scope', '$element', 'MailerService', 'NotificationsService', 'vcRecaptchaService',
        function ($rootScope, $scope, $element, MailerService, NotificationsService, recaptcha) {
            $rootScope.ngview.show = false;
            window.scrollTo(0, 0);
            $scope.contact = {};
            $scope.sendMessageLoader = false;

            $scope.recaptchaPublicKey = '6LeQgxQUAAAAABDHmMR3iAKdI8Grh3uEx9_jcLGn';
            $scope.widgetId = '';

            $scope.templateData = {}; 


            $scope.reset = function(form) {
                $scope.contact = {};
                form.$setPristine();
                form.$setUntouched();
                recaptcha.reload($scope.widgetId);
            };

            $scope.setWidgetId = function (widgetId) {
                $scope.widgetId = widgetId;
            };

            $scope.sendContactMessage = function (form) {
                form.$submitted = true;
                if(form.$valid && $scope.sendMessageLoader == false) {
                    $scope.sendMessageLoader = true;
                    var eventElement = $element.find('#eventId');
                    MailerService.sendEventMessage($scope.contact, eventElement.text()).then(function (response) {
                        if (response.status == 204) {
                            NotificationsService.addNotification('Twoje zapytanie zostało wysłane.');
                            $scope.reset(form);
                        } else {
                            NotificationsService.addNotification('Wystąpił problem techniczny. Spróbuj wysłać wiadomość ponownie', NotificationsService.ERROR_NOTIFICATION);
                        }
                        $scope.sendMessageLoader = false;
                    });
                }
            };

        }
    ]);

