(function() {
    angular.module('notificationsService', [])
        .service('NotificationsService', notificationsService);

    notificationsService.$inject = ['$interval', '$sce'];

    function notificationsService($interval, $sce) {
        var self = this;
        self.notifications = [];
        self.SUCCESS_NOTIFICATION = 'success';
        self.ERROR_NOTIFICATION = 'danger';
        self.INFO_NOTIFITICATION = 'info';
        self.NOTIFICATION_TIME = 5000;

        self.addNotification = function (notification, type, id) {
            var notificationType = !type ? self.INFO_NOTIFITICATION : type;
            var notificationId = !id ? 1 : id;

            $interval(function () {
                var currentTime = new Date().getTime();
                angular.forEach(self.notifications, function (notification) {
                    if (notification.display && (notification.time + self.NOTIFICATION_TIME < currentTime)) {
                        self.removeNotification(notification);
                    }
                })
            }, 1000);

            self.notifications.push({
                text: $sce.trustAsHtml(notification),
                display: true,
                type: notificationType,
                id: notificationId,
                time: new Date().getTime()
            });
        };

        self.getNotifications = function () {
            return self.notifications;
        };

        self.removeNotification = function (notification) {
            notification.display = false;
        };
    }
})();