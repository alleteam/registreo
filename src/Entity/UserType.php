<?php

namespace Registreo\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as ORMAssert;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as JMS;

/**
 * ORM\Entity(repositoryClass="UserBundle\Repository\UserTypeRepository")
 * ORM\Table(name="user_types")
 * ORMAssert\UniqueEntity(fields={"name"}, message="user type name must be unique")
 */
class UserType
{
    const TYPE_EVENT_ORGANIZER_ID = 1;
    const TYPE_EVENT_PARTICIPANT_ID = 2;
    const TYPE_ADMIN_ID = 3;

    const TYPE_EVENT_ORGANIZER = 'Organizer';
    const TYPE_EVENT_PARTICIPANT = 'Participant';
    const TYPE_ADMIN = 'Admin';

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     * @Assert\NotBlank
     */
    protected $name;

    /**
     * @ORM\ManyToMany(targetEntity="UserBundle\Entity\Privilege")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     * @Assert\NotNull
     */
    protected $privileges;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return $this
     */
    public function setName($role)
    {
        $this->name = $role;

        return $this;
    }

    /**
     * @return Privilege[]
     */
    public function getPrivileges()
    {
        return $this->privileges;
    }

    /**
     * @param Privilege[] $privileges
     *
     * @return $this
     */
    public function setPrivileges($privileges)
    {
        $this->privileges = $privileges;

        return $this;
    }

    /**
     * @param string $privilege
     * @return bool
     */
    public function hasPrivilege($privilege)
    {
        foreach ($this->getPrivileges() as $privilegeEntity) {
            if ($privilegeEntity->getName() == $privilege) {
                return true;
            }
        }

        return false;
    }
}
