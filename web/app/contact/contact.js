(function () {
    angular.module('registreoApp.contact', ['registreoApp.contactController'])
        .config(config);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('contact', {
                url: '/contact',
                templateUrl: 'app/contact/contact.html',
                controller: 'ContactController',
                controllerAs: 'contactCtrl'
            });
    }
})();
