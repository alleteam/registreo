<?php
namespace Registreo\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as UserModel;
use Symfony\Bridge\Doctrine\Validator\Constraints as ORMAssert;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\Exclude;

/**
 * Class Event
 *
 * @ORM\Entity(repositoryClass="Registreo\EventBundle\Repository\EventRepository")
 * @ORM\Table(name="events")
 */
class Event
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var CustomPage
     *
     * @ORM\OneToOne(targetEntity="CustomPage", cascade={"persist"})
     * @ORM\JoinColumn(name="custom_page_id", referencedColumnName="id")
     */
    protected $customPage;

    /**
     * @var CustomForm
     *
     * @ORM\OneToOne(targetEntity="CustomForm", cascade={"persist"})
     * @ORM\JoinColumn(name="custom_form_id", referencedColumnName="id")
     */
    protected $customForm;

    /**
     * @var Participant[]
     *
     * @ORM\OneToMany(targetEntity="Participant", mappedBy="event")
     */
    protected $participants;

    /**
     * @var int
     *
     * @ORM\Column(name="participants_limit", type="integer", options={"default" = 0})
     * @Assert\NotNull
     * @Assert\Type(
     *     type="integer"
     * )
     */
    protected $limit;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", options={"default" = 1})
     * @Assert\NotNull
     */
    protected $status;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="start_date", type="datetime")
     * @Assert\GreaterThan("today", message="event.startDate.greater_than_today")
     */
    protected $startDate;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="registration_date", type="datetime")
     * @Assert\GreaterThan("today", message="event.startDate.greater_than_today")
     */
    protected $registrationDate;

    /**
     * @var String
     *
     * @ORM\Column(name="description", type="string", length=400)
     * @Assert\Length(
     *      max = 400
     * )
     */
    protected $description;

    /**
     * @var String
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\Length(
     *      min = 2,
     *      max = 255
     * )
     */
    protected $name;

    /**
     * @var Int
     *
     * @ORM\Column(name="fee", type="integer", nullable=true)
     * @Assert\Type(
     *     type="integer"
     * )
     * @Assert\GreaterThan(
     *     value = 14
     * )
     */
    protected $fee;

    /**
     * @var String
     *
     * @ORM\Column(name="bank_name", type="string", length=50, nullable=true)
     * @Assert\Length(
     *      min = 2,
     *      max = 50
     * )
     */
    protected $bankName;

    /**
     * @var String
     *
     * @ORM\Column(name="account_number", type="string", length=28, nullable=true)
     * @Assert\Length(
     *      min = 28,
     *      max = 28
     * )
     * @Assert\Iban(
     *     message="Niepoprawny numer rachunku bankowego (IBAN)."
     * )
     */
    protected $accountNumber;

    /**
     * @var UserModel
     *
     * @ORM\ManyToOne(targetEntity="User")
     */
    protected $user;


    /**
     * @var float
     *
     * @ORM\Column(name="discount", type="float")
     */
    protected $discount;

    /**
     * @var int
     *
     * @ORM\Column(name="free", type="integer", options={"default" = 0})
     * @JMS\Expose
     */
    protected $free = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="closed", type="integer", options={"default" = 0})
     * @JMS\Expose
     */
    protected $closed = 0;

    /**
     * @var SeoLink
     *
     * @ORM\OneToOne(targetEntity="SeoLink", mappedBy="event", cascade={"persist"})
     */
    protected $seoLink;


    /**
     * @return DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @var DateTime
     *
     * @ORM\Column(name="regulation_accepted", type="datetime")
     * @Assert\NotNull
     */
    protected $regulation;

    /**
     * @param DateTime $startDate
     * @return $this
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * @return int
     */
    public function daysRemain()
    {
        $today = new DateTime("now");
        $difference = $today->diff($this->getStartDate());

        return $difference->days;
    }

    /**
     * @return int
     */
    public function participantCount()
    {
        return count($this->getParticipants());
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return CustomPage
     */
    public function getCustomPage()
    {
        return $this->customPage;
    }

    /**
     * @param CustomPage $customPage
     */
    public function setCustomPage($customPage)
    {
        $this->customPage = $customPage;
        return $this;
    }

    /**
     * @return CustomForm
     */
    public function getCustomForm()
    {
        return $this->customForm;
    }

    /**
     * @param CustomForm $customForm
     */
    public function setCustomForm($customForm)
    {
        $this->customForm = $customForm;
        return $this;
    }

    /**
     * @return Participant[]
     */
    public function getParticipants()
    {
        return $this->participants;
    }

    /**
     * @param Participant[] $participants
     */
    public function setParticipants($participants)
    {
        $this->participants = $participants;
        return $this;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @return String
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param String $description
     * @return Event
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return String
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param String $name
     * @return Event
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return Int
     */
    public function getFee()
    {
        return (int) $this->fee;
    }

    /**
     * @param Int $fee
     * @return Event
     */
    public function setFee($fee)
    {
        $this->fee = $fee;
        return $this;
    }

    /**
     * @return String
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param String $bankName
     * @return Event
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
        return $this;
    }

    /**
     * @return String
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @param String $accountNumber
     * @return Event
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Event
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return DateTime
     */
    public function getRegulation()
    {
        return $this->regulation;
    }

    /**
     * @param DateTime $regulation
     */
    public function setRegulation($regulation)
    {
        $this->regulation = $regulation;
    }

    public function getFree()
    {
        return $this->free;
    }

    public function setFree($free)
    {
        $this->free = $free;
        return $this;
    }

    public function getClosed()
    {
        return $this->closed;
    }

    public function setClosed($closed)
    {
        $this->closed = $closed;
        return $this;
    }

    public function getSeoLink()
    {
        return $this->seoLink;
    }

    public function setSeoLink(SeoLink $seoLink)
    {
        $this->seoLink = $seoLink;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getRegistrationDate()
    {
        return $this->registrationDate;
    }

    /**
     * @param DateTime $registrationDate
     */
    public function setRegistrationDate($registrationDate)
    {
        $this->registrationDate = $registrationDate;
    }
}
