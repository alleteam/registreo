<?php
namespace Registreo\EventBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Registreo\Entity\SeoLink;

class SeoLinkRepository extends EntityRepository
{
    /**
     * @param SeoLink $seoLink
     */
    public function save(SeoLink $link)
    {
        $this->getEntityManager()->persist($link);
        $this->getEntityManager()->flush();
    }

    public function update(SeoLink $link)
    {
        $this->getEntityManager()->merge($link);
        $this->getEntityManager()->flush();
    }

    /**
     *
     * @param type $link
     * @return \Registreo\Entity\SeoLink
     */
    public function getLink($link)
    {
        $query = $this->createQueryBuilder('t');
        $query->where('t.name = :link and t.active = :status');
        $query->setParameter('link', $link);
        $query->setParameter('status', 'Y');
        return $query->getQuery()->getOneOrNullResult();
    }

}