<?php
namespace Registreo\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as ORMAssert;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\Groups;

/**
 * Class Event
 *
 * @ORM\Entity(repositoryClass="Registreo\EventBundle\Repository\ParticipantRepository")
 * @ORM\Table(name="participants")
 */
class Participant
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"admin_list"})
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Event", inversedBy="participants")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id")
     * @Exclude
     */
    protected $event;

    /**
     * @ORM\Column(name="payment_status", type="integer", options={"default" = 0})
     * @Assert\NotBlank
     * @Groups({"admin_list", "list"})
     */
    protected $paymentStatus;

    /**
     * @var RegistrationApplicationField[]
     *
     * @ORM\OneToMany(targetEntity="RegistrationApplicationField", mappedBy="participant", cascade={"persist"})
     * @Exclude
     */
    protected $fields;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="added", type="datetime")
     * @Assert\NotBlank
     * @Groups({"admin_list", "list"})
     */
    protected $added;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     * @Assert\NotBlank
     * @Groups({"admin_list", "list"})
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=50)
     * @Assert\NotBlank
     * @Groups({"admin_list", "list"})
     */
    protected $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50)
     * @Assert\NotBlank
     * @Groups({"admin_list"})
     */
    protected $email;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="regulation_general_accepted", type="datetime")
     */
    protected $generalRegulation;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="regulation_data_accepted", type="datetime")
     */
    protected $dataRegulation;

    /**
     * @var string
     *
     * @ORM\Column(name="regulation_host_accepted", type="string")
     */
    protected $remoteHostAccepted;


    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=50, nullable=true)
     * @Exclude
     */
    protected $hash;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }

    public function getTranslatedPaymentStatus()
    {
        if ($this->getPaymentStatus() == 3) {
            return "opłacony";
        } elseif ($this->getPaymentStatus() == 10) {
            return "zweryfikowany";
        }

        return "nieopłacony";
    }

    /**
     * @param mixed $paymentStatus
     */
    public function setPaymentStatus($paymentStatus)
    {
        $this->paymentStatus = $paymentStatus;
    }

    /**
     * @return Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param Event $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }

    /**
     * @return RegistrationApplicationField[]
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param RegistrationApplicationField[] $fields
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
    }

    /**
     * @return DateTime
     */
    public function getAdded()
    {
        return $this->added;
    }

    /**
     * @param DateTime $added
     */
    public function setAdded($added)
    {
        $this->added = $added;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return DateTime
     */
    public function getGeneralRegulation()
    {
        return $this->generalRegulation;
    }

    /**
     * @param DateTime $generalRegulation
     */
    public function setGeneralRegulation($generalRegulation)
    {
        $this->generalRegulation = $generalRegulation;
    }

    /**
     * @return DateTime
     */
    public function getDataRegulation()
    {
        return $this->dataRegulation;
    }

    /**
     * @param DateTime $dataRegulation
     */
    public function setDataRegulation($dataRegulation)
    {
        $this->dataRegulation = $dataRegulation;
    }

    /**
     * @return string
     */
    public function getRemoteHostAccepted()
    {
        return $this->remoteHostAccepted;
    }

    /**
     * @param string $remoteHostAccepted
     */
    public function setRemoteHostAccepted($remoteHostAccepted)
    {
        $this->remoteHostAccepted = $remoteHostAccepted;
    }

    public function getHash()
    {
        return $this->hash;
    }

    public function setHash($hash)
    {
        $this->hash = $hash;
    }


}
