<?php

namespace Registreo\CustomPageBundle\Service;

use Registreo\CustomPageBundle\Repository\RegistrationApplicationFieldRepository;
use Registreo\Entity\Event;
use Registreo\Entity\Participant;
use Registreo\Entity\RegistrationApplicationField;
use Registreo\PaymentBundle\Service\TransactionService;
use DateTime;
use Registreo\EventBundle\Repository\CustomFormFieldRepository;
use Registreo\EventBundle\Repository\ParticipantRepository;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use mirolabs\dotpay\client\payment\Notification;
use mirolabs\dotpay\client\payment\model\Notice;
use Registreo\AppBundle\Event\InviteParticipantEvent;

class ApplicationService implements Notification
{
    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @var CustomFormFieldRepository
     */
    protected $customFormFieldRepository;

    /**
     * @var ParticipantRepository
     */
    protected $participantRepository;

    /**
     * @var RegistrationApplicationFieldRepository
     */
    protected $applicationFieldsRepository;

    /**
     * @var EventDispatcher
     */
    protected $dispatcher;


    /**
     * @param RegistrationApplicationFieldRepository $applicationFieldsRepository
     */
    public function setApplicationFieldsRepository(RegistrationApplicationFieldRepository $applicationFieldsRepository)
    {
        $this->applicationFieldsRepository = $applicationFieldsRepository;
    }

    /**
     * @param ParticipantRepository $participantRepository
     */
    public function setParticipantRepository(ParticipantRepository $participantRepository)
    {
        $this->participantRepository = $participantRepository;
    }

    /**
     * @param CustomFormFieldRepository $customFormFieldRepository
     */
    public function setCustomFormFieldRepository(CustomFormFieldRepository $customFormFieldRepository)
    {
        $this->customFormFieldRepository = $customFormFieldRepository;
    }

    /**
     * @return ValidatorInterface
     */
    public function getValidator()
    {
        return $this->validator;
    }

    /**
     * @param ValidatorInterface $validator
     * @return $this
     */
    public function setValidator(ValidatorInterface $validator)
    {
        $this->validator = $validator;

        return $this;
    }

    public function setEventDispatcher($dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param Event $event
     * @param []    $applicationForm
     * @param Participant $participant
     * 
     * @return Participant
     */
    public function saveApplicationForm($event, $applicationForm, $participant = null)
    {
        if (is_null($participant)) {
            $participant = new Participant();
            $participant->setEvent($event);
            $participant->setAdded(new DateTime());
        } else {
            $participant = $this->participantRepository->find($participant->getId());
        }

        $participant->setGeneralRegulation(new DateTime());
        $participant->setDataRegulation(new DateTime());
        $participant->setRemoteHostAccepted($_SERVER['REMOTE_ADDR']);
        if($event->getFree()) {
            $participant->setPaymentStatus(TransactionService::PAYMENT_FREE);
        } else {
            $participant->setPaymentStatus(TransactionService::PAYMENT_MISSING);
        }
        $this->setApplicationFields($participant, $applicationForm->application);
        $this->participantRepository->save($participant);
        
        return $participant;
    }


    public function inviteParticipant(Event $event, Participant $request) {
        if (!$event->getClosed()) {
            throw new \Exception('Invalid event');
        }

        $participant = new Participant();
        $participant->setEvent($event);
        $participant->setPaymentStatus(TransactionService::SENT_INVITE);
        $participant->setFields([]);
        $participant->setAdded(new DateTime());
        $participant->setGeneralRegulation(new DateTime());
        $participant->setDataRegulation(new DateTime());
        $participant->setRemoteHostAccepted('localhost');
        $participant->setName($request->getName());
        $participant->setSurname($request->getSurname());
        $participant->setEmail($request->getEmail());
        $participant->setHash(md5(microtime(true) . $event->getId() . $request->getEmail() . rand(1, 1000000)));
        $this->participantRepository->save($participant);
        $this->dispatcher->dispatch(InviteParticipantEvent::NAME, new InviteParticipantEvent($participant));
        return $participant;
    }



    /**
     * @param Participant $participant
     * @param []          $applicationFields
     */
    private function setApplicationFields($participant, $applicationFields)
    {
        $fields = [];
        foreach ($applicationFields as $field) {
            $customFormField = $this->customFormFieldRepository->findOneById($field->id);
            $applicationField = new RegistrationApplicationField();
            $applicationField->setValue($field->value);
            $applicationField->setFormField($customFormField);
            $applicationField->setParticipant($participant);
            $fields[] = $applicationField;
            $this->setParticipantData($field, $participant);
        }

        $participant->setFields($fields);
    }

    /**
     * @param Object      $field
     * @param Participant $participant
     */
    private function setParticipantData($field, $participant)
    {
        switch ($field->label) {
            case 'Imię':
                $participant->setName($field->value);
                break;
            case 'Nazwisko':
                $participant->setSurname($field->value);
                break;
            case 'Email':
                $participant->setEmail($field->value);
                break;
        }
    }

    /**
     * @param int $page
     * @param int $limit
     * @param Event $event
     * @return array
     */
    public function getParticipantsList($page, $limit, $event)
    {
        return [
            "pagin" => [
                "current" => $page,
                "limit" => $limit,
                "total" => count($event->getParticipants())
            ],
            "list" => $this->participantRepository->findBy(
                    ['event' => $event->getId()],
                    ['id' => 'ASC'],
                    $limit,
                    ($page-1) * $limit
            )
        ];
    }

    /**
     * @param int $participantId
     * @return RegistrationApplicationField[]
     */
    public function getApplicationFields($participantId)
    {
        return $this->applicationFieldsRepository->findBy(['participant' => $participantId]);
    }

    /**
     * @param Participant $participant
     * @param int $status
     * @return null
     */
    public function changePaymentStatus($participant, $status)
    {
        $participant->setPaymentStatus($status);
        $this->participantRepository->save($participant);
    }



    public function callNotification(Notice $notice)
    {
        $tab = explode(":", $notice->getControl());
        if (count($tab) > 1 && $tab[0] == 'p') {
			$participant = $this->participantRepository->find($tab[1]);
			if (!is_null($participant)) {
				$participant->setPaymentStatus($this->convertPaymentStatus($notice->getOperationStatus()));
                $this->participantRepository->save($participant);
            }
        }
    }

    private function convertPaymentStatus($statusName)
    {
        switch($statusName) {
            case 'new':
                return TransactionService::PAYMENT_NEW;
            case 'processing':
                return TransactionService::PAYMENT_PROCESSING;
            case 'completed':
                return TransactionService::PAYMENT_COMPLETED;
            case 'rejected':
                return TransactionService::PAYMENT_REJECTED;
            case 'processing_realization_waiting':
                return TransactionService::PAYMENT_WAITING;
            case 'processing_realization':
                return TransactionService::PAYMENT_REALIZATION;
        }
        return 0;
    }


}
