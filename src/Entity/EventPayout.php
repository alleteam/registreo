<?php
namespace Registreo\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Registreo\PaymentBundle\Repository\EventPayoutRepository")
 * @ORM\Table(name="event_payout")
 */
class EventPayout
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Event
     *
     * @ORM\ManyToOne(targetEntity="Event")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id", nullable=false)
     * @JMS\Exclude
     */
    protected $event;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=30)
     */
    protected $number;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float")
     */
    protected $amount;

    /**
     * @var float
     *
     * @ORM\Column(name="dotpayCommission", type="float")
     */
    protected $dotpayCommission;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=30)
     */
    protected $status;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="added", type="datetime")
     */
    protected $added;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    protected $modified;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="dirty", type="boolean")
     */
    protected $dirty;


    /**
     * @var string
     *
     * @ORM\Column(name="log", type="string", length=2000)
     */
    protected $log;


    function getId()
    {
        return $this->id;
    }

    function getEvent()
    {
        return $this->event;
    }

    function getNumber()
    {
        return $this->number;
    }

    function getAmount()
    {
        return $this->amount;
    }

    function getDotpayCommission()
    {
        return $this->dotpayCommission;
    }

    function getStatus()
    {
        return $this->status;
    }

    function getAdded()
    {
        return $this->added;
    }

    function getModified()
    {
        return $this->modified;
    }

    function getDirty()
    {
        return $this->dirty;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setEvent(Event $event)
    {
        $this->event = $event;
    }

    function setNumber($number)
    {
        $this->number = $number;
    }

    function setAmount($amount)
    {
        $this->amount = $amount;
    }

    function setDotpayCommission($dotpayCommission)
    {
        $this->dotpayCommission = $dotpayCommission;
    }

    function setStatus($status)
    {
        $this->status = $status;
    }

    function setAdded(\DateTime $added)
    {
        $this->added = $added;
    }

    function setModified(\DateTime $modified)
    {
        $this->modified = $modified;
    }

    function setDirty($dirty)
    {
        $this->dirty = $dirty;
    }


    public function getLog()
    {
        return $this->log;
    }

    public function setLog($log)
    {
        $this->log = $log;
    }




}