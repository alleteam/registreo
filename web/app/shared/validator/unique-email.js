(function () {
    angular.module('registreoApp.validator.uniqueEmailValidator', ['restangular'])
        .directive('uniqueEmail', uniqueEmail);

    uniqueEmail.$inject = ['$q', 'Restangular'];

    function uniqueEmail($q, Restangular) {
        return {
            restrict: "A",
            require: "ngModel",
            link: function (scope, element, attributes, ngModel) {
                ngModel.$asyncValidators.uniqueEmail = function (modelValue) {
                    var defer = $q.defer();
                    Restangular
                        .all('users')
                        .all('validate')
                        .all('email')
                        .post({'email': modelValue})
                        .then(function (response) {
                            if (response.unique) {
                                defer.resolve();
                            } else {
                                defer.reject();
                            }
                        });
                    return defer.promise;
                };
            }
        };
    }
})();