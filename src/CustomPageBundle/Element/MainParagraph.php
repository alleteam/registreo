<?php
namespace Registreo\CustomPageBundle\Element;

use JMS\Serializer\Annotation as JMS;

class MainParagraph implements ElementTemplate
{
    /**
     * @JMS\Exclude
     */
    public static $TEMPLATE = '<main-paragraph title="templateData.main_paragraph.title" '
        . 'description="templateData.main_paragraph.description"></main-paragraph>';


    /**
     * @JMS\Type("string")
     * @var string
     */
    private $title;

    /**
     * @JMS\Type("string")
     * @var string
     */
    private $description;

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getTemplate()
    {
        return self::$TEMPLATE;
    }

}